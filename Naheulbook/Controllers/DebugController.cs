﻿using System;
using Microsoft.AspNetCore.Mvc;
using Naheulbook.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class DebugController : NaheulbookController
    {
        public DebugController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }

        public IActionResult Report([FromBody] JObject inputData)
        {
            var errorReport = new ErrorReport
            {
                Date = DateTime.UtcNow,
                Data = inputData.ToString(Formatting.None),
                UserAgent = Request.Headers["User-Agent"]
            };

            DbContext.Add(errorReport);
            DbContext.SaveChanges();

            return new EmptyResult();
        }
    }
}