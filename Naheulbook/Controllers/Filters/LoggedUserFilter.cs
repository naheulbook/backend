﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Naheulbook.Controllers.Filters
{
    public class LoggedUserFilter : Attribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var controller = context.Controller as NaheulbookController;

            if (controller == null) return;
            if (controller.CurrentUser != null) return;

            var jsonResult = new JsonResult(new {error = "not_authentificated"})
            {
                StatusCode = (int) HttpStatusCode.Unauthorized,
                ContentType = "application/json",
            };
            context.Result = jsonResult;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}