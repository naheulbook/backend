﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Models;

namespace Naheulbook.Controllers
{
    public class JobController:NaheulbookController
    {
        public JobController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }

        public IActionResult List()
        {
            var jobs = DbContext.Job
                .Include(job => job.JobRequirement)
                .Include(job => job.JobRestrict)
                .Include(job => job.JobBonus)
                .Include(job => job.JobOriginBlacklist)
                    .ThenInclude(bl => bl.Origin)
                .Include(job => job.JobOriginWhitelist)
                    .ThenInclude(wl => wl.Origin)
                .Include(job => job.JobSkill)
                .Include(job => job.Speciality)
                    .ThenInclude(s => s.SpecialityModifier)
                .Include(job => job.Speciality)
                    .ThenInclude(s => s.SpecialitySpecial)
                .ToList();

            return Json(jobs);
        }
    }
}