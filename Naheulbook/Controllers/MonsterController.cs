﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;
using Naheulbook.Utils.Item;
using Naheulbook.Utils.Modifier;
using Naheulbook.WebSocket;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class MonsterController : NaheulbookController
    {
        public readonly IWebsocketManager WebsocketManager;

        public MonsterController(NaheulbookDbContext dbContext, IWebsocketManager websocketManager) : base(dbContext)
        {
            WebsocketManager = websocketManager;
        }


        [LoggedUserFilter]
        public async Task<IActionResult> LoadMonsters([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var monsters = await DbContext.Monster
                .Include(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateUnskills)
                .Include(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSkills)
                .Include(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateModifiers)
                .Include(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateRequirements)
                .Include(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSlot)
                .ThenInclude(i => i.Slot)
                .Include(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSkillModifiers)
                .Where(m => m.Dead == null && m.GroupId == groupId)
                .ToListAsync();

            return Json(monsters);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> LoadDeadMonsters([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            var startIndex = inputData.Value<int>("startIndex");
            var count = inputData.Value<int>("count");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var monsters = await DbContext.Monster
                .Where(m => m.Dead != null && m.GroupId == groupId)
                .OrderByDescending(m => m.Dead)
                .Skip(startIndex)
                .Take(count)
                .ToListAsync();

            return Json(monsters);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateMonster([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            var monsterData = inputData.Value<JObject>("monster");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            monsterData["color"] = "#000000";

            var monster = new Monster
            {
                Name = monsterData.Value<string>("name"),
                GroupId = groupId,
                Data = JsonConvert.SerializeObject(monsterData["data"])
            };

            DbContext.Add(monster);
            DbContext.SaveChanges();

            JArray jInventory = monsterData.Value<JArray>("items");
            if (jInventory != null)
            {
                foreach (JToken jItemInventory in jInventory)
                {
                    JObject itemInventory = jItemInventory.ToObject<JObject>();
                    JObject itemData = itemInventory.Value<JObject>("data");
                    long itemTemplateId = itemInventory.Value<JObject>("template").Value<long>("id");

                    ItemTemplate itemTemplate = DbContext.ItemTemplate.Find(itemTemplateId);
                    var item = Item.Create(itemTemplate, itemData);
                    item.MonsterId = monster.Id;

                    DbContext.Add(item);
                }
            }
            await DbContext.SaveChangesAsync();

            await ItemHelper.IncludeFullItemTemplate(DbContext.Entry(monster).Collection(i => i.Items).Query())
                .LoadAsync();

            return Json(monster);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> UpdateMonster([FromBody] JObject inputData)
        {
            var monsterId = inputData.Value<int>("monsterId");
            var fieldName = inputData.Value<string>("fieldName");

            Monster monster = DbContext.Monster
                .Include(m => m.Group)
                .First(m => m.Id == monsterId);

            if (monster.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            switch (fieldName)
            {
                case "name":
                {
                    monster.Name = inputData.Value<string>("value");
                    WebsocketManager.SendMessage("changeName", ObservedElementType.Monster, monster.Id, monster.Name);
                    break;
                }
                case "target":
                {
                    var targetInfo = inputData.Value<JObject>("value");
                    var isMonster = targetInfo.Value<bool>("isMonster");
                    var id = targetInfo.Value<long>("id");
                    if (isMonster)
                    {
                        monster.TargetMonsterId = id;
                        monster.TargetCharacterId = null;
                    }
                    else
                    {
                        monster.TargetMonsterId = null;
                        monster.TargetCharacterId = id;
                    }
                    WebsocketManager.SendMessage("changeTarget", ObservedElementType.Monster, monster.Id, targetInfo);
                    break;
                }
                default:
                {
                    JObject dataObject = JObject.Parse(monster.Data);
                    dataObject[fieldName] = inputData["value"];
                    monster.Data = JsonConvert.SerializeObject(dataObject);
                    WebsocketManager.SendMessage("changeData", ObservedElementType.Monster, monster.Id, new
                    {
                        value = inputData["value"],
                        fieldName
                    });
                    break;
                }
            }

            await DbContext.SaveChangesAsync();

            var response = new
            {
                value = inputData["value"],
                fieldName
            };

            return Json(response);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> AddModifier([FromBody] JObject inputData)
        {
            var monsterId = inputData.Value<int>("monsterId");
            var modifier = inputData["modifier"].ToObject<ActiveStatsModifier>();

            Monster monster = DbContext.Monster
                .Include(m => m.Group)
                .First(m => m.Id == monsterId);

            if (monster.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            long modifierId = 1;
            var modifiers = monster.Modifiers;
            if (modifiers.Count > 0) 
                modifierId = modifiers.Max(s => s.Id) + 1;
            modifier.Id = modifierId;
            modifier.SetActive(true);
            modifiers.Add(modifier);
            monster.ModifiersJson = JsonConvert.SerializeObject(modifiers);

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("addModifier", ObservedElementType.Monster, monster.Id, modifier);

            return Json(modifier);
        }


        [LoggedUserFilter]
        public async Task<IActionResult> RemoveModifier([FromBody] JObject inputData)
        {
            var monsterId = inputData.Value<int>("monsterId");
            var modifierId = inputData.Value<long>("modifierId");

            Monster monster = DbContext.Monster
                .Include(m => m.Group)
                .First(m => m.Id == monsterId);

            if (monster.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var modifiers = monster.Modifiers;
            ActiveStatsModifier modifier = modifiers.FirstOrDefault(s => s.Id == modifierId);
            if (modifier == null)
            {
                return Error(HttpStatusCode.BadRequest, "modifier_not_found");
            }
            modifiers.Remove(modifier);
            monster.ModifiersJson = JsonConvert.SerializeObject(modifiers);

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("removeModifier", ObservedElementType.Monster, monster.Id, modifier);
            return Json(modifier);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> ToggleModifier([FromBody] JObject inputData)
        {
            var monsterId = inputData.Value<int>("monsterId");
            var modifierId = inputData.Value<long>("modifierId");

            Monster monster = DbContext.Monster
                .Include(m => m.Group)
                .First(m => m.Id == monsterId);

            if (monster.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var modifiers = monster.Modifiers;
            ActiveStatsModifier modifier = modifiers.FirstOrDefault(s => s.Id == modifierId);
            if (modifier == null)
            {
                return Error(HttpStatusCode.BadRequest, "modifier_not_found");
            }
            if (!modifier.Reusable)
            {
                return Error(HttpStatusCode.BadRequest, "modifier_not_reusable");
            }

            modifier.SetActive(!modifier.Active);

            monster.ModifiersJson = JsonConvert.SerializeObject(modifiers);

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("updateModifier", ObservedElementType.Monster, monster.Id, modifier);
            return Json(modifier);
        }

        public async Task<IActionResult> EquipItem([FromBody] JObject inputData)
        {
            var monsterId = inputData.Value<int>("monsterId");
            var itemId = inputData.Value<long>("itemId");
            var equiped = inputData.Value<bool>("equiped");

            Monster monster = DbContext.Monster
                .Include(m => m.Group)
                .First(m => m.Id == monsterId);

            if (monster.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var item = await DbContext.Item
                .FirstAsync(i => i.Id == itemId && i.MonsterId == monsterId);

            item.Data["equiped"] = equiped ? 1 : 0;
            
            await DbContext.SaveChangesAsync();
            
            WebsocketManager.SendMessage("equipItem", ObservedElementType.Monster, monster.Id, item);

            return Json(item);
        }
    }
}