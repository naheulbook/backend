﻿using System;
using Microsoft.AspNetCore.Mvc;
using Naheulbook.Models;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Naheulbook.Controllers
{
    public abstract class NaheulbookController: Controller
    {
        protected NaheulbookDbContext DbContext;

        public class SessionData
        {
            public long UserId { get; set; }
            public string LoginToken { get; set; }
        }

        protected NaheulbookController(NaheulbookDbContext dbContext)
        {
            this.DbContext = dbContext;
        }

        private User _currentUser = null;

        public User CurrentUser => _currentUser ?? (_currentUser = GetCurrentUser());

        public User GetCurrentUser()
        {
            SessionData sessionData = GetCurrentSession();

            if (sessionData == null || sessionData.UserId == 0)
            {
                return null;
            }

            return DbContext.User.FirstOrDefault(u => u.Id == sessionData.UserId);
        }

        public SessionData GetCurrentSession()
        {
            var json = HttpContext.Session.GetString("SessionData");
            SessionData sessionData;

            if (json == null)
            {
                if (!Request.Cookies.ContainsKey("NaheulbookAuthKey"))
                {
                    return new SessionData();
                }

                UserSession userSession = DbContext.UserSession
                    .FirstOrDefault(s => s.Key.Equals(Request.Cookies["NaheulbookAuthKey"]));
                if (userSession == null)
                {
                    return new SessionData();
                }

                if (userSession.Expire < DateTime.UtcNow)
                {
                    DbContext.Remove(userSession);
                    DbContext.SaveChanges();
                    return new SessionData();
                }

                sessionData = new SessionData {UserId = userSession.UserId};
                SetCurrentSession(sessionData);
                return sessionData;
            }

            sessionData = JsonConvert.DeserializeObject<SessionData>(json);
            return sessionData ?? new SessionData();
        }

        public void SetCurrentSession(SessionData sessionData)
        {
            var json = JsonConvert.SerializeObject(sessionData);
            HttpContext.Session.SetString("SessionData", json);
        }

        public void ForgetUserSession()
        {
            if (!Request.Cookies.ContainsKey("NaheulbookAuthKey"))
            {
                return;
            }
            var authKey = Request.Cookies["NaheulbookAuthKey"];
            Response.Cookies.Delete("NaheulbookAuthKey", new CookieOptions()
            {
                Domain = "/",
                HttpOnly=  false,
                Secure = false,
            });
            if (string.IsNullOrEmpty(authKey))
            {
                return;
            }
            UserSession userSession = DbContext.UserSession.FirstOrDefault(s => s.Key.Equals(authKey));
            if (userSession != null)
            {
                DbContext.Remove(userSession);
                DbContext.SaveChanges();
            }
        }

        public void RemeberMeSession(SessionData sessionData)
        {
            string authKey = null;
            using(RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                var tokenData = new byte[128];
                rng.GetBytes(tokenData);

                authKey = Convert.ToBase64String(tokenData);
            }

            var session = new UserSession();
            session.UserId = sessionData.UserId;
            session.Key = authKey;
            session.Start = DateTime.UtcNow;
            session.Expire = DateTime.UtcNow.AddDays(60);
            session.Ip = HttpContext?.Connection?.RemoteIpAddress?.ToString();

            Response.Cookies.Append("NaheulbookAuthKey", authKey, new CookieOptions()
            {
                Expires = new DateTimeOffset(session.Expire),
                Path = "/",
                HttpOnly = false,
                Secure = false
            });

            DbContext.Add(session);
            DbContext.SaveChanges();
        }

        public IActionResult Error(HttpStatusCode status, string errorMessage)
        {
            Response.StatusCode = (int) status;
            return Json(new {error = errorMessage});
        }
    }
}