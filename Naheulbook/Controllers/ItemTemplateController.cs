﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;
using Naheulbook.Utils.Item;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class ItemTemplateController : NaheulbookController
    {
        public ItemTemplateController(NaheulbookDbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<IActionResult> CategoryList()
        {
            var itemTypes = await DbContext.ItemTemplateSection
                .Include(i => i.ItemCategory)
                .ToListAsync();

            return Json(itemTypes);
        }

        public async Task<IActionResult> SlotList()
        {
            var slots = await DbContext.ItemSlot.ToListAsync();

            return Json(slots);
        }

        public async Task<IActionResult> ItemTypesList()
        {
            var itemTypes = await DbContext.ItemType.ToListAsync();

            return Json(itemTypes);
        }

        public async Task<IActionResult> CreateItemType([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            var itemType = inputData.Value<JObject>("itemType").ToObject<ItemType>();
            
            await DbContext.ItemType.AddAsync(itemType);
            await DbContext.SaveChangesAsync();

            return Json(itemType);
        }

        public async Task<IActionResult> List([FromBody] dynamic request)
        {
            int typeId = request.typeId;
            var categories = DbContext.ItemTemplateSection
                .Include(c => c.ItemCategory)
                .First(t => t.Id == typeId)
                .ItemCategory;
            var categoriesId = categories.Select(c => c.Id);
            var currentUserId = CurrentUser?.Id;
            
            var li = await ItemHelper.IncludeFullItemTemplate(DbContext.ItemTemplate
                    .Where(i => categoriesId.Contains(i.CategoryId)
                                && (i.Source != "private" || i.SourceUserId == currentUserId)))
                .ToListAsync();

            return Json(li);
        }

        public async Task<IActionResult> Detail([FromBody] dynamic inputData)
        {
            int itemId = inputData.id;

            var item = await ItemHelper.IncludeFullItemTemplate(DbContext.ItemTemplate)
                .FirstAsync(i => i.Id == itemId);

            return Json(item);
        }

        public static string RemoveAccents(string input)
        {
            return new string(
                input
                    .Normalize(NormalizationForm.FormD)
                    .ToCharArray()
                    .Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    .ToArray());
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Create([FromBody] JObject inputData)
        {
            var itemData = inputData.Value<JObject>("item");

            var itemTemplate = new ItemTemplate();

            itemTemplate.CategoryId = itemData.Value<int>("category");
            itemTemplate.Name = itemData.Value<string>("name");
            itemTemplate.Data.Set(itemData.Value<JObject>("data"));
            itemTemplate.CleanName = RemoveAccents(itemTemplate.Name);

            var source = itemData.Value<string>("source");
            switch (source)
            {
                case "official":
                    if (!CurrentUser.Admin)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                case "private":
                case "community":
                    itemTemplate.SourceUserId = CurrentUser.Id;
                    itemTemplate.SourceUserNameCache = CurrentUser.DisplayName;
                    break;
                default:
                    return Error(HttpStatusCode.BadRequest, "invalid_source");
            }
            itemTemplate.Source = source;

            await DbContext.AddAsync(itemTemplate);
            await DbContext.SaveChangesAsync();

            #region modifiers

            var newItemEffects = itemData.Value<JArray>("modifiers");

            foreach (JToken jItemEffect in newItemEffects)
            {
                var itemEffectData = jItemEffect.ToObject<JObject>();

                var itemEffect = new ItemTemplateModifier();
                itemEffect.ItemTemplateId = itemTemplate.Id;
                itemEffect.StatName = itemEffectData.Value<string>("stat");
                itemEffect.Value = itemEffectData.Value<int>("value");
                itemEffect.Type = itemEffectData.Value<string>("type");
                itemEffect.Special = itemEffectData.Value<JArray>("special")?.ToString(Formatting.None);
                itemEffect.RequireJobId = itemEffectData.Value<int?>("job");
                itemEffect.RequireOriginId = itemEffectData.Value<int?>("origin");
                itemTemplate.ItemTemplateModifiers.Add(itemEffect);
            }

            #endregion

            #region skills/unskills

            var newItemSkills = itemData.Value<JArray>("skills") ?? new JArray();

            foreach (JToken jItemSkill in newItemSkills)
            {
                var itemSkillData = jItemSkill.ToObject<JObject>();

                var itemSkill = new ItemTemplateSkill();
                itemSkill.ItemTemplateId = itemTemplate.Id;
                itemSkill.SkillId = itemSkillData.Value<int>("id");
                itemTemplate.ItemTemplateSkills.Add(itemSkill);
            }


            var newItemUnskills = itemData.Value<JArray>("unskills") ?? new JArray();

            foreach (JToken jItemUnskill in newItemUnskills)
            {
                var itemUnskillData = jItemUnskill.ToObject<JObject>();

                var itemUnskill = new ItemTemplateUnskill();
                itemUnskill.ItemTemplateId = itemTemplate.Id;
                itemUnskill.SkillId = itemUnskillData.Value<int>("id");
                itemTemplate.ItemTemplateUnskills.Add(itemUnskill);
            }

            #endregion

            #region skill modifiers

            var newItemSkillModifiers = itemData.Value<JArray>("skillModifiers") ?? new JArray();

            foreach (JToken jItemSkillModifier in newItemSkillModifiers)
            {
                var itemSkillModifierData = jItemSkillModifier.ToObject<JObject>();

                var itemSkillModifier = new ItemTemplateSkillModifiers();
                itemSkillModifier.ItemTemplateId = itemTemplate.Id;
                itemSkillModifier.Value = itemSkillModifierData.Value<short>("value");
                itemTemplate.ItemTemplateSkillModifiers.Add(itemSkillModifier);
            }

            #endregion

            #region requirement

            var newItemRequirements = itemData.Value<JArray>("requirements") ?? new JArray();

            foreach (JToken jItemRequirement in newItemRequirements)
            {
                var itemRequirementData = jItemRequirement.ToObject<JObject>();

                var itemRequirement = new ItemTemplateRequirement();
                itemRequirement.ItemTemplateId = itemTemplate.Id;
                itemRequirement.StatName = itemRequirementData.Value<string>("stat");
                itemRequirement.Minvalue = itemRequirementData.Value<int?>("min");
                itemRequirement.Maxvalue = itemRequirementData.Value<int?>("max");
                itemTemplate.ItemTemplateRequirements.Add(itemRequirement);
            }

            #endregion

            #region slots

            var newItemSlots = itemData.Value<JArray>("slots") ?? new JArray();

            foreach (JToken jItemSlot in newItemSlots)
            {
                var itemSlotData = jItemSlot.ToObject<JObject>();

                var itemSlot = new ItemTemplateSlot();
                itemSlot.ItemTemplateId = itemTemplate.Id;
                itemSlot.SlotId = itemSlotData.Value<int>("id");
                itemTemplate.ItemTemplateSlot.Add(itemSlot);
            }

            #endregion

            await DbContext.SaveChangesAsync();
            await DbContext
                .Entry(itemTemplate)
                .ReloadAsync();

            return Json(itemTemplate);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Edit([FromBody] JObject inputData)
        {
            var itemData = inputData.Value<JObject>("item");
            var itemId = itemData.Value<int>("id");

            var itemTemplate = await DbContext.ItemTemplate
                .Include(i => i.ItemTemplateModifiers)
                .Include(i => i.ItemTemplateRequirements)
                .Include(i => i.ItemTemplateSkillModifiers)
                .Include(i => i.ItemTemplateSlot)
                .Include(i => i.ItemTemplateSkills)
                .Include(i => i.ItemTemplateUnskills)
                .FirstAsync(i => i.Id == itemId);

            switch (itemTemplate.Source)
            {
                case "official":
                    if (!CurrentUser.Admin)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                case "private":
                case "community":
                    if (itemTemplate.SourceUserId != CurrentUser.Id)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");                        
                    }
                    itemTemplate.SourceUserId = CurrentUser.Id;
                    itemTemplate.SourceUserNameCache = CurrentUser.DisplayName;
                    break;
                default:
                    return Error(HttpStatusCode.BadRequest, "invalid_source");
            }
            
            itemTemplate.CategoryId = itemData.Value<int>("category");
            itemTemplate.Name = itemData.Value<string>("name");
            itemTemplate.Data.Set(itemData.Value<JObject>("data"));
            itemTemplate.CleanName = RemoveAccents(itemTemplate.Name);

            var newSource = itemData.Value<string>("source");
            switch (newSource)
            {
                case "official":
                    if (!CurrentUser.Admin)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    itemTemplate.SourceUserId = null;
                    itemTemplate.SourceUserNameCache = null;
                    break;
                case "private":
                case "community":
                    itemTemplate.SourceUserId = CurrentUser.Id;
                    itemTemplate.SourceUserNameCache = CurrentUser.DisplayName;
                    break;
                default:
                    return Error(HttpStatusCode.BadRequest, "invalid_source");
            }
            itemTemplate.Source = newSource;

            #region modifiers

            var newItemEffects = itemData.Value<JArray>("modifiers") ?? new JArray();

            foreach (ItemTemplateModifier itemEffect in itemTemplate.ItemTemplateModifiers)
            {
                // ReSharper disable once SimplifyLinqExpression  https://youtrack.jetbrains.com/issue/RSRP-459519
                if (!newItemEffects.Any(j => j.ToObject<JObject>().Value<int>("id") == itemEffect.Id))
                {
                    DbContext.Remove(itemEffect);
                }
            }

            foreach (JToken jItemEffect in newItemEffects)
            {
                var itemEffectData = jItemEffect.ToObject<JObject>();
                if (itemEffectData.Value<long?>("id") != null)
                {
                    continue;
                }

                var itemEffect = new ItemTemplateModifier();
                itemEffect.ItemTemplateId = itemTemplate.Id;
                itemEffect.StatName = itemEffectData.Value<string>("stat");
                itemEffect.Value = itemEffectData.Value<int>("value");
                itemEffect.Type = itemEffectData.Value<string>("type");
                itemEffect.Special = itemEffectData.Value<JArray>("special")?.ToString(Formatting.None);
                itemEffect.RequireJobId = itemEffectData.Value<int?>("job");
                itemEffect.RequireOriginId = itemEffectData.Value<int?>("origin");
                itemTemplate.ItemTemplateModifiers.Add(itemEffect);
            }

            #endregion

            #region skills/unskills

            var newItemSkills = itemData.Value<JArray>("skills") ?? new JArray();

            foreach (ItemTemplateSkill itemSkill in itemTemplate.ItemTemplateSkills)
            {
                // ReSharper disable once SimplifyLinqExpression  https://youtrack.jetbrains.com/issue/RSRP-459519
                if (!newItemSkills.Any(j => j.ToObject<JObject>().Value<int>("id") == itemSkill.SkillId))
                {
                    DbContext.Remove(itemSkill);
                }
            }

            foreach (JToken jItemSkill in newItemSkills)
            {
                var itemSkillData = jItemSkill.ToObject<JObject>();
                if (itemTemplate.ItemTemplateSkills.Any(i => i.SkillId == itemSkillData.Value<int>("id")))
                {
                    continue;
                }

                var itemSkill = new ItemTemplateSkill();
                itemSkill.ItemTemplateId = itemTemplate.Id;
                itemSkill.SkillId = itemSkillData.Value<int>("id");
                itemTemplate.ItemTemplateSkills.Add(itemSkill);
            }


            var newItemUnskills = itemData.Value<JArray>("unskills") ?? new JArray();

            foreach (ItemTemplateUnskill itemUnskill in itemTemplate.ItemTemplateUnskills)
            {
                // ReSharper disable once SimplifyLinqExpression  https://youtrack.jetbrains.com/issue/RSRP-459519
                if (!newItemUnskills.Any(j => j.ToObject<JObject>().Value<int>("id") == itemUnskill.SkillId))
                {
                    DbContext.Remove(itemUnskill);
                }
            }

            foreach (JToken jItemUnskill in newItemUnskills)
            {
                var itemUnskillData = jItemUnskill.ToObject<JObject>();
                if (itemTemplate.ItemTemplateUnskills.Any(i => i.SkillId == itemUnskillData.Value<int>("id")))
                {
                    continue;
                }

                var itemUnskill = new ItemTemplateUnskill();
                itemUnskill.ItemTemplateId = itemTemplate.Id;
                itemUnskill.SkillId = itemUnskillData.Value<int>("id");
                itemTemplate.ItemTemplateUnskills.Add(itemUnskill);
            }

            #endregion

            #region skill modifiers

            JArray newItemSkillModifiers = itemData.Value<JArray>("skillModifiers") ?? new JArray();

            foreach (ItemTemplateSkillModifiers itemSkillModifier in itemTemplate.ItemTemplateSkillModifiers)
            {
                // ReSharper disable once SimplifyLinqExpression  https://youtrack.jetbrains.com/issue/RSRP-459519
                JToken itemSkillModifierData = newItemSkillModifiers.FirstOrDefault(j =>
                    j.ToObject<JObject>().Value<int>("id") == itemSkillModifier.SkillId);
                if (itemSkillModifierData == null)
                {
                    DbContext.Remove(itemSkillModifier);
                }
                else
                {
                    itemSkillModifier.Value = itemSkillModifierData.Value<short>("value");
                }
            }

            foreach (JToken jItemSkillModifier in newItemSkillModifiers)
            {
                var itemSkillModifierData = jItemSkillModifier.ToObject<JObject>();
                if (itemTemplate.ItemTemplateSkillModifiers.Any(i => i.SkillId == itemSkillModifierData.Value<int>("id")))
                {
                    continue;
                }

                var itemSkillModifier = new ItemTemplateSkillModifiers();
                itemSkillModifier.ItemTemplateId = itemTemplate.Id;
                itemSkillModifier.Value = itemSkillModifierData.Value<short>("value");
                itemTemplate.ItemTemplateSkillModifiers.Add(itemSkillModifier);
            }

            #endregion

            #region requirement

            var newItemRequirements = itemData.Value<JArray>("requirements") ?? new JArray();

            foreach (ItemTemplateRequirement itemRequirement in itemTemplate.ItemTemplateRequirements)
            {
                // ReSharper disable once SimplifyLinqExpression  https://youtrack.jetbrains.com/issue/RSRP-459519
                JToken itemRequirementData = newItemRequirements
                    .FirstOrDefault(j => j.ToObject<JObject>().Value<string>("stat") == itemRequirement.StatName);
                if (itemRequirementData == null)
                {
                    DbContext.Remove(itemRequirement);
                }
                else
                {
                    itemRequirement.Minvalue = itemRequirementData.Value<int?>("min");
                    itemRequirement.Maxvalue = itemRequirementData.Value<int?>("max");
                }
            }

            foreach (JToken jItemRequirement in newItemRequirements)
            {
                var itemRequirementData = jItemRequirement.ToObject<JObject>();
                if (itemTemplate.ItemTemplateRequirements.Any(i => i.StatName == itemRequirementData.Value<string>("stat")))
                {
                    continue;
                }

                var itemRequirement = new ItemTemplateRequirement();
                itemRequirement.ItemTemplateId = itemTemplate.Id;
                itemRequirement.StatName = itemRequirementData.Value<string>("stat");
                itemRequirement.Minvalue = itemRequirementData.Value<int?>("min");
                itemRequirement.Maxvalue = itemRequirementData.Value<int?>("max");
                itemTemplate.ItemTemplateRequirements.Add(itemRequirement);
            }

            #endregion

            #region slots

            var newItemSlots = itemData.Value<JArray>("slots") ?? new JArray();

            foreach (ItemTemplateSlot itemSlot in itemTemplate.ItemTemplateSlot)
            {
                // ReSharper disable once SimplifyLinqExpression  https://youtrack.jetbrains.com/issue/RSRP-459519
                JToken itemSlotData = newItemSlots
                    .FirstOrDefault(j => j.ToObject<JObject>().Value<int>("id") == itemSlot.SlotId);
                if (itemSlotData == null)
                {
                    DbContext.Remove(itemSlot);
                }
            }

            foreach (JToken jItemSlot in newItemSlots)
            {
                var itemSlotData = jItemSlot.ToObject<JObject>();
                if (itemTemplate.ItemTemplateSlot.Any(i => i.SlotId == itemSlotData.Value<int>("id")))
                {
                    continue;
                }

                var itemSlot = new ItemTemplateSlot();
                itemSlot.ItemTemplateId = itemTemplate.Id;
                itemSlot.SlotId = itemSlotData.Value<int>("id");
                itemTemplate.ItemTemplateSlot.Add(itemSlot);
            }

            #endregion

            await DbContext.SaveChangesAsync();
            await DbContext
                .Entry(itemTemplate)
                .ReloadAsync();

            return Json(itemTemplate);
        }

        public IActionResult GetGem([FromBody] dynamic inputData)
        {
            string type = inputData.type;
            int dice = inputData.dice;

            int categoryId;
            if (type == "raw")
            {
                categoryId = 18;
            }
            else
            {
                categoryId = 87;
            }

            var li = ItemHelper.IncludeFullItemTemplate(DbContext.ItemTemplate)
                .Where(i => i.CategoryId == categoryId).ToList();

            foreach (ItemTemplate itemTemplate in li)
            {
                if (itemTemplate.Data.ValueOrDefault<int>("diceDrop") == dice)
                {
                    return Json(itemTemplate);
                }
            }
            if (type == "raw")
            {
                categoryId = 87;
            }
            else
            {
                categoryId = 18;
            }

            li = ItemHelper.IncludeFullItemTemplate(DbContext.ItemTemplate)
                .Where(i => i.CategoryId == categoryId).ToList();

            foreach (ItemTemplate itemTemplate in li)
            {
                if (itemTemplate.Data.ValueOrDefault<int>("diceDrop") == dice)
                {
                    return Json(itemTemplate);
                }
            }

            return Json(null);
        }


        public IActionResult Search([FromBody] dynamic inputData)
        {
            string filter = inputData.filter;
            filter = filter.ToUpper();

            var currentUserId = CurrentUser?.Id;
            var items = DbContext.ItemTemplate
                .Where(item => item.CleanName.ToUpper() == filter
                               && (item.Source != "private" || item.SourceUserId == currentUserId))
                .OrderBy(item => item.Source)
                .Take(10)
                .ToList();

            var ids = items.Select(i => i.Id);
            if (items.Count < 10)
            {
                var addItems = DbContext.ItemTemplate
                    .Where(item => item.CleanName.ToUpper().Contains(filter) && !ids.Contains(item.Id)
                                   && (item.Source != "private" || item.SourceUserId == currentUserId))
                    .OrderBy(item => item.Source)
                    .Take(10 - items.Count)
                    .ToList();

                items.AddRange(addItems);
            }

            if (items.Count < 10)
            {
                var addItems = DbContext.ItemTemplate
                    .Where(item => item.CleanName.Replace("'", "")
                                       .Replace(" ", "")
                                       .ToUpper().Contains(filter.Replace("'", "").Replace(" ", "")) &&
                                   !ids.Contains(item.Id)
                                   && (item.Source != "private" || item.SourceUserId == currentUserId))
                    .OrderBy(item => item.Source)
                    .Take(10 - items.Count)
                    .ToList();

                items.AddRange(addItems);
            }

            JsonConvert.SerializeObject(items);

            return Json(items);
        }
    }
}