﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Models;

namespace Naheulbook.Controllers
{
    public class MiscController : NaheulbookController
    {
        public MiscController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<JsonResult> Icons()
        {
            var icons = await DbContext.Icon.Select(i => i.Name).ToListAsync();

            return Json(icons);
        }

        public async Task<IActionResult> GodsList()
        {
            var gods = await DbContext.God.ToListAsync();

            return Json(gods);
        }

        public async Task<JsonResult> Calendar()
        {
            var icons = await DbContext.Calendar
                .OrderBy(c => c.StartDay)
                .ToListAsync();

            return Json(icons);
        }
    }
}