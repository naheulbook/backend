﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;
using Naheulbook.Utils.Date;
using Naheulbook.Utils.Item;
using Naheulbook.Utils.Modifier;
using Naheulbook.WebSocket;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class GroupController : NaheulbookController
    {
        public IWebsocketManager WebsocketManager { get; set; }
        public ItemHelper ItemHelper { get; set; }

        public GroupController(NaheulbookDbContext dbContext, IWebsocketManager websocketManager, ItemHelper itemHelper)
            : base(dbContext)
        {
            WebsocketManager = websocketManager;
            ItemHelper = itemHelper;
        }

        #region Event

        [LoggedUserFilter]
        public async Task<IActionResult> LoadEvents([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var events = await DbContext.Event
                .Where(e => e.GroupId == groupId)
                .ToListAsync();

            return Json(events);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateEvent([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var eventData = inputData.Value<JObject>("event");
            var ev = new Event
            {
                Name = eventData.Value<string>("name"),
                Description = eventData.Value<string>("description"),
                Timestamp = eventData.Value<long>("timestamp"),
                GroupId = groupId
            };

            DbContext.Add(ev);
            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("addEvent", ObservedElementType.Group, group.Id, ev);

            return Json(ev);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> DeleteEvent([FromBody] JObject inputData)
        {
            var eventId = inputData.Value<int>("eventId");

            Event ev = DbContext.Event
                .Include(e => e.Group)
                .First(e => e.Id == eventId);
            if (ev.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            DbContext.Remove(ev);
            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("deleteEvent", ObservedElementType.Group, ev.Group.Id, ev);

            return Json(ev);
        }

        #endregion

        #region  Loot

        [LoggedUserFilter]
        public async Task<IActionResult> LoadLoots([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var loots = await DbContext.Loot
                .Include(l => l.Monsters)
                .ThenInclude(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .Include(l => l.Items)
                .ThenInclude(i => i.ItemTemplate)
                .Where(l => l.GroupId == groupId)
                .OrderByDescending(l => l.Created)
                .ToListAsync();

            return Json(loots);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateLoot([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            var name = inputData.Value<string>("name");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var loot = new Loot
            {
                Name = name,
                GroupId = groupId
            };
            DbContext.Add(loot);

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("addLoot", ObservedElementType.Group, group.Id, loot);

            return Json(loot);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> DeleteLoot([FromBody] JObject inputData)
        {
            var lootId = inputData.Value<int>("lootId");

            Loot loot = DbContext.Loot
                .Include(l => l.Group)
                .ThenInclude(g => g.Character)
                .First(l => l.Id == lootId);
            if (loot.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            WebsocketManager.SendMessage("deleteLoot", ObservedElementType.Group, loot.Group.Id, loot);

            foreach (Character character in loot.Group.Character)
            {
                WebsocketManager.SendMessage("hideLoot", ObservedElementType.Character, character.Id, loot);
            }

            DbContext.Remove(loot);
            await DbContext.SaveChangesAsync();

            return Json(loot);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> UpdateLoot([FromBody] JObject inputData)
        {
            var lootData = inputData.Value<JObject>("loot");
            var lootId = lootData.Value<int>("id");

            Loot loot = DbContext.Loot
                .Include(l => l.Group)
                .ThenInclude(g => g.Character)
                .First(l => l.Id == lootId);
            if (loot.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }
            loot.VisibleForPlayer = lootData.Value<bool>("visibleForPlayer");

            WebsocketManager.SendMessage("updateLoot", ObservedElementType.Loot, loot.Id, loot);
            if (loot.VisibleForPlayer)
            {
                Loot fullLoot = await DbContext.Loot
                    .Include(l => l.Monsters)
                    .ThenInclude(m => m.Items)
                    .ThenInclude(i => i.ItemTemplate)
                    .Include(l => l.Items)
                    .ThenInclude(i => i.ItemTemplate)
                    .FirstAsync(l => l.Id == lootId);

                foreach (Character character in loot.Group.Character)
                {
                    WebsocketManager.SendMessage("showLoot", ObservedElementType.Character, character.Id, fullLoot);
                }
            }
            else
            {
                foreach (Character character in loot.Group.Character)
                {
                    WebsocketManager.SendMessage("hideLoot", ObservedElementType.Character, character.Id, loot);
                }
            }
            await DbContext.SaveChangesAsync();

            return Json(loot);
        }

        #endregion

        [LoggedUserFilter]
        public IActionResult ListActiveCharacters([FromBody] JObject inputData)
        {
            int characterId = inputData.Value<int>("characterId");

            Character character = DbContext.Character
                .Include(c => c.Group)
                .First(c => c.Id == characterId);

            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            var characters = DbContext.Character
                .Include(c => c.Group)
                .Where(c => c.GroupId == character.GroupId && c.Active && c.Id != characterId);

            var result = new List<object>();
            foreach (Character c in characters)
            {
                result.Add(new {Isnpc = c.IsNpc, c.Name, c.Id});
            }

            return Json(result);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> KillMonster([FromBody] dynamic inputData)
        {
            int monsterId = inputData.monsterId;

            Monster monster = DbContext.Monster
                .Include(m => m.Group)
                .First(m => m.Id == monsterId);

            if (monster.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            monster.Dead = DateTime.Now;

            if (monster.Group.CurrentCombatLootId != null)
            {
                monster.LootId = monster.Group.CurrentCombatLootId;
                await ItemHelper.IncludeFullItemTemplate(DbContext.Entry(monster).Collection(m => m.Items).Query()).LoadAsync();
                WebsocketManager.SendMessage("addMonster", ObservedElementType.Loot, monster.LootId.Value, monster);
            }

            await DbContext.SaveChangesAsync();

            return Json(monster);
        }

        [LoggedUserFilter]
        public IActionResult DeleteMonster([FromBody] dynamic inputData)
        {
            int monsterId = inputData.monsterId;

            Monster monster = DbContext.Monster
                .Include(m => m.Group)
                .First(m => m.Id == monsterId);

            if (monster.Group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            DbContext.Remove(monster);
            DbContext.SaveChanges();

            return Json(monster);
        }

        [LoggedUserFilter]
        public IActionResult History([FromBody] dynamic inputData)
        {
            int groupId = inputData.groupId;
            int page = inputData.page;

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var history = DbContext.GroupHistory
                .Where(gh => gh.GroupId == groupId)
                .OrderByDescending(gh => gh.Id)
                .Skip(page * 40)
                .Take(40);

            return Json(history);
        }

        [LoggedUserFilter]
        public IActionResult AddLog([FromBody] dynamic inputData)
        {
            int groupId = inputData.groupId;
            bool isGm = inputData.is_gm;
            string info = inputData.info;

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var gh = new GroupHistory
            {
                UserId = CurrentUser.Id,
                Action = "EVENT_RP",
                Date = DateTime.Now,
                Gm = isGm,
                Info = info,
                GroupId = groupId
            };

            DbContext.GroupHistory.Add(gh);
            DbContext.SaveChanges();

            return Json(null);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> NextFighter([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            var fighterIndex = inputData.Value<int>("fighterIndex");

            var group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var data = new JObject();
            if (group.Data != null)
            {
                data = JObject.Parse(group.Data);
            }
            data["currentFighterIndex"] = fighterIndex;
            group.Data = data.ToString(Formatting.None);

            await DbContext.SaveChangesAsync();

            return Json("OK");
        }

        [LoggedUserFilter]
        public async Task<IActionResult> SaveChangedTime([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            var modifiersDurationUpdated = inputData.Value<JArray>("modifiersDurationUpdated");

            var group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            foreach (var updatedFighter in modifiersDurationUpdated)
            {
                var updatedFighterData = updatedFighter.ToObject<JObject>();
                if (updatedFighterData.Value<bool>("isMonster"))
                {
                    long monsterId = updatedFighterData.Value<long>("monsterId");
                    var monster = await DbContext.Monster.FindAsync(monsterId);
                    if (monster.Group.MasterId != CurrentUser.Id)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_master");
                    }

                    var changes = updatedFighterData.Value<JArray>("changes");
                    foreach (var change in changes)
                    {
                        var changeData = change.ToObject<JObject>();
                        var changeType = changeData.Value<string>("type");
                        switch (changeType)
                        {
                            case "modifier":
                            {
                                var modifier = changeData.Value<JObject>("modifier").ToObject<ActiveStatsModifier>();
                                var monsterModifiers = monster.Modifiers;
                                var currentModifier = monsterModifiers.FirstOrDefault(m => m.Id == modifier.Id);
                                if (!modifier.Active && !modifier.Reusable)
                                {
                                    monsterModifiers.Remove(currentModifier);
                                    WebsocketManager.SendMessage("removeModifier", ObservedElementType.Monster, monsterId, currentModifier);
                                }
                                else
                                {
                                    currentModifier.Active = modifier.Active;
                                    currentModifier.CurrentLapCount = modifier.CurrentLapCount;
                                    currentModifier.CurrentCombatCount = modifier.CurrentCombatCount;
                                    currentModifier.CurrentTimeDuration = modifier.CurrentTimeDuration;
                                    WebsocketManager.SendMessage("updateModifier", ObservedElementType.Monster, monsterId, currentModifier);
                                }
                                monster.Modifiers = monsterModifiers;


                                break;
                            }
                            case "item":
                            {
                                var modifier = changeData.Value<JObject>("modifier").ToObject<ActiveStatsModifier>();
                                var itemId = changeData.Value<long>("itemId");
                                var modifierIdx = changeData.Value<int>("modifierIdx");

                                var item = await DbContext.Item.FirstAsync(i => i.Id == itemId);
                                if (item.MonsterId != monsterId)
                                {
                                    return Error(HttpStatusCode.Forbidden, "not_master");
                                }

                                var itemModifiers = item.Modifiers;
                                var currentModifier = itemModifiers.FirstOrDefault(m => m.Id == modifier.Id);
                                if (!modifier.Active && !modifier.Reusable)
                                {
                                    itemModifiers.Remove(currentModifier);
                                }
                                else
                                {
                                    currentModifier.Active = modifier.Active;
                                    currentModifier.CurrentLapCount = modifier.CurrentLapCount;
                                    currentModifier.CurrentCombatCount = modifier.CurrentCombatCount;
                                    currentModifier.CurrentTimeDuration = modifier.CurrentTimeDuration;
                                }
                                item.Modifiers = itemModifiers;

                                WebsocketManager.SendMessage("updateItemModifiers",
                                    ObservedElementType.Monster,
                                    item.Monster.Id,
                                    item);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    long characterId = updatedFighterData.Value<long>("characterId");
                    var character = await DbContext.Character
                        .Include(c => c.CharacterModifiers)
                        .FirstAsync(c => c.Id == characterId);
                    if (!character.IsEditableBy(CurrentUser))
                    {
                        return Error(HttpStatusCode.Forbidden, "not_master");
                    }

                    var changes = updatedFighterData.Value<JArray>("changes");
                    foreach (var change in changes)
                    {
                        var changeData = change.ToObject<JObject>();
                        var changeType = changeData.Value<string>("type");
                        switch (changeType)
                        {
                            case "modifier":
                            {
                                var modifier = changeData.Value<JObject>("modifier").ToObject<ActiveStatsModifier>();
                                var characterModifiers = character.CharacterModifiers;
                                var currentModifier = characterModifiers.FirstOrDefault(m => m.Id == modifier.Id);
                                currentModifier.Active = modifier.Active;
                                if (!modifier.Active && !modifier.Reusable)
                                {
                                    currentModifier.CharacterId = null;
                                    currentModifier.Reusable = false;
                                    currentModifier.Active = false;
                                    var characterHistory = new CharacterHistory(character, CurrentUser, "REMOVE_MODIFIER")
                                    {
                                        ModifierId = modifier.Id
                                    };
                                    DbContext.CharacterHistory.Add(characterHistory);
                                    WebsocketManager.SendMessage("removeModifier", ObservedElementType.Character, character.Id, currentModifier);
                                }
                                else
                                {
                                    currentModifier.CurrentLapCount = modifier.CurrentLapCount;
                                    currentModifier.CurrentCombatCount = modifier.CurrentCombatCount;
                                    currentModifier.CurrentTimeDuration = modifier.CurrentTimeDuration;
                                    WebsocketManager.SendMessage("updateModifier", ObservedElementType.Character, character.Id, currentModifier);
                                }

                                break;
                            }
                            case "itemModifier":
                            {
                                var modifier = changeData.Value<JObject>("modifier").ToObject<ActiveStatsModifier>();
                                var itemId = changeData.Value<long>("itemId");
                                var modifierIdx = changeData.Value<int>("modifierIdx");

                                var item = await DbContext.Item.FirstAsync(i => i.Id == itemId);
                                if (item.CharacterId != characterId)
                                {
                                    return Error(HttpStatusCode.Forbidden, "not_master");
                                }

                                var itemModifiers = item.Modifiers;
                                var currentModifier = itemModifiers.FirstOrDefault(m => m.Id == modifier.Id);
                                if (!modifier.Active && !modifier.Reusable)
                                {
                                    itemModifiers.Remove(currentModifier);
                                }
                                else
                                {
                                    currentModifier.Active = modifier.Active;
                                    currentModifier.CurrentLapCount = modifier.CurrentLapCount;
                                    currentModifier.CurrentCombatCount = modifier.CurrentCombatCount;
                                    currentModifier.CurrentTimeDuration = modifier.CurrentTimeDuration;
                                }
                                item.Modifiers = itemModifiers;

                                WebsocketManager.SendMessage("updateItemModifiers",
                                    ObservedElementType.Character,
                                    item.Character.Id,
                                    item);
                                break;
                            }
                            case "itemLifetime":
                            {
                                var lifetime = changeData.Value<JObject>("lifetime");
                                var itemId = changeData.Value<long>("itemId");
                                var modifierIdx = changeData.Value<int>("modifierIdx");

                                var item = await DbContext.Item.FirstAsync(i => i.Id == itemId);
                                if (item.CharacterId != characterId)
                                {
                                    return Error(HttpStatusCode.Forbidden, "not_master");
                                }
                                item.Data["lifetime"] = lifetime;

                                WebsocketManager.SendMessage("updateItem",
                                    ObservedElementType.Character,
                                    item.Character.Id,
                                    item);
                                break;
                            }
                        }
                    }
                }
            }

            await DbContext.SaveChangesAsync();

            return Json("OK");
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Edit([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            string key = inputData.Value<string>("key");

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var data = new JObject();
            if (group.Data != null)
            {
                data = JObject.Parse(group.Data);
            }

            switch (key)
            {
                case "inCombat":
                    var inCombat = inputData.Value<bool>("value");

                    if (!data.Value<bool>(key) && inCombat)
                    {
                        DbContext.GroupHistory.Add(new GroupHistory(group, CurrentUser, "START_COMBAT", false));
                        await StartCombat(group);
                    }
                    else if (data.Value<bool>(key) && !inCombat)
                    {
                        DbContext.GroupHistory.Add(new GroupHistory(group, CurrentUser, "END_COMBAT", false));
                        UpdateCharacterForEndOfCombat(groupId);
                    }

                    data[key] = inCombat;
                    break;
                case "location":
                    group.LocationId = inputData.Value<int>("value");
                    break;
                case "mankdebol":
                    DbContext.GroupHistory.Add(new GroupHistory(group, CurrentUser, "MANKDEBOL", true, new
                    {
                        newValue = inputData.Value<int>("value"),
                        oldValue = data.Value<int>("mankdebol")
                    }));
                    data["mankdebol"] = inputData.Value<int>("value");
                    break;
                case "debilibeuk":
                    DbContext.GroupHistory.Add(new GroupHistory(group, CurrentUser, "DEBILIBEUK", true, new
                    {
                        newValue = inputData.Value<int>("value"),
                        oldValue = data.Value<int>("debilibeuk")
                    }));
                    data["debilibeuk"] = inputData.Value<int>("value");
                    break;
                case "date":
                    var newDate = inputData.Value<JObject>("value").ToObject<NhbkDate>();
                    NhbkDate oldDate = null;
                    if (data.GetValue("date") != null)
                    {
                        oldDate = data.Value<JObject>("date").ToObject<NhbkDate>();
                    }
                    DbContext.GroupHistory.Add(new GroupHistory(group, CurrentUser, "CHANGE_DATE", false, new
                    {
                        newValue = newDate,
                        oldValue = oldDate
                    }));
                    data["date"] = JObject.FromObject(newDate);
                    break;
                default:
                    throw new NotImplementedException();
            }

            WebsocketManager.SendMessage("changeData", ObservedElementType.Group, group.Id, new {key, value = inputData["value"]});

            group.Data = JsonConvert.SerializeObject(data);
            await DbContext.SaveChangesAsync();
            return Json(data);
        }

        private async Task StartCombat(Group group)
        {
            var loot = new Loot
            {
                Name = "Combat",
                GroupId = @group.Id
            };
            DbContext.Add(loot);

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("addLoot", ObservedElementType.Group, group.Id, loot);

            group.CurrentCombatLootId = loot.Id;
        }

        private void UpdateCharacterForEndOfCombat(int groupId)
        {
            var characters = DbContext.Character
                .Include(c => c.CharacterModifiers)
                .Where(c => c.GroupId == groupId && c.Active)
                .ToList();
            foreach (Character character in characters)
            {
                foreach (CharacterModifier modifier in character.CharacterModifiers)
                {
                    if (!modifier.Active || modifier.CombatCount == null)
                        continue;

                    bool toRemove;
                    switch (modifier.DurationType)
                    {
                        case "combat":
                            modifier.CurrentCombatCount--;
                            toRemove = modifier.CurrentCombatCount <= 0;
                            break;
                        case "lap":
                            modifier.CurrentLapCount = 0;
                            toRemove = true;
                            break;
                        default:
                            continue;
                    }

                    if (toRemove)
                    {
                        if (modifier.Reusable)
                        {
                            modifier.Active = false;
                        }
                        else
                        {
                            DbContext.Remove(modifier);
                        }
                    }

                    WebsocketManager.SendMessage("updateModifier", ObservedElementType.Character, character.Id, modifier);
                }
            }
        }

        [LoggedUserFilter]
        public IActionResult AddTime([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            var dateOffset = inputData.Value<JObject>("dateOffset").ToObject<NhbkDateOffset>();

            Group group = DbContext.Group.First(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var data = new JObject();
            if (group.Data != null)
            {
                data = JObject.Parse(group.Data);
            }

            var date = data.Value<JObject>("date").ToObject<NhbkDate>();
            date.Add(dateOffset);
            data["date"] = JObject.FromObject(date);
            group.Data = JsonConvert.SerializeObject(data);
            DbContext.GroupHistory.Add(new GroupHistory(group, CurrentUser, "ADD_TIME", false, new
            {
                date,
                offset = dateOffset
            }));

            WebsocketManager.SendMessage("changeData", ObservedElementType.Group, group.Id, new {key = "date", value = date});

            DbContext.SaveChanges();

            return Json(data);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> KickCharacter([FromBody] JObject inputData)
        {
            var groupId = inputData.Value<int>("groupId");
            var characterId = inputData.Value<int>("characterId");

            var group = await DbContext.Group.FirstAsync(g => g.Id == groupId);
            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_master");
            }

            var character = await DbContext.Character
                .FirstAsync(c => c.GroupId == groupId && c.Id == characterId);
            character.GroupId = null;
            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("removeCharacter", ObservedElementType.Group, group.Id, new {characterId});

            DbContext.SaveChanges();

            return Json(characterId);
        }

    }
}
