﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class EffectController : NaheulbookController
    {
        public EffectController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<IActionResult> ListTypes()
        {
            var monsters = await DbContext.EffectType
                .Include(t => t.Categories)
                .ToListAsync();

            return Json(monsters);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateCategory([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }

            var category = new EffectCategory();
            category.Name = inputData.Value<string>("name");
            category.TypeId = inputData.Value<int>("typeId");

            DbContext.EffectCategory.Add(category);
            await DbContext.SaveChangesAsync();

            return Json(category);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateType([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }

            var type = new EffectType();
            type.Name = inputData.Value<string>("name");

            DbContext.EffectType.Add(type);
            await DbContext.SaveChangesAsync();

            return Json(type);
        }

        public IActionResult List([FromBody] dynamic inputData)
        {
            int categoryId = inputData.categoryId;

            var categories = DbContext.Effect
                .Where(effect => effect.CategoryId == categoryId)
                .Include(effect => effect.EffectModifier)
                .ToList();
            return Json(categories);
        }

        public IActionResult Search([FromBody] dynamic inputData)
        {
            string filter = inputData.filter;
            if (string.IsNullOrWhiteSpace(filter))
            {
                return Json(new string[0]);
            }
            filter = filter.ToUpper();

            var effects = DbContext.Effect
                .Where(effect => effect.Name.ToUpper().Contains(filter))
                .Include(e => e.EffectModifier)
                .Take(10)
                .ToList();

            return Json(effects);
        }

        [LoggedUserFilter]
        public IActionResult Detail([FromBody] dynamic inputData)
        {
            int effectId = inputData.effectId;

            Effect effect = DbContext.Effect
                .Include(e => e.EffectModifier)
                .First(e => e.Id == effectId);

            return Json(effect);
        }

        [LoggedUserFilter]
        public IActionResult Create([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }

            var effectData = inputData.Value<JObject>("effect");
            var effectId = effectData.Value<int>("id");

            var effect = new Effect();

            effect.CombatCount = effectData.Value<int?>("combatCount");
            effect.CategoryId = effectData.Value<int>("categoryId");
            effect.Description = effectData.Value<string>("description");
            effect.Duration = effectData.Value<string>("duration");
            effect.Dice = effectData.Value<short?>("dice");
            effect.Name = effectData.Value<string>("name");
            effect.TimeDuration = effectData.Value<int?>("timeDuration");

            var modifiersData = effectData.Value<JArray>("modifiers").Select(j => j.ToObject<JObject>()).ToList();
            foreach (JObject modifierData in modifiersData)
            {
                var stat = modifierData.Value<string>("stat");
                var modifier = new EffectModifier
                {
                    StatName = stat,
                    Value = modifierData.Value<short>("value"),
                    Type = modifierData.Value<string>("type")
                };
                effect.EffectModifier.Add(modifier);
            }

            DbContext.Add(effect);
            DbContext.SaveChanges();

            return Json(effect);
        }

        [LoggedUserFilter]
        public IActionResult Edit([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }

            var effectData = inputData.Value<JObject>("effect");
            var effectId = effectData.Value<int>("id");

            Effect effect = DbContext.Effect
                .Include(e => e.EffectModifier)
                .First(e => e.Id == effectId);

            effect.CombatCount = effectData.Value<int?>("combatCount");
            effect.CategoryId = effectData.Value<int>("categoryId");
            effect.Description = effectData.Value<string>("description");
            effect.Duration = effectData.Value<string>("duration");
            effect.Dice = effectData.Value<short?>("dice");
            effect.Name = effectData.Value<string>("name");
            effect.TimeDuration = effectData.Value<int?>("timeDuration");
            effect.DurationType = effectData.Value<string>("durationType");

            var modifiersData = effectData.Value<JArray>("modifiers").Select(j => j.ToObject<JObject>()).ToList();
            foreach (EffectModifier modifier in effect.EffectModifier)
            {
                JObject modifierData = modifiersData.FirstOrDefault(m => m.Value<string>("stat") == modifier.StatName);
                if (modifierData != null)
                {
                    modifier.Type = modifierData.Value<string>("type");
                    modifier.Value = modifierData.Value<short>("value");
                }
                else
                {
                    DbContext.Remove(modifier);
                }
            }

            foreach (JObject modifierData in modifiersData)
            {
                var stat = modifierData.Value<string>("stat");
                if (effect.EffectModifier.Any(m => m.StatName == stat))
                {
                    continue;
                }
                var modifier = new EffectModifier
                {
                    StatName = stat,
                    Value = modifierData.Value<short>("value"),
                    Type = modifierData.Value<string>("type")
                };
                effect.EffectModifier.Add(modifier);
            }

            DbContext.SaveChanges();

            return Json(effect);
        }
    }
}