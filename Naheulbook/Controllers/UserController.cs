﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Controllers
{
    public class UserController : NaheulbookController
    {
        private readonly IConfiguration _configuration;
        private ILogger<UserController> _logger;

        public UserController(NaheulbookDbContext dbContext, IConfiguration configuration,
            ILogger<UserController> logger) : base(dbContext)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public IActionResult Logged()
        {
            return Json(CurrentUser);
        }

        public IActionResult Logout()
        {
            SetCurrentSession(null);
            ForgetUserSession();
            return Json(null);
        }

        [LoggedUserFilter]
        public IActionResult SearchUser([FromBody] dynamic inputData)
        {
            string filter = inputData.filter;
            filter = filter.ToUpper().Trim();

            var users = DbContext.User
                .Where(user => user.DisplayName != null && user.DisplayName.ToUpper().Trim().Contains(filter))
                .Take(7)
                .ToList();

            return Json(users.Select(u => new
            {
                id = u.Id,
                displayName = u.DisplayName
            }));
        }

        /*
         * Authentification via Facebook api.
         * 1) Check if user is already logged, if then, return OK and send data about his data
         * 2) Retrieving request parameters
         * 3) Check that the LoginToken is valid
         * 4) Retrieve with facebook api, the access token, used for next request to load client data
         * 5) Load facebook profile information
         * 6) If an accound does not exists, create it
         * 7) Send user data
         */
        public async Task<IActionResult> FbLogin([FromBody] dynamic data)
        {
            // 1)

            User user = GetCurrentUser();
            if (user?.FbId != null)
            {
                return Json(user);
            }

            // 2)

            string code = data.code;
            string loginToken = data.loginToken;
            string redirectUri = data.redirectUri;

            // 3)

            SessionData sessionData = GetCurrentSession();
            if (loginToken != sessionData.LoginToken)
            {
                Response.StatusCode = 400;
                return Json(null);
            }

            // 4)

            var appKey = _configuration.GetValue<string>("Authentication:Facebook:AppId");
            var appSecret = _configuration.GetValue<string>("Authentication:Facebook:AppSecret");

            var requestUri = new StringBuilder();
            requestUri.AppendFormat("https://graph.facebook.com/v2.12/oauth/access_token");
            requestUri.AppendFormat("?redirect_uri={0}", Uri.EscapeUriString(redirectUri));
            requestUri.AppendFormat("&code={0}", Uri.EscapeUriString(code));
            requestUri.AppendFormat("&client_id={0}", appKey);
            requestUri.AppendFormat("&client_secret={0}", appSecret);

            dynamic profileData;

            using (HttpClient client = new HttpClient())
            {
                string accessToken = null;
                using (HttpResponseMessage response = await client.GetAsync(requestUri.ToString()))
                using (HttpContent content = response.Content)
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        Response.StatusCode = 500;
                        return Json(null);
                    }

                    var result = await content.ReadAsStringAsync();
                    dynamic facebookData = JsonConvert.DeserializeObject<dynamic>(result);
                    accessToken = facebookData.access_token;
                }


                // 5)

                var profileRequestUri = new StringBuilder();
                profileRequestUri.AppendFormat("https://graph.facebook.com/me?access_token={0}",
                    Uri.EscapeUriString(accessToken));

                using (HttpResponseMessage response = await client.GetAsync(profileRequestUri.ToString()))
                using (HttpContent content = response.Content)
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        Response.StatusCode = 500;
                        return Json(null);
                    }

                    var result = await content.ReadAsStringAsync();
                    profileData = JsonConvert.DeserializeObject<dynamic>(result);
                }
            }

            // 6)

            string facebookId = profileData.id;
            if (user != null)
            {
                user.FbId = facebookId;
                await DbContext.SaveChangesAsync();
                return Json(user);
            }

            user = await DbContext.User.FirstOrDefaultAsync(u => u.FbId == facebookId);
            if (user == null)
            {
                user = new User
                {
                    Admin = false,
                    DisplayName = profileData.name,
                    FbId = facebookId
                };

                DbContext.User.Add(user);
                await DbContext.SaveChangesAsync();
            }

            // 7)

            sessionData.UserId = user.Id;
            SetCurrentSession(sessionData);
            RemeberMeSession(sessionData);

            return Json(user);
        }


        public async Task<IActionResult> GoogleLogin([FromBody] dynamic data)
        {
            const string tokenApiRequestUri = "https://www.googleapis.com/oauth2/v4/token";
            const string accessApiRequestUri = "https://www.googleapis.com/plus/v1/people/me";

            // 1)

            User user = GetCurrentUser();
            if (user?.GoogleId != null)
            {
                return Json(user);
            }

            // 2)

            string code = data.code;
            string loginToken = data.loginToken;
            string redirectUri = data.redirectUri;

            // 3)

            SessionData sessionData = GetCurrentSession();
            if (loginToken != sessionData.LoginToken)
            {
                Response.StatusCode = 400;
                return Json(null);
            }

            // 4)

            var appKey = _configuration.GetValue<string>("Authentication:Google:AppId");
            var appSecret = _configuration.GetValue<string>("Authentication:Google:AppSecret");

            var requestArgs = new Dictionary<string, string>
            {
                ["redirect_uri"] = redirectUri,
                ["code"] = code,
                ["grant_type"] = "authorization_code",
                ["client_id"] = appKey,
                ["client_secret"] = appSecret
            };

            dynamic profileData;

            using (HttpClient client = new HttpClient())
            {
                string accessToken = null;
                using (
                    HttpResponseMessage response =
                        await client.PostAsync(tokenApiRequestUri, new FormUrlEncodedContent(requestArgs)))
                using (HttpContent content = response.Content)
                {
                    var result = await content.ReadAsStringAsync();
                    _logger.LogDebug("Result from google token api: " + result);

                    if (!response.IsSuccessStatusCode)
                    {
                        Response.StatusCode = 500;
                        return Json(result);
                    }
                    dynamic googleData = JsonConvert.DeserializeObject<dynamic>(result);
                    accessToken = googleData.access_token;
                }


                // 5)

                var profileRequestUri = $"{accessApiRequestUri}?access_token={Uri.EscapeUriString(accessToken)}";
                using (HttpResponseMessage response = await client.GetAsync(profileRequestUri))
                using (HttpContent content = response.Content)
                {
                    var result = await content.ReadAsStringAsync();
                    _logger.LogDebug("Result from google api: " + result);
                    profileData = JsonConvert.DeserializeObject<dynamic>(result);

                    if (!response.IsSuccessStatusCode)
                    {
                        Response.StatusCode = 500;
                        return Json(profileData);
                    }
                }
            }

            // 6)

            string googleId = profileData.id;
            if (user != null)
            {
                user.GoogleId = googleId;
                await DbContext.SaveChangesAsync();
                return Json(user);
            }

            user = DbContext.User.FirstOrDefault(u => u.GoogleId == googleId);
            if (user == null)
            {
                user = new User
                {
                    Admin = false,
                    DisplayName = profileData.displayName,
                    GoogleId = googleId
                };

                DbContext.User.Add(user);
                await DbContext.SaveChangesAsync();
            }

            // 7)

            sessionData.UserId = user.Id;
            SetCurrentSession(sessionData);
            RemeberMeSession(sessionData);

            return Json(user);
        }

        public async Task<IActionResult> LiveLogin([FromBody] dynamic data)
        {
            const string tokenApiRequestUri = "https://login.live.com/oauth20_token.srf";
            const string accessApiRequestUri = "https://apis.live.net/v5.0/me";

            // 1)

            User user = GetCurrentUser();
            if (user?.LiveId != null)
            {
                return Json(user);
            }

            // 2)

            string code = data.code;
            string loginToken = data.loginToken;
            string redirectUri = data.redirectUri;

            // 3)

            SessionData sessionData = GetCurrentSession();
            if (loginToken != sessionData.LoginToken)
            {
                Response.StatusCode = 400;
                return Json(null);
            }

            // 4)

            var appKey = _configuration.GetValue<string>("Authentication:Live:AppId");
            var appSecret = _configuration.GetValue<string>("Authentication:Live:AppSecret");

            var requestArgs = new Dictionary<string, string>
            {
                ["redirect_uri"] = redirectUri,
                ["code"] = code,
                ["grant_type"] = "authorization_code",
                ["client_id"] = appKey,
                ["client_secret"] = appSecret
            };

            dynamic profileData;

            using (HttpClient client = new HttpClient())
            {
                string accessToken = null;
                using (
                    HttpResponseMessage response =
                        await client.PostAsync(tokenApiRequestUri, new FormUrlEncodedContent(requestArgs)))
                using (HttpContent content = response.Content)
                {
                    var result = await content.ReadAsStringAsync();
                    _logger.LogDebug("Result from live token api: " + result);

                    if (!response.IsSuccessStatusCode)
                    {
                        Response.StatusCode = 500;
                        return Json(result);
                    }
                    dynamic liveData = JsonConvert.DeserializeObject<dynamic>(result);
                    accessToken = liveData.access_token;
                }


                // 5)

                var profileRequestUri = $"{accessApiRequestUri}?access_token={Uri.EscapeUriString(accessToken)}";
                using (HttpResponseMessage response = await client.GetAsync(profileRequestUri))
                using (HttpContent content = response.Content)
                {
                    var result = await content.ReadAsStringAsync();
                    _logger.LogInformation("Result from live api: " + result);
                    profileData = JsonConvert.DeserializeObject<dynamic>(result);

                    if (!response.IsSuccessStatusCode)
                    {
                        Response.StatusCode = 500;
                        return Json(profileData);
                    }
                }
            }

            // 6)

            string liveId = profileData.id;
            if (user != null)
            {
                user.LiveId = liveId;
                await DbContext.SaveChangesAsync();
                return Json(user);
            }

            user = DbContext.User.FirstOrDefault(u => u.LiveId == liveId);
            if (user == null)
            {
                user = new User
                {
                    Admin = false,
                    DisplayName = profileData.name,
                    LiveId = liveId
                };

                DbContext.User.Add(user);
                await DbContext.SaveChangesAsync();
            }

            // 7)

            sessionData.UserId = user.Id;
            SetCurrentSession(sessionData);
            RemeberMeSession(sessionData);

            return Json(user);
        }

        public async Task<IActionResult> TwitterLogin([FromBody] dynamic data)
        {
            string oauthToken =  data.oauthToken;
            string oauthVerifier = data.oauthVerifier;

            // 1)

            User user = GetCurrentUser();
            if (user?.TwitterId != null)
            {
                return Json(user);
            }

            // 2)

            SessionData sessionData = GetCurrentSession();
            if (string.IsNullOrEmpty(sessionData.LoginToken))
            {
                Response.StatusCode = 400;
                return Json(null);
            }

            // 3)

            var appKey = _configuration.GetValue<string>("Authentication:Twitter:AppId");
            var appSecret = _configuration.GetValue<string>("Authentication:Twitter:AppSecret");
            var oauth = new Oauth(appKey, appSecret)
            {
                RequestUrl = "https://api.twitter.com/oauth/access_token",
                AccessSecret = sessionData.LoginToken
            };
            oauth.AddOauthParameter("token", oauthToken);
            oauth.AddParameter("oauth_verifier", oauthVerifier);

            var oauthResult = await oauth.DoRequest();


            // 4)

            string twitterId = oauthResult["user_id"];
            if (user != null)
            {
                user.TwitterId = twitterId;
                await DbContext.SaveChangesAsync();
                sessionData.LoginToken = null;
                SetCurrentSession(sessionData);
                return Json(user);
            }

            user = DbContext.User.FirstOrDefault(u => u.TwitterId == twitterId);
            if (user == null)
            {
                user = new User
                {
                    Admin = false,
                    DisplayName = oauthResult["screen_name"],
                    TwitterId = twitterId
                };

                DbContext.User.Add(user);
                await DbContext.SaveChangesAsync();
            }

            sessionData.UserId = user.Id;
            sessionData.LoginToken = null;

            SetCurrentSession(sessionData);
            RemeberMeSession(sessionData);
            return Json(user);
        }

        public async Task<IActionResult> LoginToken([FromBody] dynamic data)
        {
            SessionData sessionData = GetCurrentSession();

            string app = data.app;
            dynamic result;
            switch (app)
            {
                case "facebook":
                {
                    var loginToken = RandomHelper.GetUniqueKey(64);
                    sessionData.LoginToken = loginToken;

                    result = new
                    {
                        appKey = _configuration.GetValue<string>("Authentication:Facebook:AppId"),
                        loginToken = loginToken
                    };
                    break;
                }
                case "google":
                {
                    var loginToken = RandomHelper.GetUniqueKey(64);
                    sessionData.LoginToken = loginToken;

                    result = new
                    {
                        appKey = _configuration.GetValue<string>("Authentication:Google:AppId"),
                        loginToken = loginToken
                    };
                    break;
                }
                case "live":
                {
                    var loginToken = RandomHelper.GetUniqueKey(64);
                    sessionData.LoginToken = loginToken;

                    result = new
                    {
                        appKey = _configuration.GetValue<string>("Authentication:Live:AppId"),
                        loginToken = loginToken
                    };
                    break;
                }
                case "twitter":
                {
                    var appKey = _configuration.GetValue<string>("Authentication:Twitter:AppId");
                    var appSecret = _configuration.GetValue<string>("Authentication:Twitter:AppSecret");
                    var oauth = new Oauth(appKey, appSecret)
                    {
                        RequestUrl = "https://api.twitter.com/oauth/request_token"
                    };
                    oauth.AddOauthParameter("callback", _configuration.GetValue<string>("Authentication:Twitter:Callback"));
                    var oauthResult = await oauth.DoRequest();
                    result = new
                    {
                        loginToken = oauthResult["oauth_token"]
                    };
                    sessionData.LoginToken = oauthResult["oauth_token_secret"];
                    break;
                }
                default:
                {
                    Response.StatusCode = 400;
                    return Json(null);
                }
            }

            SetCurrentSession(sessionData);

            return Json(result);
        }


        public IActionResult UpdateProfile([FromBody] dynamic data)
        {
            User user = GetCurrentUser();
            if (user == null)
            {
                Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                return Json(null);
            }

            user.DisplayName = data.displayName;
            DbContext.SaveChanges();

            return Json(user);
        }
    }
}