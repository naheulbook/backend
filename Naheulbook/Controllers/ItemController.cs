﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Naheulbook.Models;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Utils.Action;
using Naheulbook.Utils.Item;
using Naheulbook.Utils.Modifier;
using Naheulbook.WebSocket;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class ItemController : NaheulbookController
    {
        public IWebsocketManager WebsocketManager { get; set; }
        protected readonly ItemHelper ItemHelper;
        protected readonly ActionManager ActionManager;

        public ItemController(NaheulbookDbContext dbContext, IWebsocketManager websocketManager, ItemHelper itemHelper, ActionManager actionManager)
            : base(dbContext)
        {
            WebsocketManager = websocketManager;
            ItemHelper = itemHelper;
            ActionManager = actionManager;
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Delete([FromBody] dynamic inputData)
        {
            long itemId = inputData.itemId;

            Item item = DbContext.Item.Find(itemId);
            if (item == null)
            {
                return Error(HttpStatusCode.BadRequest, "itemNotFound");
            }

            if (!await ItemHelper.IsUserAllowedTo(ItemAction.Delete, item, CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            await ItemHelper.DeleteItem(item, CurrentUser);

            return Json(item);
        }


        [LoggedUserFilter]
        public async Task<IActionResult> TakeItemFromLoot([FromBody] JObject inputData)
        {
            var itemId = inputData.Value<long>("itemId");
            var quantity = inputData.Value<int?>("quantity");
            var characterId = inputData.Value<long>("characterId");

            Item leftItem = null;
            Item item = DbContext.Item.Find(itemId);
            if (item == null)
            {
                return Error(HttpStatusCode.BadRequest, "itemNotFound");
            }

            Character character = DbContext.Character.Find(characterId);
            if (character == null)
            {
                return Error(HttpStatusCode.BadRequest, "characterNotFound");
            }

            var splitItem = false;
            var itemQuantity = item.Data.ValueOrDefault<int?>("quantity");
            if (itemQuantity.HasValue)
            {
                if (quantity == null)
                {
                    quantity = itemQuantity;
                }
                else
                {
                    if (quantity >= itemQuantity)
                    {
                        quantity = itemQuantity;
                    }
                    else
                    {
                        splitItem = true;
                    }
                }
            }

            long takenFromId;
            object websocketData;
            var takenFromType = ObservedElementType.Loot;
            // FIXME: possible race condition...
            if (item.LootId != null)
            {
                await DbContext.Entry(item).Reference(i => i.Loot).Query()
                    .Include(l => l.Group)
                    .LoadAsync();

                if (!item.Loot.VisibleForPlayer || character.GroupId != item.Loot.GroupId)
                {
                    return Error(HttpStatusCode.Forbidden, "not_authorized");
                }
                takenFromId = item.LootId.Value;
                if (!splitItem)
                {
                    item.LootId = null;
                    item.CharacterId = character.Id;
                }
                else
                {
                    leftItem = item;
                    item.Data["quantity"] = itemQuantity - quantity;
                    item = ItemHelper.CopyItem(item);
                    item.Data["quantity"] = quantity;
                    item.CharacterId = character.Id;
                    DbContext.Add(item);
                }

                websocketData = new {item, character, leftItem, quantity};
            }
            else if (item.MonsterId != null)
            {
                await DbContext.Entry(item).Reference(i => i.Monster).Query()
                    .Include(m => m.Loot)
                    .Include(m => m.Group)
                    .LoadAsync();

                if (item.Monster.LootId == null
                    || !item.Monster.Loot.VisibleForPlayer
                    || character.GroupId != item.Monster.Loot.GroupId)
                {
                    return Error(HttpStatusCode.Forbidden, "not_authorized");
                }

                takenFromId = item.Monster.Id;
                takenFromType = ObservedElementType.Monster;

                if (!splitItem)
                {
                    item.MonsterId = null;
                    item.CharacterId = character.Id;
                }
                else
                {
                    leftItem = item;
                    item.Data["quantity"] = itemQuantity - quantity;
                    item = ItemHelper.CopyItem(item);
                    item.Data["quantity"] = quantity;
                    item.CharacterId = character.Id;
                    DbContext.Add(item);
                }

                websocketData = new {item, character, item.Monster, leftItem, quantity};
            }
            else if (item.CharacterId != null)
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }
            else
            {
                return Error(HttpStatusCode.InternalServerError, "item not linked to something");
            }

            DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "LOOT_ITEM")
            {
                ItemId = item.Id
            });

            await DbContext.SaveChangesAsync();

            Item fullItem = ItemHelper.LoadFullItem(item.Id);
            WebsocketManager.SendMessage("tookItem", takenFromType, takenFromId, websocketData);
            WebsocketManager.SendMessage("addItem", ObservedElementType.Character, character.Id, fullItem);

            return Json(new {item, leftItem});
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Add([FromBody] JObject inputData)
        {
            var itemTemplateId = inputData.Value<int>("itemTemplateId");
            var targetType = inputData.Value<string>("targetType");
            var targetId = inputData.Value<int?>("targetId");
            var itemData = inputData.Value<JObject>("itemData");

            Character character = null;
            Loot loot = null;
            Monster monster = null;
            switch (targetType)
            {
                case "loot":
                {
                    loot = DbContext.Loot
                        .Include(c => c.Group)
                        .First(c => c.Id == targetId);

                    if (loot.Group.MasterId != CurrentUser.Id)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                }
                case "monster":
                {
                    monster = DbContext.Monster
                        .Include(c => c.Group)
                        .First(m => m.Id == targetId);

                    if (monster.Group.MasterId != CurrentUser.Id)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                }
                case "character":
                {
                    character = DbContext.Character
                        .Include(c => c.Group)
                        .First(c => c.Id == targetId);

                    if (!character.IsEditableBy(CurrentUser))
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                }
                default:
                    return Error(HttpStatusCode.BadRequest, "invalidTargetType");
            }

            var item = await ItemHelper.CreateItemFor(itemData, itemTemplateId, character, loot, monster, CurrentUser);
            return Json(item);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> AddRandom([FromBody] JObject inputData)
        {
            var targetType = inputData.Value<string>("targetType");
            var targetId = inputData.Value<int?>("targetId");
            var criteria = inputData.Value<JObject>("criteria");

            Character character = null;
            Loot loot = null;
            Monster monster = null;
            switch (targetType)
            {
                case "loot":
                {
                    loot = DbContext.Loot
                        .Include(c => c.Group)
                        .First(c => c.Id == targetId);

                    if (loot.Group.MasterId != CurrentUser.Id)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                }
                case "monster":
                {
                    monster = DbContext.Monster
                        .Include(c => c.Group)
                        .First(c => c.Id == targetId);

                    if (monster.Group.MasterId != CurrentUser.Id)
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                }
                case "character":
                {
                    character = DbContext.Character
                        .Include(c => c.Group)
                        .First(c => c.Id == targetId);

                    if (!character.IsEditableBy(CurrentUser))
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    break;
                }
                default:
                    return Error(HttpStatusCode.BadRequest, "invalidTargetType");
            }

            ItemTemplate itemTemplate;
            var categoryName = criteria.Value<string>("categoryName");
            if (categoryName != null)
            {
                var category = await DbContext.ItemTemplateCategory
                    .FirstOrDefaultAsync(c => c.TechName == categoryName);

                var list = await DbContext.ItemTemplate
                    .Where(i => i.CategoryId == category.Id).ToListAsync();

                var idx = new Random().Next(0, list.Count);

                itemTemplate = list[idx];
            }
            else
            {
                return BadRequest("bad criteria");
            }

            var itemData = new JObject();
            itemData["name"] = itemTemplate.Name;
            if (itemTemplate.Data.ValueOrDefault<bool>("quantifiable"))
            {
                itemData["quantity"] = 1;
            }

            var item = await ItemHelper.CreateItemFor(itemData, itemTemplate, character, loot, monster, CurrentUser);
            return Json(item);
        }

        [LoggedUserFilter]
        public IActionResult Equip([FromBody] dynamic inputData)
        {
            int itemId = inputData.itemId;
            int level = inputData.level;

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            if (item.Data["equiped"] == null)
            {
                item.Data["equiped"] = 1;
                DbContext.CharacterHistory.Add(new CharacterHistory(item.Character, CurrentUser, "EQUIP")
                {
                    ItemId = item.Id
                });
            }
            else
            {
                if (level == -1)
                {
                    item.Data["equiped"] = item.Data["equiped"].Value<int>() - 1;
                    if (item.Data["equiped"].Value<int>() == 0)
                    {
                        item.Data["equiped"] = - 1;
                    }
                }
                else if (level == 1)
                {
                    item.Data["equiped"] = item.Data["equiped"].Value<int>() + 1;
                    if (item.Data["equiped"].Value<int>() == 0)
                    {
                        item.Data["equiped"] = 1;
                    }
                }

                if (level == 0)
                {
                    item.Data.Remove("equiped");
                    DbContext.CharacterHistory.Add(new CharacterHistory(item.Character, CurrentUser, "UNEQUIP")
                    {
                        ItemId = item.Id
                    });
                }
                else if (item.Data["equiped"].Value<int>() == 0)
                {
                    item.Data.Remove("equiped");
                }
            }

            WebsocketManager.SendMessage("equipItem", ObservedElementType.Character, item.Character.Id, item);

            DbContext.SaveChanges();
            return Json(item);
        }

        [LoggedUserFilter]
        public IActionResult MoveToContainer([FromBody] JObject inputData)
        {
            var itemId = inputData.Value<int>("itemId");

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            item.Container = inputData.Value<int?>("containerId");
            DbContext.SaveChanges();

            WebsocketManager.SendMessage("changeContainer", ObservedElementType.Character, item.Character.Id, item);

            return Json(item);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> UpdateQuantity([FromBody] dynamic inputData)
        {
            int itemId = inputData.itemId;
            int quantity = inputData.quantity;

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            await ItemHelper.UpdateQuantity(item, CurrentUser, quantity);

            return Json(item);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> GiveItem([FromBody] dynamic inputData)
        {
            int itemId = inputData.itemId;
            int characterId = inputData.characterId;
            int? givenQuantity = inputData.quantity;

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }
            long? groupId = item.Character.GroupId;
            Character owner = item.Character;


            if (!givenQuantity.HasValue || givenQuantity.Value == 0)
            {
                givenQuantity = 1;
            }

            if (item.Data.HasKey("quantity"))
            {
                var currentQuantity = item.Data.ValueOrDefault("quantity", 1);
                if (currentQuantity != givenQuantity)
                {
                    if (currentQuantity < givenQuantity)
                    {
                        givenQuantity = currentQuantity;
                    }
                    else
                    {
                        await ItemHelper.UpdateQuantity(item, CurrentUser, currentQuantity - givenQuantity.Value);
                        item = ItemHelper.CopyItem(item);
                        DbContext.Add(item);
                        await DbContext.SaveChangesAsync();
                        item.Data["quantity"] = givenQuantity;
                    }
                }

            }

            var destination = DbContext.Character
                .First(c => c.GroupId == groupId && c.Id == characterId && c.Active);

            DbContext.CharacterHistory.Add(new CharacterHistory(owner, CurrentUser, "GIVE_ITEM",
                new
                {
                    target = destination.Name
                })
            {
                ItemId = item.Id,
            });

            DbContext.CharacterHistory.Add(new CharacterHistory(destination, CurrentUser, "GIVEN_ITEM",
                new
                {
                    source = owner.Name
                })
            {
                ItemId = item.Id,
            });

            item.CharacterId = destination.Id;
            item.Container = null;
            item.Data.Remove("readCount");
            DbContext.SaveChanges();

            WebsocketManager.SendMessage("deleteItem", ObservedElementType.Character, owner.Id, item);

            Item fullItem = ItemHelper.LoadFullItem(item.Id);
            WebsocketManager.SendMessage("addItem", ObservedElementType.Character, destination.Id, fullItem);

            return Json(item);
        }

        [LoggedUserFilter]
        public IActionResult ReadBook([FromBody] dynamic inputData)
        {
            int itemId = inputData.itemId;

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            item.Data["readCount"] = item.Data.ValueOrDefault<int>("readCount") + 1;
            DbContext.CharacterHistory.Add(new CharacterHistory(item.Character, CurrentUser, "READ_BOOK")
            {
                ItemId = item.Id
            });
            DbContext.SaveChanges();

            return Json(item);
        }

        [LoggedUserFilter]
        public IActionResult Identify([FromBody] dynamic inputData)
        {
            int itemId = inputData.itemId;

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .Include(i => i.ItemTemplate)
                .First(i => i.Id == itemId);

            if (!item.Character.IsGm(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            item.Data.Remove("notIdentified");
            item.Data["name"] = item.ItemTemplate.Name;
            DbContext.CharacterHistory.Add(new CharacterHistory(item.Character, CurrentUser, "IDENTIFY")
            {
                ItemId = item.Id
            });
            DbContext.SaveChanges();

            WebsocketManager.SendMessage("identifyItem", ObservedElementType.Character, item.Character.Id, item);

            return Json(item);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> UpdateCharge([FromBody] dynamic inputData)
        {
            int itemId = inputData.itemId;
            int charge = inputData.charge;

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .Include(i => i.ItemTemplate)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            int oldCharge = item.Data.ValueOrDefault<int?>("charge") ??
                            item.ItemTemplate.Data.ValueOrDefault<int>("charge");

            item.Data["charge"] = charge;

            DbContext.CharacterHistory.Add(new CharacterHistory(item.Character, CurrentUser, "USE_CHARGE",
                new
                {
                    oldValue = oldCharge,
                    newValue = charge
                })
            {
                ItemId = item.Id,
            });

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("useCharge", ObservedElementType.Character, item.Character.Id, item);

            if (oldCharge - 1 == charge)
            {
                await ActionManager.ExecuteItemActions(CurrentUser, item, item.Character);
            }

            return Json(item);
        }

        [LoggedUserFilter]
        public IActionResult UpdateItem([FromBody] JObject inputData)
        {
            var itemData = inputData.Value<JObject>("itemData");
            var itemId = inputData.Value<int>("itemId");

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            if (itemData["name"] != null)
            {
                item.Data["name"] = itemData["name"];
            }
            if (itemData["description"] != null)
            {
                item.Data["description"] = itemData["description"];
            }
            if (itemData["icon"] != null)
            {
                item.Data["icon"] = itemData["icon"];
            }
            item.Data["lifetime"] = itemData["lifetime"];
            item.Data["ignoreRestrictions"] = itemData["ignoreRestrictions"];

            WebsocketManager.SendMessage("updateItem", ObservedElementType.Character, item.Character.Id, item);

            DbContext.SaveChanges();

            return Json(item);
        }

        [LoggedUserFilter]
        public IActionResult UpdateItemModifiers([FromBody] JObject inputData)
        {
            var itemModifiers = inputData.Value<JToken>("modifiers").ToObject<List<ActiveStatsModifier>>();
            var itemId = inputData.Value<int>("itemId");

            Item item = DbContext.Item
                .Include(i => i.Character)
                .ThenInclude(c => c.Group)
                .First(i => i.Id == itemId);

            if (!item.Character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            item.Modifiers = itemModifiers;

            WebsocketManager.SendMessage("updateItemModifiers", ObservedElementType.Character, item.Character.Id, item);

            DbContext.SaveChanges();

            return Json(item);
        }
    }
}