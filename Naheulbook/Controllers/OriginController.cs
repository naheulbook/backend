﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Models;

namespace Naheulbook.Controllers
{
    public class OriginController: NaheulbookController
    {
        public OriginController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }

        public IActionResult List()
        {
            var origins = DbContext.Origin
                .Include(o => o.OriginRequirement)
                .Include(o => o.OriginInfo)
                .Include(o => o.OriginRestrict)
                .Include(o => o.OriginBonus)
                .Include(o => o.OriginSkill)
                .ToList();

            return Json(origins);
        }
    }
}