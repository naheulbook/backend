using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class MonsterTemplateController : NaheulbookController
    {
        public MonsterTemplateController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }


        public async Task<IActionResult> ListMonster()
        {
            var monsters = await DbContext.MonsterTemplate
                .OrderBy(m => m.Id)
                .Include(m => m.Locations)
                .Include(m => m.SimpleInventory)
                .ThenInclude(i => i.ItemTemplate)
                .ToListAsync();

            return Json(monsters);
        }

        public async Task<IActionResult> ListTypes()
        {
            var monsters = await DbContext.MonsterType
                .Include(t => t.Categories)
                .ToListAsync();

            return Json(monsters);
        }


        public async Task<IActionResult> ListTraits()
        {
            var monsters = await DbContext.MonsterTrait
                .ToListAsync();

            return Json(monsters);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateCategory([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }
            
            var category = new MonsterCategory();
            category.Name = inputData.Value<string>("name");
            category.TypeId = inputData.Value<int>("typeId");

            DbContext.MonsterCategory.Add(category);
            await DbContext.SaveChangesAsync();

            return Json(category);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateType([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }
            
            var type = new MonsterType();
            type.Name = inputData.Value<string>("name");

            DbContext.MonsterType.Add(type);
            await DbContext.SaveChangesAsync();

            return Json(type);
        }

        public IActionResult SearchMonster([FromBody] dynamic inputData)
        {
            string name = inputData.name;
            if (string.IsNullOrEmpty(name))
            {
                return Json(new string[0]);
            }
            name = name.ToUpper();

            var monsters = DbContext.MonsterTemplate
                .Include(m => m.SimpleInventory)
                .ThenInclude(si => si.ItemTemplate)
                .Where(monster => monster.Name.ToUpper().Contains(name))
                .Take(7)
                .ToList();

            return Json(monsters);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> EditMonster([FromBody] JObject inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }

            int categoryId = inputData.Value<int>("categoryId");
            var monsterCategory = await DbContext.MonsterCategory.FirstAsync(m => m.Id == categoryId);
            var monsterData = inputData.Value<JObject>("monster");

            MonsterTemplate monster;
            var monsterId = monsterData.Value<int?>("id");
            if (monsterId == null)
            {
                monster = new MonsterTemplate();
                DbContext.Add(monster);
            }
            else
            {
                monster = await DbContext.MonsterTemplate
                    .Include(m => m.SimpleInventory)
                    .ThenInclude(i => i.ItemTemplate)
                    .Include(m => m.Locations)
                    .FirstAsync(m => m.Id == monsterId);
            }


            monster.Name = monsterData.Value<string>("name");
            monster.Data = monsterData.Value<JObject>("data").ToString(Formatting.None);
            monster.Category = monsterCategory;

            await DbContext.SaveChangesAsync();

            var currentLocationsId = monster.LocationsId;
            var locations = monsterData.Value<JArray>("locations") ?? new JArray();
            foreach (JToken jLocationId in locations)
            {
                var locationId = jLocationId.Value<int>();
                if (currentLocationsId.Any(l => l == locationId))
                {
                    continue;
                }
                monster.Locations.Add(new MonsterLocation() {LocationId = locationId, MonsterTemplateId = monster.Id});
            }
            foreach (long locationId in currentLocationsId)
            {
                if (locations.Any(l => l.Value<int>() == locationId))
                {
                    continue;
                }
                monster.Locations.Remove(monster.Locations.First(l => l.LocationId == locationId));
            }

            var currentSimpleInventory = new List<MonsterTemplateSimpleInventory>(monster.SimpleInventory);
            var keepingItem = new List<long>();
            var simpleInventory = monsterData.Value<JArray>("simpleInventory") ?? new JArray();
            foreach (JToken jSimpleInventory in simpleInventory)
            {
                var jItemInventory = jSimpleInventory.Value<JObject>();
                var itemInventoryId = jItemInventory.Value<long>("id");
                var chance = jItemInventory.Value<float>("chance");
                var minCount = jItemInventory.Value<int>("minCount");
                var maxCount = jItemInventory.Value<int>("maxCount");
                if (itemInventoryId == 0)
                {
                    var itemTemplateId = jItemInventory.Value<JObject>("itemTemplate").Value<int>("id");
                    monster.SimpleInventory.Add(new MonsterTemplateSimpleInventory()
                    {
                        MonsterTemplateId = monster.Id,
                        Chance = chance,
                        MinCount = minCount,
                        MaxCount = maxCount,
                        ItemTemplateId = itemTemplateId
                    });
                }
                else
                {
                    MonsterTemplateSimpleInventory itemInventory = currentSimpleInventory.First(f => f.Id == itemInventoryId);
                    itemInventory.Chance = chance;
                    itemInventory.MinCount = minCount;
                    itemInventory.MaxCount = maxCount;
                    keepingItem.Add(itemInventoryId);
                }

            }
            foreach (MonsterTemplateSimpleInventory itemInventory in currentSimpleInventory)
            {
                if (!keepingItem.Contains(itemInventory.Id))
                {
                    monster.SimpleInventory.Remove(itemInventory);
                }
            }


            await DbContext.SaveChangesAsync();

            return Json(monster);
        }
    }
}