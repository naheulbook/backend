﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;
using Naheulbook.Utils.Item;
using Naheulbook.WebSocket;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Controllers
{
    public class CharacterController : NaheulbookController
    {
        protected readonly ItemHelper ItemHelper;
        public CharacterController(NaheulbookDbContext dbContext, IWebsocketManager websocketManager, ItemHelper itemHelper)
            : base(dbContext)
        {
            WebsocketManager = websocketManager;
            ItemHelper = itemHelper;
        }

        public IWebsocketManager WebsocketManager { get; set; }

        protected async Task<Character> GetCharacter(int characterId)
        {
            Character character = await DbContext.Character
                .Include(c => c.CharacterModifiers)
                .ThenInclude(c => c.CharacterModifierValue)
                .Include(c => c.CharacterSkills)
                .Include(c => c.Group)
                .Include(c => c.CharacterSpecialities)
                .ThenInclude(s => s.Speciality)
                .ThenInclude(s => s.SpecialitySpecial)
                .Include(c => c.CharacterSpecialities)
                .ThenInclude(s => s.Speciality)
                .ThenInclude(s => s.SpecialityModifier)
                .Include(c => c.GroupInvitations)
                .ThenInclude(gi => gi.Group)
                .Include(c => c.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateUnskills)
                .Include(c => c.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSkills)
                .Include(c => c.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateModifiers)
                .Include(c => c.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateRequirements)
                .Include(c => c.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSlot)
                .ThenInclude(s => s.Slot)
                .Include(c => c.CharacterJobs)
                .Include(c => c.Items)
                .ThenInclude(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSkillModifiers)
                .FirstAsync(c => c.Id == characterId);

            if (!character.IsEditableBy(CurrentUser))
            {
                return null;
            }

            return character;
        }

        protected async Task<Character> GetCharacterForUser(int characterId)
        {
            var character = await GetCharacter(characterId);

            if (character == null)
            {
                return null;
            }

            if (!character.IsGm(CurrentUser))
            {
                character.GmDataJson = null;
            }

            if (character.Group != null)
            {
                character.Group.Data = null;
            }
            return character;
        }

        [LoggedUserFilter]
        public IActionResult List()
        {
            var characters = DbContext.Character
                .Include(c => c.CharacterJobs)
                .Where(c => c.UserId == CurrentUser.Id && !c.IsNpc)
                .OrderBy(c => c.Id).ToList();

            return Json(characters);
        }

        [LoggedUserFilter]
        public IActionResult ResumeList([FromBody] IList<long> charactersId)
        {
            var characters = DbContext.Character
                .Where(c => charactersId.Contains(c.Id))
                .ToList();

            return Json(characters);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Detail([FromBody] dynamic inputData)
        {
            int characterId = inputData.id;

            Character character = await GetCharacterForUser(characterId);
            if (character == null)
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            return Json(character);
        }

        [LoggedUserFilter]
        public IActionResult SetStatBonusAd([FromBody] dynamic inputData)
        {
            string stat = inputData.stat;
            int characterId = inputData.id;

            Character character = DbContext.Character
                .Include(c => c.Group)
                .First(c => c.Id == characterId);

            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            character.StatBonusAD = stat;
            DbContext.SaveChanges();

            WebsocketManager.SendMessage("statBonusAd", ObservedElementType.Character, character.Id, stat);

            return Json(stat);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> LevelUp([FromBody] JObject inputData)
        {
            var characterId = (int) inputData["id"];
            Character character = await GetCharacter(characterId);

            if (character == null)
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            var levelUpInfo = (JObject) inputData["levelUpInfo"];

            character.Level++;

            var characterModifier = new CharacterModifier
            {
                Name = "LevelUp: " + character.Level,
                Permanent = true
            };

            var value1 = new CharacterModifierValue
            {
                StatName = (string) levelUpInfo["EVorEA"],
                Value = (short) levelUpInfo["EVorEAValue"],
                Type = "ADD"
            };
            characterModifier.CharacterModifierValue.Add(value1);

            var value2 = new CharacterModifierValue
            {
                StatName = (string) levelUpInfo["statToUp"],
                Value = 1,
                Type = "ADD"
            };
            characterModifier.CharacterModifierValue.Add(value2);

            character.CharacterModifiers.Add(characterModifier);

            if (levelUpInfo.TryGetValue("skill", out var skill))
            {
                var skillId = (int) skill["id"];
                var newSkill = new CharacterSkills {SkillId = skillId};
                character.CharacterSkills.Add(newSkill);
            }

            if (levelUpInfo.TryGetValue("specialities", out var jSpecialities))
            {
                var specialities = jSpecialities as JObject;
                foreach (var speciality in specialities)
                {
                    var specialityId = (int) speciality.Value["id"];
                    var newSpeciality = new CharacterSpeciality() {SpecialityId = specialityId};
                    character.CharacterSpecialities.Add(newSpeciality);
                }
            }

            if (character.Level == 2 || character.Level == 3)
            {
                Origin origin = DbContext.Origin
                    .Include(o => o.OriginBonus)
                    .First(o => o.Id == character.OriginId);
                if (origin.HasFlag("CHA_+1_LVL2_LVL3"))
                {
                    var elfCharacterModifier = new CharacterModifier
                    {
                        Name = "LevelUp charisme: " + character.Level,
                        Permanent = true
                    };

                    var elfValue1 = new CharacterModifierValue
                    {
                        StatName = "CHA",
                        Value = 1,
                        Type = "ADD"
                    };
                    elfCharacterModifier.CharacterModifierValue.Add(elfValue1);

                    character.CharacterModifiers.Add(elfCharacterModifier);
                }
            }

            DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "LEVEL_UP")
            {
                Info = character.Level.ToString(),
            });
            DbContext.SaveChanges();

            WebsocketManager.SendMessage("levelUp", ObservedElementType.Character, character.Id,
                new
                {
                    id = character.Id,
                    level = character.Level,
                    modifiers = character.CharacterModifiers,
                    skills = character.CharacterSkills
                });

            return Json(new {
                id = character.Id,
                level = character.Level,
                modifiers = character.CharacterModifiers,
                skills = character.CharacterSkills
            });
        }

        public IActionResult Stats()
        {
            var stats = DbContext.Stat
                .ToList();

            return Json(stats);
        }

        [LoggedUserFilter]
        public IActionResult Update([FromBody] JObject inputData)
        {
            var stat = inputData.Value<string>("stat");
            var characterId = inputData.Value<int>("characterId");

            Character character = DbContext.Character
                .Include(c => c.Group)
                .First(c => c.Id == characterId);
            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            switch (stat)
            {
                case "ev":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "MODIFY_EV",
                        new {oldValue = character.Ev, newValue = inputData.Value<short>("value")}));

                    character.Ev = inputData.Value<short>("value");
                    break;
                case "ea":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "MODIFY_EA",
                        new {oldValue = character.Ea, newValue = inputData.Value<short>("value")}));

                    character.Ea = inputData.Value<short>("value");
                    break;
                case "fatePoint":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "USE_FATE_POINT",
                        new {oldValue = character.FatePoint, newValue = inputData.Value<short>("value")}));

                    character.FatePoint = inputData.Value<short>("value");
                    break;
                case "experience":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "ADD_XP",
                        new {oldValue = character.Experience, newValue = inputData.Value<short>("value")}));

                    character.Experience = inputData.Value<int>("value");
                    break;
                case "sex":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "CHANGE_SEX",
                        new {oldValue = character.Experience, newValue = inputData.Value<string>("value")}));

                    character.Sexe = inputData.Value<string>("value");
                    break;
                case "name":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "CHANGE_NAME",
                        new {oldValue = character.Experience, newValue = inputData.Value<string>("value")}));

                    character.Name = inputData.Value<string>("value");
                    break;
                default:
                    return Error(HttpStatusCode.BadRequest, "invalid_stat");
            }

            DbContext.SaveChanges();
            WebsocketManager.SendMessage("update", ObservedElementType.Character, character.Id, new {stat, value = inputData["value"]});
            return Json(new {stat, value = inputData["value"]});
        }

        [LoggedUserFilter]
        public IActionResult AddJob([FromBody] JObject inputData)
        {
            var jobId = inputData.Value<int>("jobId");
            var characterId = inputData.Value<int>("characterId");

            Character character = DbContext.Character
                .Include(c => c.Group)
                .Include(c => c.CharacterJobs)
                .First(c => c.Id == characterId);
            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "ADD_JOB", new {jobId}));
            DbContext.CharacterJob.Add(new CharacterJob()
            {
                CharacterId = characterId,
                JobId = jobId,
                Order = character.CharacterJobs.Count + 1
            });

            DbContext.SaveChanges();
            WebsocketManager.SendMessage("addJob", ObservedElementType.Character, character.Id, new {jobId});

            return Json(new {jobId});
        }

        [LoggedUserFilter]
        public IActionResult RemoveJob([FromBody] JObject inputData)
        {
            var jobId = inputData.Value<int>("jobId");
            var characterId = inputData.Value<int>("characterId");

            Character character = DbContext.Character
                .Include(c => c.Group)
                .First(c => c.Id == characterId);
            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "REMOVE_JOB", new {jobId}));
            var characterJob = DbContext.CharacterJob.First(cj => cj.JobId == jobId && cj.CharacterId == characterId);
            DbContext.CharacterJob.Remove(characterJob);

            DbContext.SaveChanges();
            WebsocketManager.SendMessage("removeJob", ObservedElementType.Character, character.Id, new {jobId});

            return Json(new {jobId});
        }

        [LoggedUserFilter]
        public IActionResult UpdateGmData([FromBody] JObject inputData)
        {
            var key = inputData.Value<string>("key");
            var characterId = inputData.Value<int>("characterId");

            Character character = DbContext.Character
                .Include(c => c.Group)
                .First(c => c.Id == characterId);
            if (!character.IsGm(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            switch (key)
            {
                case "active":
                    character.Active = inputData.Value<bool>("value");
                    WebsocketManager.SendMessage("active", ObservedElementType.Character, character.Id, character.Active);
                    break;
                case "owner":
                    if (!character.IsGm(CurrentUser))
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    character.UserId = inputData.Value<int>("value");
                    if (character.UserId == 0)
                    {
                        character.UserId = null;
                    }
                    break;
                case "color":
                    character.Color = inputData.Value<string>("value");
                    WebsocketManager.SendMessage("changeColor", ObservedElementType.Character, character.Id, character.Color);
                    break;
                case "target":
                    if (!character.IsGm(CurrentUser))
                    {
                        return Error(HttpStatusCode.Forbidden, "not_authorized");
                    }
                    var targetInfo = inputData.Value<JObject>("value");
                    var isMonster = targetInfo.Value<bool>("isMonster");
                    var id = targetInfo.Value<long>("id");
                    if (isMonster)
                    {
                        character.TargetMonsterId = id;
                        character.TargetCharacterId = null;
                    }
                    else
                    {
                        character.TargetMonsterId = null;
                        character.TargetCharacterId = id;
                    }
                    WebsocketManager.SendMessage("changeTarget", ObservedElementType.Character, character.Id, targetInfo);
                    break;
                case "mankdebol":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "MANKDEBOL",
                        new {oldValue = character.GmData.ValueOrDefault<int>("mankdebol"), newValue = inputData.Value<int>("value")}));

                    character.GmData["mankdebol"] = inputData.Value<int>("value");
                    break;
                case "debilibeuk":
                    DbContext.CharacterHistory.Add(new CharacterHistory(character, CurrentUser, "DEBILIBEUK",
                        new {oldValue = character.GmData.ValueOrDefault<int>("debilibeuk"), newValue = inputData.Value<int>("value")}));

                    character.GmData["debilibeuk"] = inputData.Value<int>("value");
                    break;

                default:
                    return Error(HttpStatusCode.BadRequest, "invalid_key");
            }

            DbContext.SaveChanges();
            return Json(new {key, value = inputData["value"]});
        }

        [LoggedUserFilter]
        public async Task<IActionResult> AddModifier([FromBody] JObject inputData)
        {
            var characterId = inputData.Value<int>("characterId");

            Character character = DbContext.Character
                .Include(c => c.Group)
                .Include(c => c.Group)
                .First(c => c.Id == characterId);

            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            var modifier = inputData.Value<JObject>("modifier");

            var characterModifier = new CharacterModifier
            {
                CharacterId = characterId,
                Name = modifier.Value<string>("name"),
                Permanent = false,
                DurationType = modifier.Value<string>("durationType"),
                Duration = modifier.Value<string>("duration"),
                Reusable = modifier.Value<bool>("reusable"),
                Type = modifier.Value<string>("type"),
                Description = modifier.Value<string>("description"),
                Active = true,
                CombatCount=  modifier.Value<int?>("combatCount"),
                CurrentCombatCount = modifier.Value<int?>("combatCount"),
                TimeDuration = modifier.Value<int?>("timeDuration"),
                CurrentTimeDuration = modifier.Value<int?>("timeDuration"),
                LapCount = modifier.Value<int?>("lapCount"),
                CurrentLapCount = modifier.Value<int?>("lapCount"),
                LapCountDecrement = modifier.Value<JObject>("lapCountDecrement"),
            };

            var values = modifier.Value<JArray>("values");
            foreach (JToken value in values)
            {
                var v = (JObject) value;
                var characterModifierValue = new CharacterModifierValue
                {
                    StatName = v.Value<string>("stat"),
                    Type = v.Value<string>("type"),
                    Value = v.Value<short>("value")
                };
                characterModifier.CharacterModifierValue.Add(characterModifierValue);
            }

            DbContext.Add(characterModifier);
            await DbContext.SaveChangesAsync();
            // Workaround strange bug reusable switch back to true during save...
            characterModifier.Reusable = modifier.Value<bool>("reusable");

            DbContext.Add(new CharacterHistory(character, CurrentUser, "APPLY_MODIFIER")
            {
                ModifierId = characterModifier.Id
            });

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("addModifier", ObservedElementType.Character, character.Id, characterModifier);

            return Json(characterModifier);
        }

        [LoggedUserFilter]
        public IActionResult RemoveModifier([FromBody] dynamic inputData)
        {
            int modifierId = inputData.modifierId;
            int characterId = inputData.characterId;

            Character character = DbContext.Character
                .Include(c => c.Group)
                .Include(c => c.CharacterModifiers)
                .First(c => c.Id == characterId);

            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            CharacterModifier cm =
                character.CharacterModifiers.FirstOrDefault(charModifier => charModifier.Id == modifierId);
            if (cm == null)
            {
                return Error(HttpStatusCode.BadRequest, "modifier_not_found");
            }
            cm.Reusable = false;
            cm.Active = false;

            var characterHistory = new CharacterHistory(character, CurrentUser, "REMOVE_MODIFIER")
            {
                ModifierId = modifierId
            };
            DbContext.CharacterHistory.Add(characterHistory);

            DbContext.SaveChanges();

            WebsocketManager.SendMessage("removeModifier", ObservedElementType.Character, character.Id, cm);

            return Json(cm);
        }

        [LoggedUserFilter]
        public IActionResult ToggleModifier([FromBody] dynamic inputData)
        {
            int modifierId = inputData.modifierId;
            int characterId = inputData.characterId;

            Character character = DbContext.Character
                .Include(c => c.Group)
                .Include(c => c.CharacterModifiers)
                .First(c => c.Id == characterId);

            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            CharacterModifier cm =
                character.CharacterModifiers.FirstOrDefault(charModifier => charModifier.Id == modifierId);
            if (cm == null)
            {
                return Error(HttpStatusCode.BadRequest, "modifier_not_found");
            }
            if (!cm.Reusable)
            {
                return Error(HttpStatusCode.BadRequest, "modifier_not_reusable");
            }
            cm.Active = !cm.Active;

            if (cm.Active)
            {
                cm.CurrentCombatCount = cm.CombatCount;
                cm.CurrentLapCount = cm.LapCount;
                cm.CurrentTimeDuration = cm.TimeDuration;
                var characterHistory = new CharacterHistory(character, CurrentUser, "ACTIVE_MODIFIER")
                {
                    ModifierId = modifierId
                };
                DbContext.CharacterHistory.Add(characterHistory);
            }
            else
            {
                var characterHistory = new CharacterHistory(character, CurrentUser, "DISABLE_MODIFIER")
                {
                    ModifierId = modifierId
                };
                DbContext.CharacterHistory.Add(characterHistory);
            }

            DbContext.SaveChanges();

            WebsocketManager.SendMessage("updateModifier", ObservedElementType.Character, character.Id, cm);

            return Json(cm);
        }

        [LoggedUserFilter]
        public IActionResult History([FromBody] dynamic inputData)
        {
            int characterId = inputData.characterId;
            int page = inputData.page;

            var firstIndex = Math.Max(0, page * 40 - 1);
            const int countResult = 42;
            Character character = DbContext.Character
                .Include(c => c.Group)
                .First(c => c.Id == characterId);

            var isGm = character.IsGm(CurrentUser);

            var characterHistories = DbContext.CharacterHistory
                .Where(ch => (!ch.Gm || ch.Gm == isGm) && ch.CharacterId == characterId)
                .OrderByDescending(ch => ch.Id)
                .Include(ch => ch.Effect)
                .Include(ch => ch.Item)
                .Include(ch => ch.Modifier)
                .Skip(firstIndex)
                .Take(countResult)
                .ToList();

            if (characterHistories.Count == 0)
            {
                return Json(new JArray());
            }

            DateTime firstDate = DateTime.Now;
            CharacterHistory last = characterHistories.Last();
            characterHistories.RemoveAt(characterHistories.Count - 1);
            if (page > 0)
            {
                if (characterHistories.Count == 0)
                {
                    firstDate = last.Date;
                }
                else
                {
                    CharacterHistory first = characterHistories.First();
                    firstDate = first.Date;
                    characterHistories.RemoveAt(0);
                }
            }


            var histories = new List<IHistory>();
            histories.AddRange(characterHistories);

            if (character.Group != null)
            {
                var groupHistories = DbContext.GroupHistory
                    .Where(gh => gh.GroupId == character.Group.Id
                                 && (!gh.Gm || gh.Gm == isGm)
                                 && gh.Date > last.Date
                                 && gh.Date < firstDate)
                    .OrderByDescending(gh => gh.Id)
                    .ToList();
                histories.AddRange(groupHistories);
            }

            histories.Sort((h1, h2) => h2.Date.CompareTo(h1.Date));

            return Json(histories);
        }

        [LoggedUserFilter]
        public IActionResult SearchForInvite([FromBody] dynamic inputData)
        {
            string name = inputData.name;
            name = name.ToUpper();

            var characters = DbContext.Character.Where(c => !c.IsNpc && c.GroupId == null && !c.IsNpc && c.Name.ToUpper().Contains(name))
                .Include(c => c.CharacterJobs)
                .Include(c => c.Origin)
                .Include(c => c.User)
                .Take(7)
                .ToList();

            return Json(characters.Select(c => new
            {
                id = c.Id,
                name = c.Name,
                origin = c.Origin.Name,
                owner = c.User.DisplayName
            }));
        }

        [LoggedUserFilter]
        public IActionResult CancelInvite([FromBody] dynamic inputData)
        {
            int characterId = inputData.characterId;
            int groupId = inputData.groupId;

            GroupInvitations gi = DbContext.GroupInvitations
                .Include(g => g.Character)
                .Include(g => g.Group)
                .First(g => g.CharacterId == characterId && g.GroupId == groupId);

            DbContext.Remove(gi);
            DbContext.SaveChanges();

            var res = new
            {
                gi.FromGroup,
                group = new
                {
                    id = gi.Group.Id,
                    name = gi.Group.Name,
                },
                character = new {
                    id = gi.Character.Id,
                    name = gi.Character.Name,
                }
            };

            WebsocketManager.SendMessage("cancelInvite", ObservedElementType.Character, characterId, res);
            WebsocketManager.SendMessage("cancelInvite", ObservedElementType.Group, groupId, res);

            return Json(res);
        }

        [LoggedUserFilter]
        public IActionResult Invite([FromBody] dynamic inputData)
        {
            int characterId = inputData.characterId;
            bool fromGroup = inputData.fromGroup;
            int groupId = inputData.groupId;

            var groupInvitations = new GroupInvitations
            {
                CharacterId = characterId,
                GroupId = groupId,
                FromGroup = fromGroup
            };

            Group group = DbContext.Group
                .First(g => g.Id == groupId);
            Character character = DbContext.Character
                .Include(c => c.CharacterJobs)
                .ThenInclude(j => j.Job)
                .Include(c => c.Origin)
                .First(c => c.Id == characterId);

            var groupResult = new
            {
                id = character.Id,
                name = character.Name,
                origin = character.Origin.Name,
                job = character.CharacterJobs.FirstOrDefault()?.Job.Name,
                fromGroup
            };

            var characterResult = new
            {
                id = group.Id,
                name = group.Name
            };

            object result;
            if (fromGroup)
            {
                result = groupResult;
                if (group.MasterId != CurrentUser.Id)
                {
                    return Error(HttpStatusCode.Forbidden, "not_authorized");
                }
            }
            else
            {
                result = characterResult;
            }

            WebsocketManager.SendMessage("groupInvite", ObservedElementType.Character, characterId, characterResult);
            WebsocketManager.SendMessage("groupInvite", ObservedElementType.Group, groupId, groupResult);

            DbContext.GroupInvitations.Add(groupInvitations);
            DbContext.SaveChanges();

            return Json(result);
        }

        [LoggedUserFilter]
        public IActionResult GroupDetail([FromBody] dynamic inputData)
        {
            int groupId = inputData.groupId;

            Group group = DbContext.Group
                .Include(g => g.Character)
                .Include(g => g.Location)
                .Include(g => g.GroupInvitations)
                .First(g => g.Id == groupId);

            if (group.MasterId != CurrentUser.Id)
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            return Json(group);
        }

        [LoggedUserFilter]
        public IActionResult CreateGroup([FromBody] dynamic inputData)
        {
            string name = inputData.name;

            var group = new Group();
            group.Name = name;
            group.MasterId = CurrentUser.Id;
            group.LocationId = 1;

            DbContext.Group.Add(group);
            DbContext.SaveChanges();

            return Json(group);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> JoinGroup([FromBody] dynamic inputData)
        {
            int characterId = inputData.characterId;
            int groupId = inputData.groupId;

            GroupInvitations invitation = DbContext.GroupInvitations
                .Include(gi => gi.Character)
                .Include(gi => gi.Group)
                .First(gi => gi.GroupId == groupId && gi.CharacterId == characterId);
            if (invitation.FromGroup)
            {
                if (invitation.Character.UserId != CurrentUser.Id)
                {
                    return Error(HttpStatusCode.Forbidden, "not_authorized");
                }
            }
            else
            {
                if (invitation.Group.MasterId != CurrentUser.Id)
                {
                    return Error(HttpStatusCode.Forbidden, "not_authorized");
                }
            }

            invitation.Character.GroupId = groupId;
            DbContext.Remove(invitation);
            DbContext.SaveChanges();

            var res = new
            {
                group = new
                {
                    id = invitation.Group.Id,
                    name = invitation.Group.Name,
                },
                character = new
                {
                    id = invitation.Character.Id,
                    name = invitation.Character.Name,
                }
            };

            var character = await GetCharacterForUser(characterId);
            WebsocketManager.SendMessage("joinGroup", ObservedElementType.Character, characterId, res);
            WebsocketManager.SendMessage("joinCharacter", ObservedElementType.Group, groupId, character);

            return Json(res);
        }

        [LoggedUserFilter]
        public IActionResult ListGroups()
        {
            User user = GetCurrentUser();
            if (user == null)
            {
                HttpContext.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                return Json(null);
            }

            var groups = DbContext.Group
                .Where(g => g.MasterId == user.Id)
                .Include(g => g.Character)
                .ToList();

            var groupsResume = groups.Select((g) =>
            {
                dynamic groupResume = new ExpandoObject();
                groupResume.id = g.Id;
                groupResume.name = g.Name;
                groupResume.characterCount = g.Character.Count;
                return groupResume;
            });

            return Json(groupsResume);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Create([FromBody] JObject inputData)
        {
            var stats = inputData.Value<JObject>("stats");
            var character = new Character
            {
                Name = inputData.Value<string>("name"),
                FatePoint = inputData.Value<short>("fatePoint"),
                Sexe = inputData.Value<string>("sex"),
                Cou = stats.Value<short>("COU"),
                Int = stats.Value<short>("INT"),
                Cha = stats.Value<short>("CHA"),
                Ad = stats.Value<short>("AD"),
                Fo = stats.Value<short>("FO"),
                Level = 1,
                Active = true,
                OriginId = inputData.Value<int>("origin"),
                IsNpc = inputData.Value<bool>("isNpc"),
                GroupId = inputData.Value<int?>("groupId"),
                UserId = CurrentUser.Id
            };

            int money = inputData.Value<int>("money");
            int? money2 = inputData.Value<int?>("money2");
            if (money2.HasValue)
            {
                money += money2.Value;
            }

            using (SHA256 algorithm = SHA256.Create())
            {
                var hash = algorithm.ComputeHash(Encoding.ASCII.GetBytes(character.Name));
                character.Color = hash[0].ToString("X2") + hash[1].ToString("X2") + hash[2].ToString("X2");
            }

            if (inputData["modifiedStat"] != null)
            {
                var modifiedStats = inputData.Value<JObject>("modifiedStat");
                foreach (var m in modifiedStats)
                {
                    var modifiedStat = (JObject) m.Value;
                    var characterModifier = new CharacterModifier
                    {
                        Name = modifiedStat.Value<string>("name") ?? m.Key,
                        Permanent = true
                    };
                    modifiedStat.Remove("name");

                    foreach (var v in modifiedStat)
                    {
                        var value = new CharacterModifierValue
                        {
                            StatName = v.Key.ToUpper(),
                            Value = (short) v.Value
                        };
                        characterModifier.CharacterModifierValue.Add(value);
                    }
                    character.CharacterModifiers.Add(characterModifier);
                }
            }

            if (inputData["speciality"] != null)
            {
                var characterSpeciality = new CharacterSpeciality
                {
                    SpecialityId = inputData.Value<int>("speciality")
                };
                character.CharacterSpecialities.Add(characterSpeciality);
            }

            var skills = inputData.Value<JArray>("skills");
            foreach (JToken skillId in skills)
            {
                var characterSkill = new CharacterSkills()
                {
                    SkillId = (int) skillId
                };
                character.CharacterSkills.Add(characterSkill);
            }

            DbContext.Character.Add(character);
            await DbContext.SaveChangesAsync();

            ItemTemplate purseItemTemplate;
            if (money <= 50)
            {
                purseItemTemplate = await DbContext.ItemTemplate.FirstAsync(it => it.TechName == "SMALL_PURSE");
            }
            else if (money <= 100)
            {
                purseItemTemplate = await DbContext.ItemTemplate.FirstAsync(it => it.TechName == "MEDIUM_PURSE");
            }
            else
            {
                purseItemTemplate = await DbContext.ItemTemplate.FirstAsync(it => it.TechName == "BIG_PURSE");
            }
            var purseItem = await ItemHelper.CreateItemFor(purseItemTemplate, null, CurrentUser, equiped: true, character: character);
            var moneyItemTemplate = await DbContext.ItemTemplate.FirstAsync(it => it.TechName == "MONEY");
            var moneyItem = await ItemHelper.CreateItemFor(moneyItemTemplate, money, CurrentUser, character: character);
            moneyItem.Container = purseItem.Id;

            var jobId = inputData.Value<int?>("job");
            if (jobId.HasValue)
            {
                var characterJob = new CharacterJob
                {
                    CharacterId = character.Id,
                    JobId = jobId.Value,
                    Order = 1
                };
                DbContext.CharacterJob.Add(characterJob);
            }


            await DbContext.SaveChangesAsync();

            return Json(new {id = character.Id});
        }

        [LoggedUserFilter]
        public async Task<IActionResult> CreateCustom([FromBody] JObject inputData)
        {
            var stats = inputData.Value<JObject>("stats");
            var character = new Character
            {
                Name = inputData.Value<string>("name"),
                FatePoint = inputData.Value<short>("fatePoint"),
                Sexe = inputData.Value<string>("sex"),
                Cou = stats.Value<short>("cou"),
                Int = stats.Value<short>("int"),
                Cha = stats.Value<short>("cha"),
                Ad = stats.Value<short>("ad"),
                Fo = stats.Value<short>("fo"),
                Level = inputData.Value<short>("level"),
                Experience = inputData.Value<long>("xp"),
                Active = true,
                OriginId = inputData.Value<int>("originId"),
                IsNpc = inputData.Value<bool>("isNpc"),
                GroupId = inputData.Value<int?>("groupId"),
                UserId = CurrentUser.Id
            };

            using (SHA256 algorithm = SHA256.Create())
            {
                var hash = algorithm.ComputeHash(Encoding.ASCII.GetBytes(character.Name));
                character.Color = hash[0].ToString("X2") + hash[1].ToString("X2") + hash[2].ToString("X2");
            }

            if (inputData["specialities"] != null)
            {
                var jobsSpecialities = inputData.Value<JObject>("specialities");
                foreach (var jobSpecialities in jobsSpecialities.Properties())
                {
                    var specialitiesIds = jobSpecialities.Value.ToObject<JArray>();
                    foreach (var specialityId in specialitiesIds)
                    {
                        var characterSpeciality = new CharacterSpeciality
                        {
                            SpecialityId = (int) specialityId
                        };
                        character.CharacterSpecialities.Add(characterSpeciality);
                    }
                }
            }

            var skills = inputData.Value<JArray>("skills");
            foreach (var skillId in skills)
            {
                var characterSkill = new CharacterSkills()
                {
                    SkillId = (int) skillId
                };
                character.CharacterSkills.Add(characterSkill);
            }

            DbContext.Character.Add(character);
            await DbContext.SaveChangesAsync();

            var jobIds= inputData.Value<JArray>("jobsIds");
            foreach (var jobId in jobIds)
            {
                var characterJob = new CharacterJob
                {
                    CharacterId = character.Id,
                    JobId = (int)jobId,
                    Order = 1
                };
                DbContext.CharacterJob.Add(characterJob);
            }

            await DbContext.SaveChangesAsync();

            return Json(new {id = character.Id});
        }

        [LoggedUserFilter]
        public IActionResult LoadLoots([FromBody] JObject inputData)
        {
            int characterId = inputData.Value<int>("characterId");

            Character character = DbContext.Character
                .Include(c => c.Group)
                .First(c => c.Id == characterId);

            if (!character.IsEditableBy(CurrentUser))
            {
                return Error(HttpStatusCode.Forbidden, "not_authorized");
            }

            if (character.GroupId == null)
            {
                return Json(new string[0]);
            }

            var loots = DbContext.Loot
                .Include(l => l.Monsters)
                .ThenInclude(m => m.Items)
                .ThenInclude(i => i.ItemTemplate)
                .Include(l => l.Items)
                .ThenInclude(i => i.ItemTemplate)
                .Where(l => l.VisibleForPlayer && l.GroupId == character.GroupId);

            return Json(loots);
        }
    }
}