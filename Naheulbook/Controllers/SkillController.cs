using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Models;

namespace Naheulbook.Controllers
{
    public class SkillController : NaheulbookController
    {
        public SkillController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }

        public IActionResult List()
        {
            var li = DbContext.Skill
                .Include(s => s.SkillEffects)
                .ToList();

            return Json(li);
        }
    }
}