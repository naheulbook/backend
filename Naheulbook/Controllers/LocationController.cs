﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Controllers.Filters;
using Naheulbook.Models;

namespace Naheulbook.Controllers
{
    public class LocationController : NaheulbookController
    {
        public LocationController(NaheulbookDbContext dbContext) : base(dbContext)
        {
        }

        public IActionResult List()
        {
            var locations = DbContext.Location
                .ToList();

            return Json(locations);
        }

        public IActionResult Maps([FromBody] dynamic inputData)
        {
            int locationId = inputData.locationId;

            var locationMaps = DbContext.LocationMap
                .Where(m => m.LocationId == locationId)
                .ToList();

            return Json(locationMaps);
        }

        public IActionResult Search([FromBody] dynamic inputData)
        {
            string filter = inputData.filter;
            filter = filter.ToUpper();

            var locations = DbContext.Location
                .Where(location => location.Name.ToUpper().Contains(filter))
                .Take(7)
                .ToList();

            return Json(locations);
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Detail([FromBody] dynamic inputData)
        {
            int locationId = inputData.locationId;

            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }

            Location location = await DbContext.Location
                .Include(l => l.LocationMap)
                .Include(l => l.Sons)
                .FirstAsync(l => l.Id == locationId);

            foreach (Location locationSon in location.Sons)
            {
                locationSon.ParentId = null;
                locationSon.Parent = null;
                locationSon.LocationMap = null;
            }
            return Json(location);
        }

        [LoggedUserFilter]
        public IActionResult ListMapImages([FromBody] dynamic inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }
            throw new NotImplementedException();
        }

        [LoggedUserFilter]
        public async Task<IActionResult> Create([FromBody] dynamic inputData)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }

            string name = inputData.locationName;
            long parentId = inputData.parentId;

            var location = new Location
            {
                Name = name,
                ParentId = parentId
            };

            DbContext.Add(location);
            await DbContext.SaveChangesAsync();

            return Json(location);
        }

        [LoggedUserFilter]
        public IActionResult Edit([FromBody] dynamic data)
        {
            if (!CurrentUser.Admin)
            {
                return Error(HttpStatusCode.Forbidden, "not_admin");
            }
            throw new NotImplementedException();
        }
    }
}