﻿using Newtonsoft.Json;

namespace Naheulbook.Utils.Date
{
    [JsonObject(MemberSerialization.OptIn)]
    public class NhbkDate
    {
        public const int YearDuration = 365;
        [JsonProperty("year")]
        public int Year { get; set; }

        [JsonProperty("day")]
        public int Day { get; set; }

        [JsonProperty("hour")]
        public int Hour { get; set; }

        [JsonProperty("minute")]
        public int Minute { get; set; }

        public void Add(NhbkDateOffset dateOffset)
        {
            Minute += dateOffset.Minute;
            while (Minute >= 60)
            {
                Minute -= 60;
                Hour++;
            }
            Hour += dateOffset.Hour;
            while (Hour >= 24)
            {
                Hour -= 24;
                Day++;
            }
            Day += dateOffset.Day;
            Day += dateOffset.Week * 7;
            while (Day >= YearDuration)
            {
                Day -= YearDuration;
                Year++;
            }
            Year += dateOffset.Year;
        }
    }
}