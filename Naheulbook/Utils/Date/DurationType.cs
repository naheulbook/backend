﻿namespace Naheulbook.Utils.Date
{
    public enum DurationType
    {
        Time,
        Forever,
        Custom,
        Combat,
        Lap,
    }
}