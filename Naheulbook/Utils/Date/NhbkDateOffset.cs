﻿using Newtonsoft.Json;

namespace Naheulbook.Utils.Date
{
    [JsonObject(MemberSerialization.OptIn)]
    public class NhbkDateOffset
    {
        public const int YearDuration = 365 * 24 * 3600;
        public const int WeekDuration = 7 * 24 * 3600;
        public const int DayDuration = 24 * 3600;
        public const int HourDuration = 3600;
        public const int MinuteDuration = 60;

        [JsonProperty("day")]
        public int Day { get; set; }

        [JsonProperty("hour")]
        public int Hour { get; set; }

        [JsonProperty("minute")]
        public int Minute { get; set; }

        [JsonProperty("week")]
        public int Week { get; set; }

        [JsonProperty("year")]
        public int Year { get; set; }

        public int GetDuration()
        {
            return Year * YearDuration
                   + Week * WeekDuration
                   + Day * DayDuration
                   + Hour * HourDuration
                   + Minute * MinuteDuration;
        }
    }
}