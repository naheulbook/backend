﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Models;
using Naheulbook.WebSocket;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Utils.Item
{
    public class ItemHelper
    {
        protected readonly NaheulbookDbContext DbContext;
        protected readonly IWebsocketManager WebsocketManager;

        public ItemHelper(NaheulbookDbContext dbContext, IWebsocketManager websocketManager)
        {
            DbContext = dbContext;
            WebsocketManager = websocketManager;
        }

        public async Task<bool> IsUserAllowedTo(ItemAction action, Models.Item item, User user)
        {
            switch (action)
            {
                case ItemAction.Delete:
                {
                    if (item.LootId != null)
                    {
                        if (item.Loot == null)
                        {
                            await DbContext.Entry(item).Reference(i => i.Loot).Query()
                                .Include(l => l.Group)
                                .LoadAsync();
                        }
                        if (item.Loot != null)
                        {
                            return item.Loot.Group.MasterId == user.Id;
                        }
                    }
                    if (item.MonsterId != null)
                    {
                        if (item.Monster == null)
                        {
                            await DbContext.Entry(item).Reference(i => i.Monster).Query()
                                .Include(m => m.Loot)
                                .Include(m => m.Group)
                                .LoadAsync();
                        }
                        if (item.Monster != null)
                        {
                            return item.Monster.Group.MasterId == user.Id;
                        }
                    }
                    if (item.CharacterId != null)
                    {
                        if (item.Character == null)
                        {
                            await DbContext.Entry(item).Reference(i => i.Character).Query().Include(i => i.Group).LoadAsync();
                        }
                        if (item.Character != null)
                        {
                            return item.Character.IsEditableBy(user);
                        }
                    }
                    return false;
                }
                case ItemAction.Identify:
                {
                    return item.Character.Group.MasterId == user.Id;
                }
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
        }

        public Task DecrementQuantityOrDeleteItem(Models.Item item, User currentUser)
        {
            int quantity = item.Data.ValueOrDefault<int>("quantity");
            if (quantity <= 1)
            {
                return DeleteItem(item, currentUser);
            }
            else
            {
                if (item.ItemTemplate.Data.HasKey("charge"))
                {
                    item.Data["charge"] = item.ItemTemplate.Data["charge"];
                }
                return UpdateQuantity(item, currentUser, quantity - 1);
            }
        }

        public async Task DeleteItem(Models.Item item, User currentUser)
        {
            if (item.LootId != null)
            {
                WebsocketManager.SendMessage("deleteItem", ObservedElementType.Loot, item.LootId.Value, item);
                DbContext.Remove(item);
            }
            else if (item.MonsterId != null)
            {
                WebsocketManager.SendMessage("deleteItem", ObservedElementType.Monster, item.Monster.Id, item);
                DbContext.Remove(item);
            }
            else if (item.CharacterId != null)
            {
                var characterId = item.Character.Id;

                item.CharacterId = null;
                item.Container = null;
                item.Data["equiped"] = 0;

                WebsocketManager.SendMessage("deleteItem", ObservedElementType.Character, characterId, item);

                DbContext.CharacterHistory.Add(new CharacterHistory(item.Character, currentUser, "DELETE_ITEM")
                {
                    ItemId = item.Id
                });
            }
            else
            {
                throw new Exception("Invalid item, no owner");
            }

            await DbContext.SaveChangesAsync();
        }

        public static IQueryable<ItemTemplate> IncludeFullItemTemplate(IQueryable<ItemTemplate> query)
        {
            return query.Include(i => i.ItemTemplateUnskills)
                .Include(i => i.ItemTemplateSkills)
                .Include(i => i.ItemTemplateModifiers)
                .Include(i => i.ItemTemplateRequirements)
                .Include(i => i.ItemTemplateSlot)
                .ThenInclude(i => i.Slot)
                .Include(i => i.ItemTemplateSkillModifiers);
        }

        public static IQueryable<Models.Item> IncludeFullItemTemplate(IQueryable<Models.Item> query)
        {
            return query
                .Include(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateUnskills)
                .Include(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSkills)
                .Include(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateModifiers)
                .Include(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateRequirements)
                .Include(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSlot)
                .ThenInclude(i => i.Slot)
                .Include(i => i.ItemTemplate)
                .ThenInclude(i => i.ItemTemplateSkillModifiers);
        }

        public static IQueryable<ItemTemplateCategory> IncludeFullItemTemplate(IQueryable<ItemTemplateCategory> query)
        {
            return query
                .Include(i => i.ItemTemplates)
                .ThenInclude(i => i.ItemTemplateUnskills)
                .Include(i => i.ItemTemplates)
                .ThenInclude(i => i.ItemTemplateSkills)
                .Include(i => i.ItemTemplates)
                .ThenInclude(i => i.ItemTemplateModifiers)
                .Include(i => i.ItemTemplates)
                .ThenInclude(i => i.ItemTemplateRequirements)
                .Include(i => i.ItemTemplates)
                .ThenInclude(i => i.ItemTemplateSlot)
                .ThenInclude(i => i.Slot)
                .Include(i => i.ItemTemplates)
                .ThenInclude(i => i.ItemTemplateSkillModifiers);
        }

        public ItemTemplate LoadFullItemTemplate(long itemTemplateId)
        {
            var query = IncludeFullItemTemplate(DbContext.ItemTemplate);
            return query.First(i => i.Id == itemTemplateId);
        }

        public Models.Item LoadFullItem(long itemId)
        {
            Models.Item item = DbContext.Item
                .First(i => i.Id == itemId);

            item.ItemTemplate = LoadFullItemTemplate(item.ItemTemplateId);

            return item;
        }

        public Models.Item CopyItem(Models.Item item)
        {
            var copyItem = new Models.Item
            {
                DataJson = item.DataJson,
                ItemTemplateId = item.ItemTemplateId,
                ModifiersJson = item.ModifiersJson
            };

            return copyItem;
        }

        public Task<Models.Item> CreateItemFor(
            ItemTemplate itemTemplate,
            int? quantity,
            User currentUser,
            bool equiped = false,
            Character character = null,
            Loot loot = null,
            Monster monster = null)
        {
            var itemData = new JObject();
            itemData["name"] = itemTemplate.Name;
            if (itemTemplate.Data.ValueOrDefault<bool>("quantifiable"))
            {
                itemData["quantity"] = quantity ?? 1;
            }
            if (equiped)
            {
                itemData["equiped"] = 1;
            }

            return CreateItemFor(itemData, itemTemplate, character, loot, monster, currentUser);
        }

        public Task<Models.Item> CreateItemFor(
            long itemTemplateId,
            int? quantity,
            Character character,
            Loot loot,
            Monster monster,
            User currentUser)
        {
            var itemData = new JObject();
            ItemTemplate itemTemplate = DbContext.ItemTemplate
                .First(i => i.Id == itemTemplateId);

            itemData["name"] = itemTemplate.Name;
            if (itemTemplate.Data.ValueOrDefault<bool>("quantifiable"))
            {
                itemData["quantity"] = quantity ?? 1;
            }

            return CreateItemFor(itemData, itemTemplate, character, loot, monster, currentUser);
        }

        public Task<Models.Item> CreateItemFor(JObject itemData,
            int itemTemplateId,
            Character character,
            Loot loot,
            Monster monster,
            User currentUser)
        {
            ItemTemplate itemTemplate = DbContext.ItemTemplate
                .First(i => i.Id == itemTemplateId);

            return CreateItemFor(itemData, itemTemplate, character, loot, monster, currentUser);
        }

        public async Task<Models.Item> CreateItemFor(JObject itemData,
            ItemTemplate itemTemplate,
            Character character,
            Loot loot,
            Monster monster,
            User currentUser)
        {
            var item = Models.Item.Create(itemTemplate, itemData);
            item.LootId = loot?.Id;
            item.CharacterId = character?.Id;
            item.MonsterId = monster?.Id;

            DbContext.Item.Add(item);
            await DbContext.SaveChangesAsync();

            if (character != null)
            {
                DbContext.CharacterHistory.Add(new CharacterHistory(character, currentUser, "ADD_ITEM")
                {
                    ItemId = item.Id
                });
                await DbContext.SaveChangesAsync();
            }

            Models.Item fullItem = LoadFullItem(item.Id);

            if (character != null)
            {
                WebsocketManager.SendMessage("addItem", ObservedElementType.Character, character.Id, fullItem);
            }
            if (loot != null)
            {
                WebsocketManager.SendMessage("addItem", ObservedElementType.Loot, loot.Id, fullItem);
            }
            if (monster != null)
            {
                WebsocketManager.SendMessage("addItem", ObservedElementType.Monster, monster.Id, fullItem);
            }

            return fullItem;
        }

        public async Task UpdateQuantity(Models.Item item, User currentUser, int newQuantity)
        {
            DbContext.CharacterHistory.Add(new CharacterHistory(item.Character, currentUser, "CHANGE_QUANTITY",
                new
                {
                    oldValue = item.Data["quantity"],
                    newValue = newQuantity
                })
            {
                ItemId = item.Id,
            });

            item.Data["quantity"] = newQuantity;

            await DbContext.SaveChangesAsync();

            WebsocketManager.SendMessage("changeQuantity", ObservedElementType.Character, item.Character.Id, item);
        }
    }
}