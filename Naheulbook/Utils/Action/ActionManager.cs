﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Naheulbook.Models;
using Naheulbook.Utils.Action.Actions;
using Naheulbook.Utils.Item;
using Naheulbook.WebSocket;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Utils.Action
{
    public class ActionManager
    {
        public readonly NaheulbookDbContext DbContext;
        public readonly ItemHelper ItemHelper;
        public readonly IWebsocketManager WebsocketManager;

        public ActionManager(NaheulbookDbContext dbContext, ItemHelper itemHelper, IWebsocketManager websocketManager)
        {
            DbContext = dbContext;
            ItemHelper = itemHelper;
            WebsocketManager = websocketManager;
        }

        protected List<IAction> ParseActions(JArray actionsDatas)
        {
            List<IAction> actions = new List<IAction>();

            foreach (JToken actionData in actionsDatas)
            {
                var jAction = actionData.ToObject<JObject>();
                var type = jAction.Value<string>("type");
                var data = jAction.Value<JObject>("data");

                IAction action = null;
                switch (type)
                {
                    case "addEv":
                    {
                        action = new AddEv(data.Value<short>("ev"));
                        break;
                    }
                    case "addEa":
                    {
                        action = new AddEv(data.Value<short>("ea"));
                        break;
                    }
                    case "addItem":
                    {
                        action = new AddItem(data.Value<int>("templateId"), data.Value<int?>("quantity"));
                        break;
                    }
                    case "removeItem":
                    {
                        action = new DeleteItem();
                        break;
                    }
                    case "addEffect":
                    {
                        action = new AddEffect(data.Value<int>("effectId"), data.Value<JObject>("effectData"));
                        break;
                    }
                    case "addCustomModifier":
                    {
                        action = new AddCustomModifier(data.Value<JObject>("modifier"));
                        break;
                    }
                    case "custom":
                    {
                        // Nothing to do
                        break;
                    }
                }

                if (action != null)
                {
                    actions.Add(action);
                }
            }
            return actions;
        }

        public async Task ExecuteActions(IList<IAction> actions, ActionContext context)
        {
            foreach (IAction action in actions)
            {
                await action.ExecuteAsync(this, context);
            }
        }

        public Task ExecuteItemActions(User currentUser, Models.Item item, Character character)
        {
            if (!item.ItemTemplate.Data.HasKey("actions"))
            {
                return Task.CompletedTask;
            }

            var context = new ActionContext
            {
                CurrentUser = currentUser,
                Character = character,
                Item = item
            };

            var actions = ParseActions(item.ItemTemplate.Data.ValueOrDefault<JArray>("actions"));

            return ExecuteActions(actions, context);
        }
    }
}