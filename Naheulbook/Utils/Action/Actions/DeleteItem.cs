﻿using System.Threading.Tasks;
using Naheulbook.Utils.Item;

namespace Naheulbook.Utils.Action.Actions
{
    public class DeleteItem : IAction
    {
        public async Task ExecuteAsync(ActionManager manager, ActionContext context)
        {
            if (await manager.ItemHelper.IsUserAllowedTo(ItemAction.Delete, context.Item, context.CurrentUser))
            {
                await manager.ItemHelper.DecrementQuantityOrDeleteItem(context.Item, context.CurrentUser);
            }
        }
    }
}