﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Naheulbook.Models;
using Naheulbook.WebSocket;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Utils.Action.Actions
{
    public class AddEffect : IAction
    {
        public readonly int EffectId;
        public readonly JObject EffectData;

        public AddEffect(int effectId, JObject effectData)
        {
            EffectId = effectId;
            EffectData = effectData;
        }

        public async Task ExecuteAsync(ActionManager manager, ActionContext context)
        {
            Effect effect = await manager.DbContext.Effect
                .Include(e => e.Category)
                .Include(e => e.EffectModifier)
                .FirstAsync(e => e.Id == EffectId);
            if (effect == null)
            {
                return;
            }

            var combatCount = effect.CombatCount;
            var timeDuration = effect.TimeDuration;
            var duration = effect.Duration;
            var lapCount = effect.LapCount;
            var durationType = EffectData.Value<string>("durationType");

            switch (durationType)
            {
                case "combat":
                    timeDuration = null;
                    duration = null;
                    combatCount = EffectData.Value<int?>("combatCount");
                    lapCount = null;
                    break;
                case "time":
                    combatCount = null;
                    duration = null;
                    timeDuration = EffectData.Value<int?>("timeDuration");
                    lapCount = null;
                    break;
                case "custom":
                    combatCount = null;
                    duration = EffectData.Value<string>("duration");
                    timeDuration = null;
                    lapCount = null;
                    break;
                case "lap":
                    combatCount = null;
                    duration = null;
                    timeDuration = null;
                    lapCount = EffectData.Value<int?>("lapCount");
                    break;
                case "forever":
                    combatCount = null;
                    duration = null;
                    timeDuration = null;
                    lapCount = null;
                    break;
            }

            
            var characterModifier = new CharacterModifier
            {
                CharacterId = context.Character.Id,
                Name = effect.Name,
                Permanent = false,
                DurationType = durationType,
                Duration = duration,
                Type = effect.Category.Name,
                Description = effect.Description,
                Reusable = false,
                Active = true,
                CombatCount=  combatCount,
                CurrentCombatCount = combatCount,
                TimeDuration = timeDuration,
                CurrentTimeDuration = timeDuration,
                LapCount = lapCount,
                CurrentLapCount = lapCount,
            };

            foreach (var modifier in effect.EffectModifier)
            {
                var characterModifierValue = new CharacterModifierValue
                {
                    StatName = modifier.StatName,
                    Type = modifier.Type,
                    Value = modifier.Value
                };
                characterModifier.CharacterModifierValue.Add(characterModifierValue);
            }

            manager.DbContext.Add(characterModifier);
            await manager.DbContext.SaveChangesAsync();
            // Workaround strange bug reusable switch back to true during save...
            characterModifier.Reusable = false;

            manager.DbContext.Add(new CharacterHistory(context.Character, context.CurrentUser, "APPLY_MODIFIER")
            {
                ModifierId = characterModifier.Id
            });

            await manager.DbContext.SaveChangesAsync();

            manager.WebsocketManager.SendMessage("addModifier", ObservedElementType.Character, context.Character.Id,
                characterModifier);
        }
    }
}