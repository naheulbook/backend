﻿using System.Threading.Tasks;
using Naheulbook.Models;
using Naheulbook.WebSocket;

namespace Naheulbook.Utils.Action.Actions
{
    public class AddEv : IAction
    {
        private readonly short _ev;

        public AddEv(short ev)
        {
            _ev = ev;
        }

        public async Task ExecuteAsync(ActionManager manager, ActionContext context)
        {
            short oldEv = context.Character.Ev ?? 0;
            short newEv = (short) (oldEv + _ev);

            manager.DbContext.CharacterHistory.Add(new CharacterHistory(context.Character, context.CurrentUser,
                "MODIFY_EV",
                new {oldValue = oldEv, newValue = newEv}));

            context.Character.Ev = newEv;

            await manager.DbContext.SaveChangesAsync();

            manager.WebsocketManager.SendMessage("update", ObservedElementType.Character, context.Character.Id,
                new {stat = "ev", value = newEv});
        }
    }
}