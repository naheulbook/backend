﻿using System.Threading.Tasks;

namespace Naheulbook.Utils.Action.Actions
{
    public class AddItem : IAction
    {
        private readonly int _templateId;
        private readonly int? _quantity;

        public AddItem(int templateId, int? quantity)
        {
            _templateId = templateId;
            _quantity = quantity;
        }

        public Task ExecuteAsync(ActionManager manager, ActionContext context)
        {
            if (context.Character.IsEditableBy(context.CurrentUser))
            {
                return manager.ItemHelper.CreateItemFor(_templateId,
                    _quantity,
                    context.Character,
                    null,
                    null,
                    context.CurrentUser);
            }
            return Task.CompletedTask;
        }
    }
}