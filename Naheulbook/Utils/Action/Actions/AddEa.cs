﻿using System.Threading.Tasks;
using Naheulbook.Models;
using Naheulbook.WebSocket;

namespace Naheulbook.Utils.Action.Actions
{
    public class AddEa : IAction
    {
        private readonly short _ea;

        public AddEa(short ea)
        {
            _ea = ea;
        }

        public async Task ExecuteAsync(ActionManager manager, ActionContext context)
        {
            short oldEa = context.Character.Ea ?? 0;
            short newEa = (short) (oldEa + _ea);

            manager.DbContext.CharacterHistory.Add(new CharacterHistory(context.Character, context.CurrentUser,
                "MODIFY_EV",
                new {oldValue = oldEa, newValue = newEa}));

            context.Character.Ea = newEa;

            await manager.DbContext.SaveChangesAsync();

            manager.WebsocketManager.SendMessage("update", ObservedElementType.Character, context.Character.Id,
                new {stat = "ea", value = newEa});
        }
    }
}