﻿using System.Threading.Tasks;
using Naheulbook.Models;
using Naheulbook.WebSocket;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Utils.Action.Actions
{
    public class AddCustomModifier : IAction
    {
        public readonly JObject Modifier;

        public AddCustomModifier(JObject modifier)
        {
            Modifier = modifier;
        }

        public async Task ExecuteAsync(ActionManager manager, ActionContext context)
        {
            var characterModifier = new CharacterModifier
            {
                CharacterId = context.Character.Id,
                Name = Modifier.Value<string>("name"),
                Permanent = false,
                DurationType = Modifier.Value<string>("durationType"),
                Duration = Modifier.Value<string>("duration"),
                Type = Modifier.Value<string>("type"),
                Description = Modifier.Value<string>("description"),
                Reusable = Modifier.Value<bool>("reusable"),
                Active = true,
                CombatCount=  Modifier.Value<int?>("combatCount"),
                CurrentCombatCount = Modifier.Value<int?>("combatCount"),
                TimeDuration = Modifier.Value<int?>("timeDuration"),
                CurrentTimeDuration = Modifier.Value<int?>("timeDuration"),
                LapCount = Modifier.Value<int?>("lapCount"),
                CurrentLapCount = Modifier.Value<int?>("lapCount"),
            };

            var values = Modifier.Value<JArray>("values");
            foreach (JToken value in values)
            {
                var v = (JObject) value;
                var characterModifierValue = new CharacterModifierValue
                {
                    StatName = v.Value<string>("stat"),
                    Type = v.Value<string>("type"),
                    Value = v.Value<short>("value")
                };
                characterModifier.CharacterModifierValue.Add(characterModifierValue);
            }

            manager.DbContext.Add(characterModifier);
            await manager.DbContext.SaveChangesAsync();
            // Workaround strange bug reusable switch back to true during save...
            characterModifier.Reusable = Modifier.Value<bool>("reusable");

            manager.DbContext.Add(new CharacterHistory(context.Character, context.CurrentUser, "APPLY_MODIFIER")
            {
                ModifierId = characterModifier.Id
            });

            await manager.DbContext.SaveChangesAsync();

            manager.WebsocketManager.SendMessage("addModifier", ObservedElementType.Character, context.Character.Id,
                characterModifier);
        }
    }
}