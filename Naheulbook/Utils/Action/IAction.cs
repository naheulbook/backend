﻿using System.Threading.Tasks;

namespace Naheulbook.Utils.Action
{
    public interface IAction
    {
        Task ExecuteAsync(ActionManager manager, ActionContext context);
    }
}