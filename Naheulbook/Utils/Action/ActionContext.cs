﻿using Naheulbook.Models;

namespace Naheulbook.Utils.Action
{
    public class ActionContext
    {
        public User CurrentUser { get; set; }

        /// <summary>
        /// Represent the item that trigger the action when using an item
        /// </summary>
        public Models.Item Item { get; set; }

        /// <summary>
        /// Represent the character that trigger the action
        /// </summary>
        public Character Character { get; set; }
    }
}