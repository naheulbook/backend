using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Utils.Modifier
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ActiveStatsModifier : StatsModifier
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("permanent")]
        public bool Permanent { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("currentCombatCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? CurrentCombatCount { get; set; }

        [JsonProperty("currentLapCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? CurrentLapCount { get; set; }

        [JsonProperty("currentTimeDuration", NullValueHandling = NullValueHandling.Ignore)]
        public int? CurrentTimeDuration { get; set; }
        
        [JsonProperty("lapCountDecrement", NullValueHandling = NullValueHandling.Ignore)]
        public JObject LapCountDecrement { get; set; }

        public void ResetTime()
        {
            CurrentCombatCount = CombatCount;
            CurrentLapCount = LapCount;
            CurrentTimeDuration = TimeDuration;
        }

        public void SetActive(bool active)
        {
            Active = active;
            if (active)
            {
                ResetTime();
            }
        }
    }
}