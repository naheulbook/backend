﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Utils.Modifier
{
    [JsonObject(MemberSerialization.OptIn)]
    public class StatModifier
    {
        [JsonProperty("stat")]
        public string Stat { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("special")]
        public IList<string> Special { get; set; }
    }
}