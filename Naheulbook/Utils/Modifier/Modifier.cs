﻿using System;
using System.Collections.Generic;
using Naheulbook.Utils.Date;
using Newtonsoft.Json;

namespace Naheulbook.Utils.Modifier
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Modifier
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("durationType")]
        public string DurationType { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("timeDuration")]
        public int TimeDuration { get; set; }

        [JsonProperty("combatCount")]
        public int CombatCount { get; set; }

        [JsonProperty("lapCount")]
        public int LapCount { get; set; }

        [JsonProperty("currentTimeDuration")]
        public int CurrentTimeDuration { get; set; }

        [JsonProperty("currentCombatCount")]
        public int CurrentCombatCount { get; set; }

        [JsonProperty("currentLapCount")]
        public int CurrentLapCount { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("reusable")]
        public bool Reusable { get; set; }

        [JsonProperty("values")]
        public IList<StatModifier> Values { get; set; }

        public void UpdateDuration(DurationType type, int durationChange)
        {
            if (!Active)
            {
                return;
            }

            // At the end of a combat, "lap" are reset to 0
            if (DurationType == "lap" && type == Date.DurationType.Combat)
            {
                CurrentLapCount = 0;
                Active = false;
                return;
            }

            if (DurationType != type.ToString().ToLower())
            {
                return;
            }

            switch (type)
            {
                case Date.DurationType.Time:
                    CurrentTimeDuration -= durationChange;
                    if (CurrentTimeDuration <= 0)
                    {
                        CurrentTimeDuration = 0;
                        Active = false;
                    }
                    break;
                case Date.DurationType.Forever:
                    break;
                case Date.DurationType.Custom:
                    break;
                case Date.DurationType.Combat:
                    CurrentCombatCount -= durationChange;
                    if (CurrentCombatCount <= 0)
                    {
                        CurrentCombatCount = 0;
                        Active = false;
                    }
                    break;
                case Date.DurationType.Lap:
                    CurrentLapCount -= durationChange;
                    if (CurrentLapCount <= 0)
                    {
                        CurrentLapCount = 0;
                        Active = false;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}