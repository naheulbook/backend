using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Utils.Modifier
{
    [JsonObject(MemberSerialization.OptIn)]
    public class StatsModifier
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("reusable")]
        public bool Reusable { get; set; }

        [JsonProperty("durationType")]
        public string DurationType { get; set; }

        [JsonProperty("duration", NullValueHandling = NullValueHandling.Ignore)]
        public string Duration { get; set; }

        [JsonProperty("combatCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? CombatCount { get; set; }

        [JsonProperty("lapCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? LapCount { get; set; }

        [JsonProperty("timeDuration", NullValueHandling = NullValueHandling.Ignore)]
        public int? TimeDuration { get; set; }

        [JsonProperty("values")] 
        public List<StatModifier> Values { get; set; } = new List<StatModifier>();
    }
}