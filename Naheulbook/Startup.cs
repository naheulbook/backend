using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Naheulbook.Models;
using Naheulbook.Tool;
using Naheulbook.Utils.Action;
using Naheulbook.Utils.Item;
using Naheulbook.WebSocket;

namespace Naheulbook
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddEntityFrameworkMySql();

            var connection = Configuration["ConnectionStrings:DefaultConnection"];
            services.AddDbContext<NaheulbookDbContext>(options => options.UseMySql(connection));

            services.AddDistributedMemoryCache();
            services.AddSession(options => { options.IdleTimeout = TimeSpan.FromHours(1); });

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IWebsocketManager>(new WebsocketManager());
            services.AddScoped<ItemHelper>();
            services.AddScoped<ActionManager>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IWebsocketManager websocketManager)
        {
            loggerFactory.AddProvider(new EFLoggerProvider());
            loggerFactory.AddDebug();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSession();

            app.UseWebSockets();
            app.Use(async (context, next) =>
            {
                var http = (HttpContext) context;

                if (http.WebSockets.IsWebSocketRequest)
                {
                    System.Net.WebSockets.WebSocket webSocket = await http.WebSockets.AcceptWebSocketAsync();
                    await websocketManager.HandleWebSocket(webSocket);
                }
                else
                {
                    await next();
                }
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });
        }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }
    }
}