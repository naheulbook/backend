﻿using Microsoft.AspNetCore.Hosting;

namespace Naheulbook
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseUrls(args)
                .Build();

            host.Run();
        }
    }
}
