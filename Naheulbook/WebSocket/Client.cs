﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Naheulbook.WebSocket
{
    public class Client
    {
        public WebsocketManager WebsocketManager { get; set; }

        private System.Net.WebSockets.WebSocket Socket { get; }

        private readonly IList<Tuple<ObservedElementType, long>> _registered = new List<Tuple<ObservedElementType, long>>();

        public Client(System.Net.WebSockets.WebSocket socket, WebsocketManager websocketManager)
        {
            Socket = socket;
            WebsocketManager = websocketManager;
        }

        public async Task Recieve()
        {
            try
            {
                var buffer = new ArraySegment<byte>(new byte[4096]);
                while (Socket.State == WebSocketState.Open)
                {
                    WebSocketReceiveResult received = await Socket.ReceiveAsync(buffer, CancellationToken.None);
                    switch (received.MessageType)
                    {
                        case WebSocketMessageType.Text:
                            var request = Encoding.UTF8.GetString(buffer.Array,
                                0,
                                received.Count);
                            try
                            {
                                var message = JsonConvert.DeserializeObject<Message>(request);
                                await RecieveMessage(message);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("======================================================");
                                Console.WriteLine("====================== ERROR =========================");
                                Console.WriteLine("======================================================");
                                Console.WriteLine("Websocket: Exception");
                                Console.WriteLine("Request was `" + request + "'");
                                Console.WriteLine(e.Message);
                                Console.WriteLine(e.StackTrace);
                                Console.WriteLine("======================================================");
                                await Socket.CloseAsync(WebSocketCloseStatus.ProtocolError
                                    , "invalid message"
                                    , CancellationToken.None);
                            }
                            break;
                        case WebSocketMessageType.Binary:
                            break;
                        case WebSocketMessageType.Close:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            finally
            {
                Unregister();
            }
        }

        private void Register(ObservedElementType type, long elementId)
        {
            _registered.Add(new Tuple<ObservedElementType, long>(type, elementId));
            WebsocketManager.RegisterClient(type, elementId, this);
        }

        private void Unregister()
        {
            foreach (var tuple in _registered)
            {
                WebsocketManager.UnregisterClient(tuple.Item1, tuple.Item2, this);
            }
        }

        private Task RecieveMessage(Message msg)
        {
            switch (msg.Opcode)
            {
                case "LISTEN_ELEMENT":
                {
                    if (msg.ElementType == "character")
                    {
                        Register(ObservedElementType.Character, msg.ElementId);
                    }
                    else if (msg.ElementType == "loot")
                    {
                        Register(ObservedElementType.Loot, msg.ElementId);
                    }
                    else if (msg.ElementType == "group")
                    {
                        Register(ObservedElementType.Group, msg.ElementId);
                    }
                    else if (msg.ElementType == "monster")
                    {
                        Register(ObservedElementType.Monster, msg.ElementId);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    break;
                }
                case "STOP_LISTEN_ELEMENT":
                {
                    if (msg.ElementType == "character")
                    {
                        WebsocketManager.UnregisterClient(ObservedElementType.Character, msg.ElementId, this);
                    }
                    else if (msg.ElementType == "loot")
                    {
                        WebsocketManager.UnregisterClient(ObservedElementType.Loot, msg.ElementId, this);
                    }
                    else if (msg.ElementType == "group")
                    {
                        WebsocketManager.UnregisterClient(ObservedElementType.Group, msg.ElementId, this);
                    }
                    else if (msg.ElementType == "monster")
                    {
                        WebsocketManager.UnregisterClient(ObservedElementType.Monster, msg.ElementId, this);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    break;
                }
                default:
                {
                    throw new ArgumentOutOfRangeException();
                }
            }

            return Task.FromResult(0);
        }


        public Task SendMessage<T>(string opcode, ObservedElementType type, long elementId, T data)
        {
            var message = new Message
            {
                Opcode = opcode,
                ElementId =  elementId,
                ElementType = type.ToString().ToLower(),
                Data = data
            };
            return SendMessage(message);
        }

        public Task SendMessage(Message message)
        {
            switch (Socket.State)
            {
                case WebSocketState.Open:
                {
                    var json = JsonConvert.SerializeObject(message, Formatting.None,
                        new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()});
                    var buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(json));
                    return Socket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
                }
                case WebSocketState.Aborted:
                case WebSocketState.Closed:
                case WebSocketState.CloseSent:
                case WebSocketState.CloseReceived:
                case WebSocketState.None:
                    Unregister();
                    break;
                case WebSocketState.Connecting:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return Task.FromResult(0);
        }
    }
}