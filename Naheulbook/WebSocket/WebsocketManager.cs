﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Naheulbook.WebSocket
{
    public class WebsocketManager: IWebsocketManager
    {
        public IDictionary<ObservedElementType, IDictionary<long, HashSet<Client>>> RegisteredClients
            = new ConcurrentDictionary<ObservedElementType, IDictionary<long, HashSet<Client>>>();

        public async Task HandleWebSocket(System.Net.WebSockets.WebSocket webSocket)
        {
            var client = new Client(webSocket, this);
            await client.Recieve();
        }

        public void SendMessage<T>(string opcode, ObservedElementType type, long elementId, T data)
        {
            IDictionary<long, HashSet<Client>> registeredClients;
            if (RegisteredClients.TryGetValue(type, out registeredClients))
            {
                HashSet<Client> clients;
                if (registeredClients.TryGetValue(elementId, out clients))
                {
                    lock (clients)
                    {
                        // FIXME: Wait => async
                        try
                        {
                            Task.WhenAll(clients.Select(c => c.SendMessage(opcode, type, elementId, data))).Wait();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                        }
                    }
                }
            }
        }

        public void RegisterClient(ObservedElementType type, long elementId, Client client)
        {
            IDictionary<long, HashSet<Client>> registeredClients;
            lock (RegisteredClients)
            {
                if (!RegisteredClients.TryGetValue(type, out registeredClients))
                {
                    registeredClients = new ConcurrentDictionary<long, HashSet<Client>>();
                    RegisteredClients.Add(type, registeredClients);
                }
            }

            HashSet<Client> clients;
            lock (registeredClients)
            {
                if (!registeredClients.TryGetValue(elementId, out clients))
                {
                    clients = new HashSet<Client>();
                    registeredClients.Add(elementId, clients);
                }
            }

            lock (clients)
            {
               clients.Add(client);
            }
        }

        public void UnregisterClient(ObservedElementType type, long elementId, Client client)
        {
            IDictionary<long, HashSet<Client>> registeredClients;

            if (RegisteredClients.TryGetValue(type, out registeredClients))
            {
                HashSet<Client> clients;
                if (registeredClients.TryGetValue(elementId, out clients))
                {
                    lock (clients)
                    {
                        clients.Remove(client);
                    }
                }
            }
        }
    }
}