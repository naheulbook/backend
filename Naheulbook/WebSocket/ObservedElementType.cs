﻿namespace Naheulbook.WebSocket
{
    public enum ObservedElementType
    {
        Character,
        Loot,
        Group,
        Monster,
    }
}