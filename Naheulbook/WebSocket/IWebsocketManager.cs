﻿using System.Threading.Tasks;

namespace Naheulbook.WebSocket
{
    public interface IWebsocketManager
    {
        Task HandleWebSocket(System.Net.WebSockets.WebSocket webSocket);

        void SendMessage<T>(string opcode, ObservedElementType type, long elementId, T data);
    }
}