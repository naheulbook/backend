﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.WebSocket
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Message
    {
        [JsonProperty("opcode")]
        public string Opcode { get; set; }

        [JsonProperty("type")]
        public string ElementType { get; set; }

        [JsonProperty("id")]
        public long ElementId { get; set; }

        [JsonProperty("data")]
        public object Data { get; set; }

        public JObject JData => Data as JObject;
    }
}