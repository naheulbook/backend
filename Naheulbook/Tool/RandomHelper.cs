﻿using System.Security.Cryptography;
using System.Text;

namespace Naheulbook.Tool
{
    public static class RandomHelper
    {
        public static string GetUniqueKey(int size)
        {
            RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.Create();

            byte[] data = new byte[size];
            randomNumberGenerator.GetBytes(data);

            char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
    }
}