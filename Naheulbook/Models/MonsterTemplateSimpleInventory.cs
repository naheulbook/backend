﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class MonsterTemplateSimpleInventory
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        public long MonsterTemplateId { get; set; }

        [JsonProperty]
        public int MinCount { get; set; }

        [JsonProperty]
        public int MaxCount { get; set; }

        [JsonProperty]
        public float Chance { get; set; }

        public MonsterTemplate MonsterTemplate { get; set; }

        public long ItemTemplateId { get; set; }

        [JsonProperty("itemTemplate")]
        public virtual ItemTemplate ItemTemplate { get; set; }
    }
}