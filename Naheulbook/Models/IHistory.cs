﻿using System;

namespace Naheulbook.Models
{
    public interface IHistory
    {
        long Id { get; set; }

        DateTime Date { get; set; }

        string Action { get; set; }

        bool Gm { get; set; }

        bool IsGroup { get; }
    }
}