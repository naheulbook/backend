﻿namespace Naheulbook.Models
{
    public partial class JobSkill
    {
        public long Jobid { get; set; }

        public long SkillId { get; set; }

        public bool Default { get; set; }

        public virtual Job Job { get; set; }

        public virtual Skill Skill { get; set; }
    }
}