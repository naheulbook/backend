﻿namespace Naheulbook.Models
{
    public partial class Spell
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public long Cost { get; set; }
        public long CategoryId { get; set; }
        public bool Variablecost { get; set; }
        public string Description { get; set; }
        public long Durationprepare { get; set; }
        public string Durationprepareunit { get; set; }
        public long Durationeffect { get; set; }
        public string Durationeffectunit { get; set; }
        public long Speed { get; set; }
        public long? Distance { get; set; }
        public string Distancenote { get; set; }
        public long Test { get; set; }
        public string TestStatName { get; set; }

        public virtual SpellCategory Category { get; set; }
        public virtual Stat TestStat { get; set; }
    }
}
