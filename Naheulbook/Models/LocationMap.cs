﻿namespace Naheulbook.Models
{
    public partial class LocationMap
    {
        public long Id { get; set; }

        public long LocationId { get; set; }

        public string Name { get; set; }

        public string File { get; set; }

        public bool Gm { get; set; }

        public string Data { get; set; }

        public virtual Location Location { get; set; }
    }
}