﻿using System.Collections.Generic;

namespace Naheulbook.Models
{
    public partial class SpellCategory
    {
        public SpellCategory()
        {
            Spell = new HashSet<Spell>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public bool Default { get; set; }

        public virtual ICollection<Spell> Spell { get; set; }
    }
}
