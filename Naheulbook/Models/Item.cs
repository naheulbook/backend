﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Naheulbook.Tool;
using Naheulbook.Utils.Modifier;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Item
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        public long? CharacterId { get; set; }

        public long? MonsterId { get; set; }

        public long? LootId { get; set; }

        [JsonProperty("container")]
        public long? Container { get; set; }

        public long ItemTemplateId { get; set; }

        [JsonProperty("data")]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string DataJson {
            get { return Data.GetJson(); }
            set { Data.SetJson(value); } }

        [NotMapped]
        public JsonDataField Data { get; } = new JsonDataField();

        [JsonProperty("modifiers")]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string ModifiersJson { get; set; }

        
        [NotMapped]
        public List<ActiveStatsModifier> Modifiers
        {
            get => ModifiersJson == null
                ? new List<ActiveStatsModifier>()
                : JsonConvert.DeserializeObject<List<ActiveStatsModifier>>(ModifiersJson);
            set => ModifiersJson = JArray.FromObject(value).ToString(Formatting.None);
        }

        public virtual string LifetimeType { get; set; }

        public virtual Character Character { get; set; }

        public virtual Monster Monster { get; set; }

        public virtual Loot Loot { get; set; }

        [JsonProperty("template")]
        public virtual ItemTemplate ItemTemplate { get; set; }

        public virtual ICollection<CharacterHistory> CharacterHistory { get; set; }

        public static Item Create(ItemTemplate itemTemplate, JObject itemData)
        {
            var item = new Item();
            if (itemData != null)
            {
                item.DataJson = itemData.ToString(Formatting.None);
            }
            if (item.Data["name"] == null)
            {
                item.Data["name"] = itemTemplate.Name;
            }

            item.ItemTemplateId = itemTemplate.Id;


            if (itemTemplate.Data["charge"]?.Value<int?>() != null)
            {
                item.Data["charge"] = itemTemplate.Data["charge"];
            }
            if (itemTemplate.Data["icon"]?.Value<JObject>() != null)
            {
                item.Data["icon"] = itemTemplate.Data["icon"];
            }
            if (itemTemplate.Data["lifetime"] != null)
            {
                item.Data["lifetime"] = itemTemplate.Data["lifetime"];
            }
            return item;
        }
    }
}