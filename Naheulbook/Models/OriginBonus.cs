﻿using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class OriginBonus
    {
        public long Id { get; set; }

        public long OriginId { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }

        public Origin Origin { get; set; }
    }
}