﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SkillEffect
    {
        public long SkillId { get; set; }

        [JsonProperty("stat")]
        public string StatName { get; set; }

        [JsonProperty]
        public long Value { get; set; }

        [JsonProperty("type")]
        public string Type => "ADD";

        public Skill Skill { get; set; }
    }
}