﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class GroupHistory : IHistory
    {
        public GroupHistory()
        {
        }

        public GroupHistory(Group group, User user, string action, bool gm ,object data = null)
        {
            GroupId = group.Id;
            UserId = user.Id;
            Action = action;
            Date = DateTime.Now;
            Gm = gm;
            if (data != null)
            {
                Data = JsonConvert.SerializeObject(data);
            }
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public DateTime Date { get; set; }

        public long GroupId { get; set; }

        [JsonProperty]
        public string Action { get; set; }

        [JsonProperty]
        public string Info { get; set; }

        [JsonProperty]
        public bool Gm { get; set; }

        public long? UserId { get; set; }

        [JsonProperty("isGroup")]
        [NotMapped]
        public bool IsGroup => true;

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Data { get; set; }

        public virtual Group Group { get; set; }

        public virtual User User { get; set; }
    }
}