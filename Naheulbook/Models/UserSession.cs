﻿using System;

namespace Naheulbook.Models
{
    public class UserSession
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public DateTime Start { get; set; }

        public DateTime Expire { get; set; }

        public string Key { get; set; }

        public string Ip { get; set; }

        public User User { get; set; }
    }
}