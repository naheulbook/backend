﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Naheulbook.Tool;
using Naheulbook.Utils.Modifier;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Monster
    {
        public Monster()
        {
            Items = new HashSet<Item>();
        }

        [JsonProperty]
        public long Id { get; set; }

        public long GroupId { get; set; }

        public long? LootId { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Data { get; set; }

        [NotMapped]
        public dynamic DataObject => JsonConvert.SerializeObject(Data);

        [JsonProperty("modifiers")]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string ModifiersJson { get; set; }

        [NotMapped]
        public List<ActiveStatsModifier> Modifiers
        {
            get => ModifiersJson == null
                ? new List<ActiveStatsModifier>()
                : JsonConvert.DeserializeObject<List<ActiveStatsModifier>>(ModifiersJson);
            set => ModifiersJson = JArray.FromObject(value).ToString(Formatting.None);
        }

        [JsonProperty]
        public DateTime? Dead { get; set; }

        public long? TargetCharacterId { get; set; }

        public long? TargetMonsterId { get; set; }

        public virtual Group Group { get; set; }

        public virtual Loot Loot { get; set; }

        public virtual Character TargetCharacter { get; set; }

        public virtual Monster TargetMonster { get; set; }

        public virtual ICollection<Character> InverseTargetCharacter { get; set; }

        public virtual ICollection<Monster> InverseTargetMonsters { get; set; }

        [NotMapped]
        [JsonProperty("target")]
        public object Target
        {
            get
            {
                if (TargetMonsterId != null)
                {
                    return new
                    {
                        isMonster = true,
                        id = TargetMonsterId
                    };
                }
                if (TargetCharacterId != null)
                {
                    return new
                    {
                        isMonster = false,
                        id = TargetCharacterId
                    };
                }
                return null;
            }
        }

        [JsonProperty("items")]
        public virtual ICollection<Item> Items { get; set; }
    }
}