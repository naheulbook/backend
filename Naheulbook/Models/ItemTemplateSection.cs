﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ItemTemplateSection
    {
        public ItemTemplateSection()
        {
            ItemCategory = new HashSet<ItemTemplateCategory>();
        }

        [JsonProperty]
        public long Id { get; set; }
        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public string Note { get; set; }
        public string Special { get; set; }

        [JsonProperty("special")]
        public string[] SpecialArray => Special.Split(',');

        [JsonProperty("categories")]
        public virtual ICollection<ItemTemplateCategory> ItemCategory { get; set; }
    }
}
