﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Loot
    {
        public Loot()
        {
            Items = new HashSet<Item>();
            Monsters = new HashSet<Monster>();
        }

        [JsonProperty]
        public long Id { get; set; }

        public long GroupId { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public DateTime? Created { get; set; }

        [JsonProperty]
        public bool VisibleForPlayer { get; set; } = false;

        public Group Group { get; set; }

        public Group CurrentCombatLootForGroup { get; set; }

        [JsonProperty("items")]
        public ICollection<Item> Items { get; set; }

        [JsonProperty("monsters")]
        public ICollection<Monster> Monsters { get; set; }
    }
}