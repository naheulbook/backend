﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ItemTemplateModifier
    {
        [JsonProperty]
        public long Id { get; set; }

        public long ItemTemplateId { get; set; }

        [JsonProperty("stat")]
        public string StatName { get; set; }

        [JsonProperty]
        public long Value { get; set; }

        [JsonProperty("job")]
        public long? RequireJobId { get; set; }

        [JsonProperty("origin")]
        public long? RequireOriginId { get; set; }

        [JsonProperty]
        // FIXME: string to array
        public string Special { get; set; }

        [JsonProperty]
        public string Type { get; set; }

        public virtual ItemTemplate ItemTemplate { get; set; }

        public virtual Job RequireJob { get; set; }

        public virtual Origin RequireOrigin { get; set; }

        public virtual Stat Stat { get; set; }
    }
}