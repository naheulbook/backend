﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class CharacterModifierValue
    {
        public long Id { get; set; }
        public long CharacterModifierId { get; set; }

        [JsonProperty("stat")]
        public string StatName { get; set; }

        [JsonProperty("value")]
        public short Value { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        public virtual CharacterModifier CharacterModifier { get; set; }

        public virtual Stat Stat { get; set; }
    }
}