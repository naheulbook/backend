﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class God
    {
        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string DisplayName { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        [JsonProperty]
        public string TechName { get; set; }
    }
}