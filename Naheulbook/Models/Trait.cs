﻿namespace Naheulbook.Models
{
    public partial class Trait
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Level1 { get; set; }
        public string Level2 { get; set; }
        public string Level3 { get; set; }
    }
}
