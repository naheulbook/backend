﻿namespace Naheulbook.Models
{
    public partial class OriginInfo
    {
        public long Id { get; set; }

        public long OriginId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public virtual Origin Origin { get; set; }
    }
}