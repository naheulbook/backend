﻿using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class OriginRestrict
    {
        public long Id { get; set; }

        public long Originid { get; set; }

        [JsonProperty("description")]
        public string Text { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }

        public Origin Origin { get; set; }
    }
}