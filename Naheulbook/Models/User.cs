﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class User
    {
        public User()
        {
            Character = new HashSet<Character>();
            CharacterHistory = new HashSet<CharacterHistory>();
            Group = new HashSet<Group>();
            GroupHistory = new HashSet<GroupHistory>();
            UserSessions = new HashSet<UserSession>();
        }

        [JsonProperty]
        public long Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        [JsonProperty("admin")]
        public bool Admin { get; set; }

        public string FbId { get; set; }

        public string GoogleId { get; set; }

        public string LiveId { get; set; }

        public string TwitterId { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("linkedWithFb")]
        public bool LinkedWithFb => FbId != null;

        [JsonProperty("linkedWithGoogle")]
        public bool LinkWithGoogle => GoogleId != null;

        [JsonProperty("linkedWithTwitter")]
        public bool LinkWithTwitter => TwitterId != null;

        [JsonProperty("linkedWithLive")]
        public bool LinkedWithLive => LiveId != null;

        public ICollection<Character> Character { get; set; }

        public ICollection<CharacterHistory> CharacterHistory { get; set; }

        public ICollection<Group> Group { get; set; }

        public ICollection<GroupHistory> GroupHistory { get; set; }

        public ICollection<UserSession> UserSessions { get; set; }

        public IEnumerable<ItemTemplate> ItemTemplates { get; set; }
    }
}