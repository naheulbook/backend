﻿using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Models
{
    public class JsonDataField
    {
        private JObject _ob;

        public JToken this[string field]
        {
            get
            {
                if (_ob == null)
                {
                    _ob = new JObject();
                }
                return _ob[field];
            }
            set
            {
                if (_ob == null)
                {
                    _ob = new JObject();
                }
                _ob[field] = value;
            }
        }

        public T ValueOrDefault<T>(string field)
        {
            if (_ob == null)
            {
                _ob = new JObject();
            }
            return _ob[field] == null ? default(T) : _ob[field].Value<T>();
        }

        public T ValueOrDefault<T>(string field, T defaultValue)
        {
            if (_ob == null)
            {
                _ob = new JObject();
            }
            return _ob[field] == null ? defaultValue : _ob[field].Value<T>();
        }

        public string GetJson()
        {
            if (_ob == null)
            {
                _ob = new JObject();
            }
            return _ob.ToString(Formatting.None);
        }

        public void SetJson(string value)
        {
            _ob = value == null ? new JObject() : JObject.Parse(value);
        }

        public void Set(object value)
        {
            _ob = JObject.FromObject(value);
        }

        public void Remove(string field)
        {
            _ob?.Remove(field);
        }

        public bool HasKey(string key)
        {
            return _ob.Properties().Any(p => p.Name == key);
        }
    }
}