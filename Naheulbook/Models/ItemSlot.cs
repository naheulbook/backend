﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ItemSlot
    {
        public ItemSlot()
        {
            ItemTemplateSlot = new HashSet<ItemTemplateSlot>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        public bool? Stackable { get; set; }

        public short Count { get; set; }

        [JsonProperty("techName")]
        public string Techname { get; set; }

        public ICollection<ItemTemplateSlot> ItemTemplateSlot { get; set; }
    }
}