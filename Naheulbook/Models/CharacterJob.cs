﻿namespace Naheulbook.Models
{
    public class CharacterJob
    {
        public long CharacterId { get; set; }

        public long JobId { get; set; }

        public int Order { get; set; }

        public Character Character { get; set; }

        public Job Job { get; set; }
    }
}