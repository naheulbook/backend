﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class EffectType
    {
        public EffectType()
        {
            Categories = new HashSet<EffectCategory>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public ICollection<EffectCategory> Categories { get; set; }
    }
}