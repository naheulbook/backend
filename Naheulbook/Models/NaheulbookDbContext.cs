﻿using Microsoft.EntityFrameworkCore;

namespace Naheulbook.Models
{
    public class NaheulbookDbContext : DbContext
    {
        public virtual DbSet<Calendar> Calendar { get; set; }
        public virtual DbSet<Character> Character { get; set; }
        public virtual DbSet<CharacterHistory> CharacterHistory { get; set; }
        public virtual DbSet<CharacterJob> CharacterJob { get; set; }
        public virtual DbSet<CharacterModifier> CharacterModifier { get; set; }
        public virtual DbSet<CharacterModifierValue> CharacterModifierValue { get; set; }
        public virtual DbSet<CharacterSkills> CharacterSkills { get; set; }
        public virtual DbSet<CharacterSpeciality> CharacterSpeciality { get; set; }
        public virtual DbSet<Effect> Effect { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EffectCategory> EffectCategory { get; set; }
        public virtual DbSet<EffectModifier> EffectModifier { get; set; }
        public virtual DbSet<EffectType> EffectType { get; set; }
        public virtual DbSet<ErrorReport> ErrorReport { get; set; }
        public virtual DbSet<God> God { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupHistory> GroupHistory { get; set; }
        public virtual DbSet<GroupInvitations> GroupInvitations { get; set; }
        public virtual DbSet<Icon> Icon { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemSlot> ItemSlot { get; set; }
        public virtual DbSet<ItemTemplate> ItemTemplate { get; set; }
        public virtual DbSet<ItemTemplateCategory> ItemTemplateCategory { get; set; }
        public virtual DbSet<ItemTemplateModifier> ItemTemplateModifier { get; set; }
        public virtual DbSet<ItemTemplateRequirement> ItemTemplateRequirement { get; set; }
        public virtual DbSet<ItemTemplateSection> ItemTemplateSection { get; set; }
        public virtual DbSet<ItemTemplateSkill> ItemTemplateSkill { get; set; }
        public virtual DbSet<ItemTemplateSkillModifiers> ItemTemplateSkillModifiers { get; set; }
        public virtual DbSet<ItemTemplateSlot> ItemTemplateSlot { get; set; }
        public virtual DbSet<ItemTemplateUnskill> ItemTemplateUnskill { get; set; }
        public virtual DbSet<ItemType> ItemType { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<JobBonus> JobBonus { get; set; }
        public virtual DbSet<JobOriginBlacklist> JobOriginBlacklist { get; set; }
        public virtual DbSet<JobOriginWhitelist> JobOriginWhitelist { get; set; }
        public virtual DbSet<JobRequirement> JobRequirement { get; set; }
        public virtual DbSet<JobRestrict> JobRestrict { get; set; }
        public virtual DbSet<JobSkill> JobSkill { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationMap> LocationMap { get; set; }
        public virtual DbSet<Loot> Loot { get; set; }
        public virtual DbSet<Monster> Monster { get; set; }
        public virtual DbSet<MonsterCategory> MonsterCategory { get; set; }
        public virtual DbSet<MonsterLocation> MonsterLocation { get; set; }
        public virtual DbSet<MonsterTemplate> MonsterTemplate { get; set; }
        public virtual DbSet<MonsterTemplateSimpleInventory> MonsterTemplateSimpleInventory { get; set; }
        public virtual DbSet<MonsterTrait> MonsterTrait { get; set; }
        public virtual DbSet<MonsterType> MonsterType { get; set; }
        public virtual DbSet<Origin> Origin { get; set; }
        public virtual DbSet<OriginBonus> OriginBonus { get; set; }
        public virtual DbSet<OriginInfo> OriginInfo { get; set; }
        public virtual DbSet<OriginRequirement> OriginRequirement { get; set; }
        public virtual DbSet<OriginRestrict> OriginRestrict { get; set; }
        public virtual DbSet<OriginSkill> OriginSkill { get; set; }
        public virtual DbSet<Quest> Quest { get; set; }
        public virtual DbSet<QuestTemplate> QuestTemplate { get; set; }
        public virtual DbSet<Skill> Skill { get; set; }
        public virtual DbSet<SkillEffect> SkillEffect { get; set; }
        public virtual DbSet<Speciality> Speciality { get; set; }
        public virtual DbSet<SpecialityModifier> SpecialityModifier { get; set; }
        public virtual DbSet<SpecialitySpecial> SpecialitySpecial { get; set; }
        public virtual DbSet<Spell> Spell { get; set; }
        public virtual DbSet<SpellCategory> SpellCategory { get; set; }
        public virtual DbSet<Stat> Stat { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserSession> UserSession { get; set; }

        public NaheulbookDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Calendar>(entity =>
            {
                entity.ToTable("calendar");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.EndDay)
                    .HasColumnName("endday");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Note)
                    .HasColumnName("note");

                entity.Property(e => e.StartDay)
                    .HasColumnName("startday");
            });

            modelBuilder.Entity<Character>(entity =>
            {
                entity.ToTable("character");

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.OriginId);

                entity.HasIndex(e => e.TargetCharacterId);

                entity.HasIndex(e => e.TargetMonsterId);

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Ad)
                    .HasColumnName("ad");

                entity.Property(e => e.Cha)
                    .HasColumnName("cha");

                entity.Property(e => e.Color)
                    .IsRequired()
                    .HasColumnName("color")
                    .HasMaxLength(6)
                    .HasDefaultValueSql("'22DD22'");

                entity.Property(e => e.Cou)
                    .HasColumnName("cou");

                entity.Property(e => e.Ea)
                    .HasColumnName("ea");

                entity.Property(e => e.Ev)
                    .HasColumnName("ev");

                entity.Property(e => e.Experience)
                    .HasColumnName("experience");

                entity.Property(e => e.FatePoint)
                    .HasColumnName("fatepoint")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Fo)
                    .HasColumnName("fo");

                entity.Property(e => e.GmDataJson)
                    .HasColumnName("gmdata")
                    .HasColumnType("json");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group");

                entity.Property(e => e.Int)
                    .HasColumnName("int");

                entity.Property(e => e.IsNpc)
                    .HasColumnName("isnpc")
                    .IsRequired()
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Level)
                    .HasColumnName("level");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.OriginId)
                    .HasColumnName("origin");

                entity.Property(e => e.Sexe)
                    .IsRequired()
                    .HasColumnName("sexe")
                    .HasMaxLength(32);

                entity.Property(e => e.StatBonusAD)
                    .HasColumnName("statbonusad")
                    .HasMaxLength(16);

                entity.Property(e => e.TargetCharacterId)
                    .HasColumnName("targetcharacterid");

                entity.Property(e => e.TargetMonsterId)
                    .HasColumnName("targetmonsterid");

                entity.Property(e => e.UserId)
                    .HasColumnName("user");

                entity.HasOne(e => e.TargetCharacter)
                    .WithMany(e => e.InverseTargetCharacter)
                    .HasForeignKey(e => e.TargetCharacterId)
                    .OnDelete(DeleteBehavior.SetNull);

                entity.HasOne(e => e.TargetMonster)
                    .WithMany(e => e.InverseTargetCharacter)
                    .HasForeignKey(e => e.TargetMonsterId)
                    .OnDelete(DeleteBehavior.SetNull);

                entity.HasOne(e => e.Origin)
                    .WithMany(j => j.Characters)
                    .HasForeignKey(e => e.OriginId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.User)
                    .WithMany(j => j.Character)
                    .HasForeignKey(e => e.UserId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasMany(j => j.CharacterJobs)
                    .WithOne(e => e.Character)
                    .HasForeignKey(e => e.CharacterId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<CharacterHistory>(entity =>
            {
                entity.ToTable("character_history");

                entity.HasIndex(e => e.CharacterId);

                entity.HasIndex(e => e.EffectId);

                entity.HasIndex(e => e.ItemId);

                entity.HasIndex(e => e.ModifierId);

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasColumnName("action")
                    .HasMaxLength(255);

                entity.Property(e => e.CharacterId)
                    .HasColumnName("character");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EffectId)
                    .HasColumnName("effect");

                entity.Property(e => e.Gm)
                    .HasColumnName("gm")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Info)
                    .HasColumnName("info");

                entity.Property(e => e.ItemId)
                    .HasColumnName("item");

                entity.Property(e => e.ModifierId)
                    .HasColumnName("modifier");

                entity.Property(e => e.UserId)
                    .HasColumnName("user");

                entity.HasOne(e => e.Item)
                    .WithMany(c => c.CharacterHistory)
                    .HasForeignKey(e => e.ItemId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.Character)
                    .WithMany(c => c.History)
                    .HasForeignKey(e => e.CharacterId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(e => e.User)
                    .WithMany(u => u.CharacterHistory)
                    .HasForeignKey(e => e.UserId)
                    .OnDelete(DeleteBehavior.SetNull);

                entity.HasOne(e => e.Effect)
                    .WithMany(c => c.CharacterHistory)
                    .HasForeignKey(e => e.EffectId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<CharacterJob>(entity =>
            {
                entity.ToTable("character_job");

                entity.HasKey(job => new {job.CharacterId, job.JobId});

                entity.Property(e => e.CharacterId)
                    .HasColumnName("character");

                entity.Property(e => e.JobId)
                    .HasColumnName("job");

                entity.Property(e => e.Order)
                    .HasColumnName("order");

                entity.HasOne(j => j.Job)
                    .WithMany(e => e.CharacterJobs)
                    .HasForeignKey(e => e.JobId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<CharacterModifier>(entity =>
            {
                entity.ToTable("character_modifier");

                entity.HasIndex(e => e.CharacterId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .IsRequired()
                    .HasDefaultValueSql("true");

                entity.Property(e => e.CharacterId)
                    .HasColumnName("character");

                entity.Property(e => e.CombatCount)
                    .HasColumnName("combatcount");

                entity.Property(e => e.CurrentCombatCount)
                    .HasColumnName("currentcombatcount");

                entity.Property(e => e.LapCount)
                    .HasColumnName("lapcount");

                entity.Property(e => e.CurrentLapCount)
                    .HasColumnName("currentlapcount");

                entity.Property(e => e.CurrentTimeDuration)
                    .HasColumnName("currenttimeduration");

                entity.Property(e => e.DurationType)
                    .HasColumnName("durationtype");

                entity.Property(e => e.Duration)
                    .HasColumnName("duration");

                entity.Property(e => e.LapCountDecrementJson)
                    .HasColumnName("lapCountDecrement");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("text");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Permanent)
                    .HasColumnName("permanent");

                entity.Property(e => e.Reusable)
                    .HasColumnName("reusable")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.TimeDuration)
                    .HasColumnName("timeduration");
            });

            modelBuilder.Entity<CharacterModifierValue>(entity =>
            {
                entity.ToTable("character_modifier_value");

                entity.HasIndex(e => e.CharacterModifierId);

                entity.HasIndex(e => e.StatName);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CharacterModifierId)
                    .HasColumnName("charactermodifier");

                entity.Property(e => e.StatName)
                    .IsRequired()
                    .HasColumnName("stat")
                    .HasMaxLength(64);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(64)
                    .HasDefaultValueSql("'ADD'");

                entity.Property(e => e.Value)
                    .HasColumnName("value");

                entity.HasOne(e => e.CharacterModifier)
                    .WithMany(cm => cm.CharacterModifierValue)
                    .HasForeignKey(e => e.CharacterModifierId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<CharacterSkills>(entity =>
            {
                entity.ToTable("character_skills");

                entity.HasKey(e => new {e.CharacterId, e.SkillId});

                entity.HasIndex(e => e.CharacterId);

                entity.HasIndex(e => e.SkillId);

                entity.Property(e => e.CharacterId)
                    .IsRequired()
                    .HasColumnName("character");

                entity.Property(e => e.SkillId)
                    .IsRequired()
                    .HasColumnName("skill");

                entity.HasOne(e => e.Character)
                    .WithMany(c => c.CharacterSkills)
                    .HasForeignKey(e => e.CharacterId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(e => e.Skill)
                    .WithMany(c => c.CharacterSkills)
                    .HasForeignKey(e => e.SkillId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<CharacterSpeciality>(entity =>
            {
                entity.HasKey(e => new {e.CharacterId, Speciality = e.SpecialityId});

                entity.ToTable("character_speciality");

                entity.HasIndex(e => e.SpecialityId);

                entity.Property(e => e.CharacterId)
                    .HasColumnName("character");

                entity.Property(e => e.SpecialityId)
                    .HasColumnName("speciality");
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.ToTable("event");

                entity.HasKey(e => e.Id);

                entity.HasIndex(e => e.GroupId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name");

                entity.Property(e => e.Description)
                    .HasColumnName("description");

                entity.Property(e => e.Timestamp)
                    .HasColumnName("timestamp");

                entity.Property(e => e.GroupId)
                    .HasColumnName("groupId");

                entity.HasOne(d => d.Group)
                    .WithMany(g => g.Events)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Effect>(entity =>
            {
                entity.ToTable("effect");

                entity.HasIndex(e => e.CategoryId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category");

                entity.Property(e => e.DurationType)
                    .HasColumnName("durationtype");

                entity.Property(e => e.CombatCount)
                    .HasColumnName("combatcount");

                entity.Property(e => e.LapCount)
                    .HasColumnName("lapcount");

                entity.Property(e => e.Description)
                    .HasColumnName("description");

                entity.Property(e => e.Dice)
                    .HasColumnName("dice");

                entity.Property(e => e.Duration)
                    .HasColumnName("duration");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.TimeDuration)
                    .HasColumnName("timeduration");
            });

            modelBuilder.Entity<EffectCategory>(entity =>
            {
                entity.ToTable("effect_category");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Dicecount)
                    .HasColumnName("dicecount");

                entity.Property(e => e.Dicesize)
                    .HasColumnName("dicesize");

                entity.Property(e => e.TypeId)
                    .HasColumnName("typeid");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Categories)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<EffectModifier>(entity =>
            {
                entity.HasKey(e => new {Effect = e.EffectId, Stat = e.StatName});

                entity.ToTable("effect_modifier");

                entity.HasIndex(e => e.StatName);

                entity.Property(e => e.EffectId)
                    .HasColumnName("effect");

                entity.Property(e => e.StatName)
                    .HasColumnName("stat")
                    .HasMaxLength(64);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(16)
                    .HasDefaultValueSql("'ADD'");

                entity.Property(e => e.Value)
                    .HasColumnName("value");
            });

            modelBuilder.Entity<EffectType>(entity =>
            {
                entity.ToTable("effect_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ErrorReport>(entity =>
            {
                entity.ToTable("error_report");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.Date)
                    .HasColumnName("date");

                entity.Property(e => e.UserAgent)
                    .HasColumnName("useragent");
            });

            modelBuilder.Entity<God>(entity =>
            {
                entity.ToTable("god");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasColumnName("displayname")
                    .HasMaxLength(255);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.TechName)
                    .IsRequired()
                    .HasColumnName("techname")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("group");

                entity.HasIndex(e => e.CurrentCombatLootId)
                    .IsUnique();

                entity.HasIndex(e => e.LocationId);

                entity.HasIndex(e => e.MasterId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CurrentCombatLootId)
                    .HasColumnName("combatlootid");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.LocationId)
                    .HasColumnName("location")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.MasterId)
                    .HasColumnName("master");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.HasOne(d => d.CurrentCombatLoot)
                    .WithOne(p => p.CurrentCombatLootForGroup)
                    .HasForeignKey<Group>(g => g.CurrentCombatLootId)
                    .OnDelete(DeleteBehavior.SetNull);
            });

            modelBuilder.Entity<GroupHistory>(entity =>
            {
                entity.ToTable("group_history");

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasColumnName("action")
                    .HasMaxLength(64);

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Gm)
                    .HasColumnName("gm");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group");

                entity.Property(e => e.Info)
                    .HasColumnName("info");

                entity.Property(e => e.UserId)
                    .HasColumnName("user");
            });

            modelBuilder.Entity<GroupInvitations>(entity =>
            {
                entity.HasKey(e => new {Group = e.GroupId, Character = e.CharacterId});

                entity.ToTable("group_invitations");

                entity.HasIndex(e => e.CharacterId);

                entity.HasIndex(e => e.GroupId);

                entity.Property(e => e.GroupId)
                    .HasColumnName("group");

                entity.Property(e => e.CharacterId)
                    .HasColumnName("character");

                entity.Property(e => e.FromGroup)
                    .HasColumnName("fromgroup");
            });

            modelBuilder.Entity<Icon>(entity =>
            {
                entity.HasKey(e => e.Name);

                entity.ToTable("icon");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.ToTable("item");

                entity.HasIndex(e => e.CharacterId);

                entity.HasIndex(e => e.Container);

                entity.HasIndex(e => e.ItemTemplateId);

                entity.HasIndex(e => e.LootId);

                entity.HasIndex(e => e.MonsterId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CharacterId)
                    .HasColumnName("characterid");

                entity.Property(e => e.Container)
                    .HasColumnName("container");

                entity.Property(e => e.LifetimeType)
                    .HasColumnName("lifetimetype")
                    .HasComputedColumnSql("lifetimetype");

                entity.Property(e => e.DataJson)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemtemplateid");

                entity.Property(e => e.LootId)
                    .HasColumnName("lootid");

                entity.Property(e => e.ModifiersJson)
                    .HasColumnName("modifiers")
                    .HasColumnType("json");

                entity.Property(e => e.MonsterId)
                    .HasColumnName("monsterid");

                entity.HasOne(d => d.Loot)
                    .WithMany(p => p.Items)
                    .HasForeignKey(d => d.LootId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Monster)
                    .WithMany(p => p.Items)
                    .HasForeignKey(d => d.MonsterId)
                    .OnDelete(DeleteBehavior.SetNull);

                entity.HasOne(d => d.ItemTemplate)
                    .WithMany(p => p.Items)
                    .HasForeignKey(d => d.ItemTemplateId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<ItemSlot>(entity =>
            {
                entity.ToTable("item_slot");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Count)
                    .HasColumnName("count");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Stackable)
                    .HasColumnName("stackable");

                entity.Property(e => e.Techname)
                    .IsRequired()
                    .HasColumnName("techname")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ItemTemplate>(entity =>
            {
                entity.ToTable("item_template");

                entity.HasIndex(e => e.CategoryId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category");

                entity.Property(e => e.CleanName)
                    .HasColumnName("cleanname")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.DataJson)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.TechName)
                    .HasColumnName("techname")
                    .HasMaxLength(255);

                entity.Property(e => e.SourceUserNameCache)
                    .HasColumnName("sourceusernamecache")
                    .HasColumnType("text");
                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("text");

                entity.Property(e => e.SourceUserId)
                    .HasColumnName("sourceuserid");

                entity.HasOne(e => e.SourceUser)
                    .WithMany(u => u.ItemTemplates)
                    .HasForeignKey(e => e.SourceUserId)
                    .OnDelete(DeleteBehavior.SetNull);

                entity.HasMany(e => e.ItemTemplateModifiers)
                    .WithOne(s => s.ItemTemplate)
                    .HasForeignKey(s => s.ItemTemplateId);

                entity.HasMany(e => e.ItemTemplateSlot)
                    .WithOne(s => s.ItemTemplate)
                    .HasForeignKey(s => s.ItemTemplateId);

                entity.HasOne(e => e.TemplateCategory)
                    .WithMany(c => c.ItemTemplates)
                    .HasForeignKey(e => e.CategoryId);

                entity.HasMany(e => e.ItemTemplateRequirements)
                    .WithOne(r => r.ItemTemplate)
                    .HasForeignKey(r => r.ItemTemplateId);

                entity.HasMany(e => e.ItemTemplateSkills)
                    .WithOne(s => s.ItemTemplate)
                    .HasForeignKey(s => s.ItemTemplateId);

                entity.HasMany(e => e.ItemTemplateUnskills)
                    .WithOne(s => s.ItemTemplate)
                    .HasForeignKey(s => s.ItemTemplateId);
            });

            modelBuilder.Entity<ItemTemplateCategory>(entity =>
            {
                entity.ToTable("item_template_category");

                entity.HasIndex(e => e.SectionId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(4096);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note");

                entity.Property(e => e.TechName)
                    .IsRequired()
                    .HasColumnName("techname")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.SectionId)
                    .IsRequired()
                    .HasColumnName("section");

                entity.HasOne(e => e.TemplateSection)
                    .WithMany(s => s.ItemCategory)
                    .HasForeignKey(e => e.SectionId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<ItemTemplateModifier>(entity =>
            {
                entity.ToTable("item_template_modifier");

                entity.HasIndex(e => e.ItemTemplateId);

                entity.HasIndex(e => e.RequireJobId);

                entity.HasIndex(e => e.RequireOriginId);

                entity.HasIndex(e => e.StatName);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemTemplate");

                entity.Property(e => e.RequireJobId)
                    .HasColumnName("requirejob");

                entity.Property(e => e.RequireOriginId)
                    .HasColumnName("requireorigin");

                entity.Property(e => e.Special)
                    .HasColumnName("special")
                    .HasMaxLength(2048);

                entity.Property(e => e.StatName)
                    .IsRequired()
                    .HasColumnName("stat")
                    .HasMaxLength(64);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(64)
                    .HasDefaultValueSql("'ADD'");

                entity.Property(e => e.Value)
                    .HasColumnName("value");
            });

            modelBuilder.Entity<ItemTemplateRequirement>(entity =>
            {
                entity.ToTable("item_template_requirement");

                entity.HasIndex(e => e.ItemTemplateId);

                entity.HasIndex(e => e.StatName);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemTemplate");

                entity.Property(e => e.Maxvalue)
                    .HasColumnName("maxvalue");

                entity.Property(e => e.Minvalue)
                    .HasColumnName("minvalue");

                entity.Property(e => e.StatName)
                    .IsRequired()
                    .HasColumnName("stat")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<ItemTemplateSection>(entity =>
            {
                entity.ToTable("item_template_section");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note");

                entity.Property(e => e.Special)
                    .IsRequired()
                    .HasColumnName("special")
                    .HasMaxLength(2048);
            });

            modelBuilder.Entity<ItemTemplateSkill>(entity =>
            {
                entity.ToTable("item_template_skill");

                entity.HasKey(e => new {e.SkillId, e.ItemTemplateId});

                entity.HasIndex(e => e.ItemTemplateId);

                entity.HasIndex(e => e.SkillId);

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemTemplate");

                entity.Property(e => e.SkillId)
                    .HasColumnName("skill");

                entity.HasOne(e => e.Skill)
                    .WithMany(s => s.ItemSkills)
                    .HasForeignKey(e => e.SkillId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<ItemTemplateSkillModifiers>(entity =>
            {
                entity.ToTable("item_template_skill_modifiers");

                entity.HasIndex(e => e.ItemTemplateId);

                entity.HasIndex(e => e.SkillId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemTemplate");

                entity.Property(e => e.SkillId)
                    .HasColumnName("skill");

                entity.Property(e => e.Value)
                    .HasColumnName("value");
            });

            modelBuilder.Entity<ItemTemplateSlot>(entity =>
            {
                entity.HasKey(e => new {Slot = e.SlotId, Item = e.ItemTemplateId});

                entity.ToTable("item_template_slot");

                entity.HasIndex(e => e.ItemTemplateId);

                entity.Property(e => e.SlotId)
                    .HasColumnName("slot");

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemTemplate");
            });

            modelBuilder.Entity<ItemTemplateUnskill>(entity =>
            {
                entity.ToTable("item_template_unskill");

                entity.HasKey(e => new {e.SkillId, e.ItemTemplateId});

                entity.HasIndex(e => e.ItemTemplateId);

                entity.HasIndex(e => e.SkillId);

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemTemplate");

                entity.Property(e => e.SkillId)
                    .HasColumnName("skill");

                entity.HasOne(e => e.Skill)
                    .WithMany(s => s.ItemUnskills)
                    .HasForeignKey(e => e.SkillId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<ItemType>(entity =>
            {
                entity.ToTable("item_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasColumnName("displayName")
                    .HasMaxLength(255);

                entity.Property(e => e.TechName)
                    .IsRequired()
                    .HasColumnName("techName")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("job");

                entity.HasIndex(e => e.ParentJobId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.PlayerDescription)
                    .HasColumnName("playerDescription");

                entity.Property(e => e.PlayerSummary)
                    .HasColumnName("playerSummary");

                entity.Property(e => e.BaseAt)
                    .HasColumnName("baseat");

                entity.Property(e => e.BaseEa)
                    .HasColumnName("baseea");

                entity.Property(e => e.BaseEv)
                    .HasColumnName("baseev");

                entity.Property(e => e.BasePrd)
                    .HasColumnName("baseprd");

                entity.Property(e => e.BonusEv)
                    .HasColumnName("bonusev");

                entity.Property(e => e.DiceEaLevelUp)
                    .HasColumnName("diceealevelup");

                entity.Property(e => e.FactorEv)
                    .HasColumnName("factorev");

                entity.Property(e => e.Informations)
                    .HasColumnName("informations");

                entity.Property(e => e.Internalname)
                    .HasColumnName("internalname")
                    .HasMaxLength(255);

                entity.Property(e => e.IsMagic)
                    .HasColumnName("ismagic")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.MaxArmorPr)
                    .HasColumnName("maxarmorpr");

                entity.Property(e => e.MaxLoad)
                    .HasColumnName("maxload");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.ParentJobId)
                    .HasColumnName("parentjob");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");
            });

            modelBuilder.Entity<JobBonus>(entity =>
            {
                entity.ToTable("job_bonus");

                entity.HasIndex(e => e.JobId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.JobId)
                    .HasColumnName("jobid");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");
            });

            modelBuilder.Entity<JobOriginBlacklist>(entity =>
            {
                entity.ToTable("job_origin_blacklist");

                entity.HasIndex(e => e.JobId);

                entity.HasIndex(e => e.OriginId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.JobId)
                    .HasColumnName("job");

                entity.Property(e => e.OriginId)
                    .HasColumnName("origin");
            });

            modelBuilder.Entity<JobOriginWhitelist>(entity =>
            {
                entity.ToTable("job_origin_whitelist");

                entity.HasIndex(e => e.JobId);

                entity.HasIndex(e => e.OriginId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.JobId)
                    .HasColumnName("job");

                entity.Property(e => e.OriginId)
                    .HasColumnName("origin");
            });

            modelBuilder.Entity<JobRequirement>(entity =>
            {
                entity.ToTable("job_requirement");

                entity.HasIndex(e => e.JobId);

                entity.HasIndex(e => e.StatName);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.JobId)
                    .HasColumnName("jobid");

                entity.Property(e => e.MaxValue)
                    .HasColumnName("maxvalue");

                entity.Property(e => e.MinValue)
                    .HasColumnName("minvalue");

                entity.Property(e => e.StatName)
                    .IsRequired()
                    .HasColumnName("stat")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<JobRestrict>(entity =>
            {
                entity.ToTable("job_restrict");

                entity.HasIndex(e => e.JobId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.JobId)
                    .HasColumnName("jobid");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasColumnName("text");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");
            });

            modelBuilder.Entity<JobSkill>(entity =>
            {
                entity.HasKey(e => new {e.Jobid, e.SkillId});

                entity.ToTable("job_skill");

                entity.HasIndex(e => e.SkillId);

                entity.Property(e => e.Jobid)
                    .HasColumnName("jobid");

                entity.Property(e => e.SkillId)
                    .HasColumnName("skillid");

                entity.Property(e => e.Default)
                    .HasColumnName("default");
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("location");

                entity.HasIndex(e => e.ParentId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.ParentId)
                    .HasColumnName("parent");
            });

            modelBuilder.Entity<LocationMap>(entity =>
            {
                entity.ToTable("location_map");

                entity.HasIndex(e => e.LocationId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.File)
                    .IsRequired()
                    .HasColumnName("file")
                    .HasMaxLength(255);

                entity.Property(e => e.Gm)
                    .HasColumnName("gm");

                entity.Property(e => e.LocationId)
                    .HasColumnName("location");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Loot>(entity =>
            {
                entity.ToTable("loot");

                entity.HasIndex(e => e.GroupId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Created)
                    .HasColumnName("dead");

                entity.Property(e => e.GroupId)
                    .HasColumnName("groupid");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.VisibleForPlayer)
                    .HasColumnName("visibleForPlayer");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Loots)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Monster>(entity =>
            {
                entity.ToTable("monster");

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.LootId);

                entity.HasIndex(e => e.TargetCharacterId);

                entity.HasIndex(e => e.TargetMonsterId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.Dead)
                    .HasColumnName("dead")
                    .HasColumnType("timestamp");

                entity.Property(e => e.ModifiersJson)
                    .HasColumnName("modifiers")
                    .HasColumnType("json");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group");

                entity.Property(e => e.LootId)
                    .HasColumnName("lootid");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.TargetCharacterId)
                    .HasColumnName("targetcharacterid");

                entity.Property(e => e.TargetMonsterId)
                    .HasColumnName("targetmonsterid");

                entity.HasOne(d => d.Loot)
                    .WithMany(p => p.Monsters)
                    .HasForeignKey(d => d.LootId)
                    .OnDelete(DeleteBehavior.SetNull);

                entity.HasOne(e => e.TargetCharacter)
                    .WithMany(e => e.InverseTargetMonsters)
                    .HasForeignKey(e => e.TargetCharacterId)
                    .OnDelete(DeleteBehavior.SetNull);

                entity.HasOne(e => e.TargetMonster)
                    .WithMany(e => e.InverseTargetMonsters)
                    .HasForeignKey(e => e.TargetMonsterId)
                    .OnDelete(DeleteBehavior.SetNull);
            });

            modelBuilder.Entity<MonsterCategory>(entity =>
            {
                entity.ToTable("monster_category");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.TypeId)
                    .HasColumnName("typeid");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Categories)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<MonsterLocation>(entity =>
            {
                entity.HasKey(e => new {e.MonsterTemplateId, e.LocationId});

                entity.ToTable("monster_location");

                entity.Property(e => e.MonsterTemplateId)
                    .HasColumnName("monsterid");

                entity.Property(e => e.LocationId)
                    .HasColumnName("locationid");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Monsters)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.MonsterTemplate)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.MonsterTemplateId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<MonsterTemplate>(entity =>
            {
                entity.ToTable("monster_template");

                entity.HasIndex(e => e.CategoryId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnName("data")
                    .HasColumnType("json");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.CategoryId)
                    .HasColumnName("categoryid");
            });

            modelBuilder.Entity<MonsterTemplateSimpleInventory>(entity =>
            {
                entity.ToTable("monster_template_simple_inventory");

                entity.HasIndex(e => e.ItemTemplateId);

                entity.HasIndex(e => e.MonsterTemplateId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Chance)
                    .HasColumnName("chance");

                entity.Property(e => e.ItemTemplateId)
                    .HasColumnName("itemtemplateid");

                entity.Property(e => e.MaxCount)
                    .HasColumnName("maxCount");

                entity.Property(e => e.MinCount)
                    .HasColumnName("minCount");

                entity.Property(e => e.MonsterTemplateId)
                    .HasColumnName("monstertemplateid");

                entity.HasOne(d => d.MonsterTemplate)
                    .WithMany(p => p.SimpleInventory)
                    .HasForeignKey(d => d.MonsterTemplateId);
            });

            modelBuilder.Entity<MonsterTrait>(entity =>
            {
                entity.ToTable("monster_trait");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.LevelsJson)
                    .HasColumnName("levels")
                    .HasColumnType("json");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<MonsterType>(entity =>
            {
                entity.ToTable("monster_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Origin>(entity =>
            {
                entity.ToTable("origin");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Advantage)
                    .HasColumnName("advantage");

                entity.Property(e => e.BaseEa)
                    .HasColumnName("baseea");

                entity.Property(e => e.BaseEv)
                    .HasColumnName("baseev");

                entity.Property(e => e.BonusAt)
                    .HasColumnName("bonusat");

                entity.Property(e => e.BonusPrd)
                    .HasColumnName("bonusprd");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.PlayerDescription)
                    .HasColumnName("playerDescription");

                entity.Property(e => e.PlayerSummary)
                    .HasColumnName("playerSummary");

                entity.Property(e => e.DiceEvLevelUp)
                    .HasColumnName("diceevlevelup");

                entity.Property(e => e.MaxArmorPr)
                    .HasColumnName("maxarmorpr");

                entity.Property(e => e.MaxLoad)
                    .HasColumnName("maxload");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Size)
                    .HasColumnName("size");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");

                entity.Property(e => e.SpeedModifier)
                    .HasColumnName("speedmodifier");
            });

            modelBuilder.Entity<OriginBonus>(entity =>
            {
                entity.ToTable("origin_bonus");

                entity.HasIndex(e => e.OriginId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.OriginId)
                    .HasColumnName("originid");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");
            });

            modelBuilder.Entity<OriginInfo>(entity =>
            {
                entity.ToTable("origin_info");

                entity.HasIndex(e => e.OriginId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.OriginId)
                    .HasColumnName("originid");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<OriginRequirement>(entity =>
            {
                entity.ToTable("origin_requirement");

                entity.HasIndex(e => e.Originid);

                entity.HasIndex(e => e.StatName);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.MaxValue)
                    .HasColumnName("maxvalue");

                entity.Property(e => e.MinValue)
                    .HasColumnName("minvalue");

                entity.Property(e => e.Originid)
                    .HasColumnName("originid");

                entity.Property(e => e.StatName)
                    .IsRequired()
                    .HasColumnName("stat")
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<OriginRestrict>(entity =>
            {
                entity.ToTable("origin_restrict");

                entity.HasIndex(e => e.Originid);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Originid)
                    .HasColumnName("originid");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasColumnName("text");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");
            });

            modelBuilder.Entity<OriginSkill>(entity =>
            {
                entity.HasKey(e => new {e.Originid, e.Skillid});

                entity.ToTable("origin_skill");

                entity.HasIndex(e => e.Skillid);

                entity.Property(e => e.Originid)
                    .HasColumnName("originid");

                entity.Property(e => e.Skillid)
                    .HasColumnName("skillid");

                entity.Property(e => e.Default)
                    .HasColumnName("default");
            });

            modelBuilder.Entity<Quest>(entity =>
            {
                entity.ToTable("quest");

                entity.HasIndex(e => e.GroupId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnName("data");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<QuestTemplate>(entity =>
            {
                entity.ToTable("quest_template");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Data)
                    .IsRequired()
                    .HasColumnName("data");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Skill>(entity =>
            {
                entity.ToTable("skill");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnName("description");

                entity.Property(e => e.PlayerDescription)
                    .HasColumnName("playerDescription");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Require)
                    .HasColumnName("require");

                entity.Property(e => e.Resist)
                    .HasColumnName("resist");

                entity.Property(e => e.Roleplay)
                    .HasColumnName("roleplay");

                entity.Property(e => e.Stat)
                    .HasColumnName("stat")
                    .HasMaxLength(2048);

                entity.Property(e => e.Test)
                    .HasColumnName("test");

                entity.Property(e => e.Using)
                    .HasColumnName("using");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");

                entity.HasMany(e => e.SkillEffects)
                    .WithOne(s => s.Skill)
                    .HasForeignKey(e => e.SkillId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<SkillEffect>(entity =>
            {
                entity.ToTable("skill_effect");

                entity.HasKey(e => new {e.SkillId, e.StatName});

                entity.HasIndex(e => e.SkillId);

                entity.HasIndex(e => e.StatName);

                entity.Property(e => e.SkillId)
                    .IsRequired()
                    .HasColumnName("skill");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value");

                entity.Property(e => e.StatName)
                    .IsRequired()
                    .HasColumnName("stat")
                    .HasMaxLength(255);

                entity.HasOne(e => e.Skill)
                    .WithMany(s => s.SkillEffects)
                    .HasForeignKey(e => e.SkillId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Speciality>(entity =>
            {
                entity.ToTable("speciality");

                entity.HasIndex(e => e.JobId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.JobId)
                    .HasColumnName("job");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");

                entity.HasOne(e => e.Job)
                    .WithMany(j => j.Speciality)
                    .HasForeignKey(e => e.JobId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<SpecialityModifier>(entity =>
            {
                entity.ToTable("speciality_modifier");

                entity.HasIndex(e => e.SpecialityId);

                entity.HasIndex(e => e.Stat);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.SpecialityId)
                    .HasColumnName("speciality");

                entity.Property(e => e.Stat)
                    .IsRequired()
                    .HasColumnName("stat")
                    .HasMaxLength(64);

                entity.Property(e => e.Value)
                    .HasColumnName("value");

                entity.HasOne(e => e.Speciality)
                    .WithMany(s => s.SpecialityModifier)
                    .HasForeignKey(e => e.SpecialityId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<SpecialitySpecial>(entity =>
            {
                entity.ToTable("speciality_special");

                entity.HasIndex(e => e.SpecialityId);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Isbonus)
                    .HasColumnName("isbonus");

                entity.Property(e => e.SpecialityId)
                    .HasColumnName("speciality");

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasColumnType("json");

                entity.HasOne(e => e.Speciality)
                    .WithMany(s => s.SpecialitySpecial)
                    .HasForeignKey(e => e.SpecialityId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Spell>(entity =>
            {
                entity.ToTable("spell");

                entity.HasIndex(e => e.CategoryId);

                entity.HasIndex(e => e.TestStatName);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Distance)
                    .HasColumnName("distance");

                entity.Property(e => e.Distancenote)
                    .HasColumnName("distancenote");

                entity.Property(e => e.Durationeffect)
                    .HasColumnName("durationeffect");

                entity.Property(e => e.Durationeffectunit)
                    .IsRequired()
                    .HasColumnName("durationeffectunit")
                    .HasMaxLength(2048);

                entity.Property(e => e.Durationprepare)
                    .HasColumnName("durationprepare");

                entity.Property(e => e.Durationprepareunit)
                    .IsRequired()
                    .HasColumnName("durationprepareunit")
                    .HasMaxLength(2048);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Speed)
                    .HasColumnName("speed");

                entity.Property(e => e.Test)
                    .HasColumnName("test");

                entity.Property(e => e.TestStatName)
                    .IsRequired()
                    .HasColumnName("teststat")
                    .HasMaxLength(64);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(2048);

                entity.Property(e => e.Variablecost)
                    .HasColumnName("variablecost");
            });

            modelBuilder.Entity<SpellCategory>(entity =>
            {
                entity.ToTable("spell_category");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Default)
                    .HasColumnName("default");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Stat>(entity =>
            {
                entity.HasKey(e => e.Name);

                entity.ToTable("stat");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(64);

                entity.Property(e => e.Bonus)
                    .HasColumnName("bonus");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasColumnName("displayname")
                    .HasMaxLength(64);

                entity.Property(e => e.Penality)
                    .HasColumnName("penality");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasIndex(e => e.Username)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Admin)
                    .HasColumnName("admin")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.DisplayName)
                    .HasColumnName("displayname")
                    .HasMaxLength(255);

                entity.Property(e => e.FbId)
                    .HasColumnName("fbid")
                    .HasMaxLength(255);

                entity.Property(e => e.GoogleId)
                    .HasColumnName("googleid")
                    .HasMaxLength(255);

                entity.Property(e => e.LiveId)
                    .HasColumnName("liveid")
                    .HasMaxLength(255);

                entity.Property(e => e.TwitterId)
                    .HasColumnName("twitterid")
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<UserSession>(entity =>
            {
                entity.ToTable("user_session");

                entity.HasIndex(e => e.Key)
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Expire)
                    .HasColumnName("expire");

                entity.Property(e => e.Ip)
                    .HasColumnName("ip")
                    .HasMaxLength(255);

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasColumnName("key")
                    .HasMaxLength(1024);

                entity.Property(e => e.Start)
                    .HasColumnName("start");

                entity.Property(e => e.UserId)
                    .HasColumnName("userid");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserSessions)
                    .HasForeignKey(d => d.UserId);
            });
        }
    }
}