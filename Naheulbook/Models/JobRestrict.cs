﻿using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class JobRestrict
    {
        public long Id { get; set; }

        public long JobId { get; set; }

        [JsonProperty("description")]
        public string Text { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }

        public Job Job { get; set; }
    }
}