using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class MonsterCategory
    {
        public MonsterCategory()
        {
            MonsterTemplate = new HashSet<MonsterTemplate>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public long TypeId { get; set; }

        public MonsterType Type { get; set; }

        public ICollection<MonsterTemplate> MonsterTemplate { get; set; }
    }
}