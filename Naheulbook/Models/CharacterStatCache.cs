﻿namespace Naheulbook.Model
{
    public partial class CharacterStatCache
    {
        public string Stat { get; set; }
        public long Value { get; set; }
        public long Character { get; set; }
    }
}
