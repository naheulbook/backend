﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ItemTemplate
    {
        public ItemTemplate()
        {
            ItemTemplateModifiers = new HashSet<ItemTemplateModifier>();
            ItemTemplateRequirements = new HashSet<ItemTemplateRequirement>();
            ItemTemplateSkillModifiers = new HashSet<ItemTemplateSkillModifiers>();
            ItemTemplateSlot = new HashSet<ItemTemplateSlot>();
            ItemTemplateSkills = new HashSet<ItemTemplateSkill>();
            ItemTemplateUnskills = new HashSet<ItemTemplateUnskill>();
        }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        public string CleanName { get; set; }

        [JsonProperty("techname")]
        public string TechName { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }
        
        [JsonProperty("sourceUserId")]
        public long? SourceUserId { get; set; }

        [JsonProperty("sourceUser")]
        public string SourceUserNameCache { get; set; }
        
        [JsonProperty("category")]
        public long CategoryId { get; set; }

        [JsonProperty("data")]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string DataJson
        {
            get { return Data.GetJson(); }
            set { Data.SetJson(value); }
        }

        [NotMapped]
        public JsonDataField Data { get; } = new JsonDataField();

        [JsonProperty("slots")]
        [NotMapped]
        public IEnumerable<ItemSlot> Slots => ItemTemplateSlot?.Select(s => s.Slot);

        [JsonProperty("modifiers")]
        public ICollection<ItemTemplateModifier> ItemTemplateModifiers { get; set; }

        [JsonProperty("requirements")]
        public ICollection<ItemTemplateRequirement> ItemTemplateRequirements { get; set; }

        [JsonProperty("skillModifiers")]
        public ICollection<ItemTemplateSkillModifiers> ItemTemplateSkillModifiers { get; set; }

        public ICollection<ItemTemplateSlot> ItemTemplateSlot { get; set; }

        public ICollection<ItemTemplateSkill> ItemTemplateSkills { get; set; }

        [JsonProperty("skills")]
        [NotMapped]
        public IEnumerable<object> Skills => ItemTemplateSkills?.Select(s => new {id = s.SkillId});

        public ICollection<ItemTemplateUnskill> ItemTemplateUnskills { get; set; }

        [JsonProperty("unskills")]
        [NotMapped]
        public IEnumerable<object> Unskills => ItemTemplateUnskills?.Select(s => new {id = s.SkillId});

        public ItemTemplateCategory TemplateCategory { get; set; }

        public ICollection<Item> Items { get; set; }
        
        public User SourceUser { get; set; }
    }
}