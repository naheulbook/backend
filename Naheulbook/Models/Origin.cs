﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using Naheulbook.Tool;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Origin
    {
        public Origin()
        {
            ItemEffect = new HashSet<ItemTemplateModifier>();
            OriginBonus = new HashSet<OriginBonus>();
            OriginInfo = new HashSet<OriginInfo>();
            OriginRequirement = new HashSet<OriginRequirement>();
            OriginRestrict = new HashSet<OriginRestrict>();
            OriginSkill = new HashSet<OriginSkill>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        [JsonProperty("playerDescription")]
        public string PlayerDescription { get; set; }

        [JsonProperty("playerSummary")]
        public string PlayerSummary { get; set; }

        [JsonProperty("maxLoad")]
        public long? MaxLoad { get; set; }

        [JsonProperty("maxArmorPR")]
        public short? MaxArmorPr { get; set; }

        [JsonProperty]
        public string Advantage { get; set; }

        [JsonProperty("baseEV")]
        public short? BaseEv { get; set; }

        [JsonProperty("baseEA")]
        public short? BaseEa { get; set; }

        [JsonProperty("bonusAT")]
        public short? BonusAt { get; set; }

        [JsonProperty("bonusPRD")]
        public short? BonusPrd { get; set; }

        [JsonProperty("diceEVLevelUp")]
        public int DiceEvLevelUp { get; set; }

        [JsonProperty]
        public string Size { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }
        
        [JsonProperty("speedModifier")]
        public short? SpeedModifier { get; set; }

        [JsonProperty("skills")]
        public IEnumerable<dynamic> Skills => OriginSkill.Where(s => s.Default).Select(os =>
        {
            dynamic d = new ExpandoObject();
            d.id = os.Skillid;
            return d;
        });

        [JsonProperty("availableSkills")]
        public IEnumerable<dynamic> AvailableSkills => OriginSkill.Where(s => !s.Default).Select(os =>
        {
            dynamic d = new ExpandoObject();
            d.id = os.Skillid;
            return d;
        });

        [JsonProperty("infos")]
        public IEnumerable<dynamic> Infos => OriginInfo.Select(info =>
        {
            dynamic d = new ExpandoObject();
            d.title = info.Title;
            d.description = info.Description;
            return d;
        });

        public ICollection<ItemTemplateModifier> ItemEffect { get; set; }

        [JsonProperty("bonuses")]
        public ICollection<OriginBonus> OriginBonus { get; set; }

        public ICollection<OriginInfo> OriginInfo { get; set; }

        [JsonProperty("requirements")]
        public ICollection<OriginRequirement> OriginRequirement { get; set; }

        [JsonProperty("restricts")]
        public ICollection<OriginRestrict> OriginRestrict { get; set; }

        public ICollection<OriginSkill> OriginSkill { get; set; }

        public ICollection<Character> Characters { get; set; }

        public bool HasFlag(string flagName)
        {
            if (Flags != null)
            {
                var flags = JsonConvert.DeserializeObject<JArray>(Flags);
                foreach (JToken jFlag in flags)
                {
                    var flag = jFlag.ToObject<JObject>();
                    if (flag.Value<string>("type") == flagName)
                    {
                        return true;
                    }
                }
            }
            if (OriginBonus != null)
            {
                foreach (var originBonuse in OriginBonus)
                {
                    if (originBonuse.Flags == null)
                    {
                        continue;
                    }
                    var flags = JsonConvert.DeserializeObject<JArray>(originBonuse.Flags);
                    foreach (JToken jFlag in flags)
                    {
                        var flag = jFlag.ToObject<JObject>();
                        if (flag.Value<string>("type") == flagName)
                        {
                            return true;
                        }
                    }
                }
            }
            if (OriginRestrict != null)
            {
                foreach (var originRestrict in OriginRestrict)
                {
                    if (originRestrict.Flags == null)
                    {
                        continue;
                    }
                    var flags = JsonConvert.DeserializeObject<JArray>(originRestrict.Flags);
                    foreach (JToken jFlag in flags)
                    {
                        var flag = jFlag.ToObject<JObject>();
                        if (flag.Value<string>("type") == flagName)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}