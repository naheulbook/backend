﻿namespace Naheulbook.Models
{
    public partial class OriginSkill
    {
        public long Originid { get; set; }
        public long Skillid { get; set; }
        public bool Default { get; set; }

        public virtual Origin Origin { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
