﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class JobRequirement
    {
        public long Id { get; set; }

        public long JobId { get; set; }

        [JsonProperty("stat")]
        public string StatName { get; set; }

        [JsonProperty("min")]
        public long? MinValue { get; set; }

        [JsonProperty("max")]
        public long? MaxValue { get; set; }

        public virtual Job Job { get; set; }

        public virtual Stat Stat { get; set; }
    }
}