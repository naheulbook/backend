﻿using System ;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ErrorReport
    {
        [JsonProperty]
        public long Id { get; set; }

        public string Data { get; set; }

        [JsonProperty]
        public DateTime Date { get; set; }

        public string UserAgent { get; set; }
    }
}
