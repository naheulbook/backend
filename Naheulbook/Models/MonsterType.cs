﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class MonsterType
    {
        public MonsterType()
        {
            Categories = new HashSet<MonsterCategory>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public ICollection<MonsterCategory> Categories { get; set; }
    }
}