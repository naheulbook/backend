﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class EffectModifier
    {
        public long EffectId { get; set; }

        [JsonProperty("stat")]
        public string StatName { get; set; }

        [JsonProperty]
        public short Value { get; set; }

        [JsonProperty]
        public string Type { get; set; }

        public virtual Effect Effect { get; set; }

        public virtual Stat Stat { get; set; }
    }
}