﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Naheulbook.Tool;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class CharacterModifier
    {
        public CharacterModifier()
        {
            CharacterModifierValue = new HashSet<CharacterModifierValue>();
            Active = true;
        }

        [JsonProperty("id")]
        public long Id { get; set; }

        public long? CharacterId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("permanent")]
        public bool Permanent { get; set; }

        [JsonProperty("durationType", NullValueHandling = NullValueHandling.Ignore)]
        public string DurationType { get; set; }

        [JsonProperty("duration", NullValueHandling = NullValueHandling.Ignore)]
        public string Duration { get; set; }

        [JsonProperty("timeDuration", NullValueHandling = NullValueHandling.Ignore)]
        public int? TimeDuration { get; set; }

        [JsonProperty("currentTimeDuration", NullValueHandling = NullValueHandling.Ignore)]
        public int? CurrentTimeDuration { get; set; }

        [JsonProperty("lapCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? LapCount { get; set; }

        [JsonProperty("currentLapCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? CurrentLapCount { get; set; }

        [JsonProperty("combatCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? CombatCount { get; set; }

        [JsonProperty("currentCombatCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? CurrentCombatCount { get; set; }

        [JsonProperty("reusable")]
        public bool Reusable { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }
        
        [JsonProperty("lapCountDecrement", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string LapCountDecrementJson { get; set; }

        [NotMapped]
        public JObject LapCountDecrement {
            get => JObject.Parse(LapCountDecrementJson);
            set => LapCountDecrementJson = value?.ToString(Formatting.None);
        }

        [JsonProperty("values")]
        public ICollection<CharacterModifierValue> CharacterModifierValue { get; set; }

        public virtual Character Character { get; set; }
    }
}