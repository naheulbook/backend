﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using Naheulbook.Tool;
using Newtonsoft.Json;
using System.Linq;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Group
    {
        public Group()
        {
            Character = new HashSet<Character>();
            GroupHistory = new HashSet<GroupHistory>();
            GroupInvitations = new HashSet<GroupInvitations>();
            Monster = new HashSet<Monster>();
            Quest = new HashSet<Quest>();
            Loots = new HashSet<Loot>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty("master")]
        public long? MasterId { get; set; }

        [JsonProperty, JsonConverter(typeof(PlainJsonStringConverter))]
        public string Data { get; set; }

        public long LocationId { get; set; }

        [JsonProperty]
        public long? CurrentCombatLootId { get; set; }

        public ICollection<Character> Character { get; set; }

        [NotMapped]
        [JsonProperty("characters")]
        public ICollection<object> Characters => Character.Select(c => {
            dynamic d = new ExpandoObject();
            d.id = c.Id;
            return d;
        }).ToList();

        public ICollection<GroupHistory> GroupHistory { get; set; }

        public ICollection<GroupInvitations> GroupInvitations { get; set; }

        [NotMapped]
        [JsonProperty("invites")]
        public IEnumerable<dynamic> Invites => GroupInvitations.Where(s => !s.FromGroup).Select(invite =>
        {
            dynamic d = new ExpandoObject();
            d.id = invite.CharacterId;
            return d;
        });

        [NotMapped]
        [JsonProperty("invited")]
        public IEnumerable<dynamic> Invited => GroupInvitations.Where(s => s.FromGroup).Select(invite =>
        {
            dynamic d = new ExpandoObject();
            d.id = invite.CharacterId;
            return d;
        });


        [JsonProperty("monsters")]
        public ICollection<Monster> Monster { get; set; }

        public ICollection<Quest> Quest { get; set; }

        public ICollection<Loot> Loots { get; set; }

        public ICollection<Event> Events { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }

        public Loot CurrentCombatLoot { get; set; }

        public User Master { get; set; }
    }
}