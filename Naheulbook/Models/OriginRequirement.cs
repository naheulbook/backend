﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class OriginRequirement
    {
        public long Id { get; set; }

        public long Originid { get; set; }

        [JsonProperty("stat")]
        public string StatName { get; set; }

        [JsonProperty("min")]
        public long? MinValue { get; set; }

        [JsonProperty("max")]
        public long? MaxValue { get; set; }

        public virtual Origin Origin { get; set; }

        public virtual Stat Stat { get; set; }
    }
}