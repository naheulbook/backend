﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class CharacterSkills
    {
        public long CharacterId { get; set; }

        [JsonProperty("id")]
        public long SkillId { get; set; }

        public virtual Character Character { get; set; }

        public virtual Skill Skill { get; set; }
    }
}