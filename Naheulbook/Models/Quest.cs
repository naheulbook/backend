﻿namespace Naheulbook.Models
{
    public partial class Quest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long GroupId { get; set; }

        public string Data { get; set; }

        public virtual Group Group { get; set; }
    }
}