﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Skill
    {
        public Skill()
        {
            SkillEffects = new HashSet<SkillEffect>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        [JsonProperty]
        public string PlayerDescription { get; set; }

        [JsonProperty]
        public string Require { get; set; }

        [JsonProperty]
        public string Resist { get; set; }

        [JsonProperty]
        public string Using { get; set; }

        [JsonProperty]
        public string Roleplay { get; set; }

        public string Stat { get; set; }

        [NotMapped]
        [JsonProperty("stat")]
        public string[] StatArray => Stat == null ? new string[0] : Stat.Split(',');

        [JsonProperty]
        public short? Test { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }
        
        [JsonProperty("effects")]
        public ICollection<SkillEffect> SkillEffects { get; set; }

        public ICollection<CharacterSkills> CharacterSkills { get; set; }

        public ICollection<ItemTemplateSkill> ItemSkills { get; set; }

        public ICollection<ItemTemplateUnskill> ItemUnskills { get; set; }
    }
}