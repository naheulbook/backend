﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Stat
    {
        public Stat()
        {
            EffectModifier = new HashSet<EffectModifier>();
            ItemEffect = new HashSet<ItemTemplateModifier>();
            ItemRequirement = new HashSet<ItemTemplateRequirement>();
            JobRequirement = new HashSet<JobRequirement>();
            OriginRequirement = new HashSet<OriginRequirement>();
            Spell = new HashSet<Spell>();
        }

        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public string DisplayName { get; set; }
        [JsonProperty]
        public string Description { get; set; }
        public string Bonus { get; set; }
        public string Penality { get; set; }

        public virtual ICollection<EffectModifier> EffectModifier { get; set; }
        public virtual ICollection<ItemTemplateModifier> ItemEffect { get; set; }
        public virtual ICollection<ItemTemplateRequirement> ItemRequirement { get; set; }
        public virtual ICollection<JobRequirement> JobRequirement { get; set; }
        public virtual ICollection<OriginRequirement> OriginRequirement { get; set; }
        public virtual ICollection<Spell> Spell { get; set; }
    }
}
