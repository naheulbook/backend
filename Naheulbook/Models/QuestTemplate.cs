﻿namespace Naheulbook.Models
{
    public partial class QuestTemplate
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
    }
}
