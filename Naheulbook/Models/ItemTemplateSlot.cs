﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ItemTemplateSlot
    {
        public long ItemTemplateId { get; set; }

        public long SlotId { get; set; }

        public virtual ItemTemplate ItemTemplate { get; set; }

        public virtual ItemSlot Slot { get; set; }
    }
}