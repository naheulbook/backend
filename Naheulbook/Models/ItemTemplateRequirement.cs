﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ItemTemplateRequirement
    {
        public long Id { get; set; }

        public long ItemTemplateId { get; set; }

        [JsonProperty("stat")]
        public string StatName { get; set; }

        [JsonProperty("min")]
        public long? Minvalue { get; set; }

        [JsonProperty("max")]
        public long? Maxvalue { get; set; }

        public virtual ItemTemplate ItemTemplate { get; set; }

        public virtual Stat Stat { get; set; }
    }
}