﻿namespace Naheulbook.Models
{
    public class ItemTemplateSkill
    {
        public long ItemTemplateId { get; set; }

        public long SkillId { get; set; }

        public virtual ItemTemplate ItemTemplate { get; set; }

        public virtual Skill Skill { get; set; }
    }
}