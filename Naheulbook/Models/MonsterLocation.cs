﻿namespace Naheulbook.Models
{
    public class MonsterLocation
    {
        public long MonsterTemplateId { get; set; }

        public MonsterTemplate MonsterTemplate { get; set; }

        public long LocationId { get; set; }

        public Location Location { get; set; }
    }
}