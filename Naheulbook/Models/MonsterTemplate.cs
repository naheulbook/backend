﻿using System.Collections.Generic;
using System.Linq;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class MonsterTemplate
    {
        public MonsterTemplate()
        {
            Locations = new HashSet<MonsterLocation>();
            SimpleInventory = new HashSet<MonsterTemplateSimpleInventory>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }

        [JsonConverter(typeof(PlainJsonStringConverter))]
        [JsonProperty]
        public string Data { get; set; }

        public virtual MonsterCategory Category { get; set; }

        [JsonProperty("locations")]
        public ICollection<long> LocationsId => Locations.Select(l => l.LocationId).ToList();

        public ICollection<MonsterLocation> Locations { get; set; }

        [JsonProperty("simpleInventory")]
        public ICollection<MonsterTemplateSimpleInventory> SimpleInventory { get; set; }
    }
}