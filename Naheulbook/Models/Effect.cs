﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Effect
    {
        public Effect()
        {
            EffectModifier = new HashSet<EffectModifier>();
        }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("durationType")]
        public string DurationType { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("combatCount")]
        public int? CombatCount { get; set; }

        [JsonProperty("lapCount")]
        public int? LapCount { get; set; }

        [JsonProperty("timeDuration")]
        public int? TimeDuration { get; set; }

        [JsonProperty("dice")]
        public short? Dice { get; set; }

        [JsonProperty("categoryId")]
        public long CategoryId { get; set; }

        [JsonProperty("modifiers")]
        public virtual ICollection<EffectModifier> EffectModifier { get; set; }

        public virtual ICollection<CharacterHistory> CharacterHistory { get; set; }

        public virtual EffectCategory Category { get; set; }
    }
}