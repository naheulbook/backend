﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class GroupInvitations
    {
        [JsonProperty]
        public long GroupId { get; set; }

        public long CharacterId { get; set; }

        public bool FromGroup { get; set; }

        public virtual Character Character { get; set; }

        public virtual Group Group { get; set; }
    }
}