﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class EffectCategory
    {
        public EffectCategory()
        {
            Effect = new HashSet<Effect>();
        }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("diceCount")]
        public short Dicecount { get; set; }

        [JsonProperty("diceSize")]
        public short Dicesize { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty]
        public long TypeId { get; set; }

        public EffectType Type { get; set; }

        public virtual ICollection<Effect> Effect { get; set; }
    }
}