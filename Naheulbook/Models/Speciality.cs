﻿using System.Collections.Generic;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Speciality
    {
        public Speciality()
        {
            SpecialityModifier = new HashSet<SpecialityModifier>();
            SpecialitySpecial = new HashSet<SpecialitySpecial>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        public long JobId { get; set; }

        [JsonProperty("modifiers")]
        public virtual ICollection<SpecialityModifier> SpecialityModifier { get; set; }

        [JsonProperty("specials")]
        public virtual ICollection<SpecialitySpecial> SpecialitySpecial { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }

        public virtual Job Job { get; set; }
    }
}