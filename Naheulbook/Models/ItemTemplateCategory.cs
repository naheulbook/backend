﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ItemTemplateCategory
    {
        public ItemTemplateCategory()
        {
            ItemTemplates = new HashSet<ItemTemplate>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public string TechName { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        [JsonProperty]
        public string Note { get; set; }

        [JsonProperty]
        public long SectionId { get; set; }

        public ICollection<ItemTemplate> ItemTemplates { get; set; }

        public ItemTemplateSection TemplateSection { get; set; }
    }
}