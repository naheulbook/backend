﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Job
    {
        public Job()
        {
            JobBonus = new HashSet<JobBonus>();
            JobOriginBlacklist = new HashSet<JobOriginBlacklist>();
            JobOriginWhitelist = new HashSet<JobOriginWhitelist>();
            JobRequirement = new HashSet<JobRequirement>();
            JobRestrict = new HashSet<JobRestrict>();
            JobSkill = new HashSet<JobSkill>();
            Speciality = new HashSet<Speciality>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public string Internalname { get; set; }

        [JsonProperty]
        public string Informations { get; set; }

        [JsonProperty]
        public string PlayerDescription { get; set; }

        [JsonProperty("playerSummary")]
        public string PlayerSummary { get; set; }

        public long? MaxLoad { get; set; }

        public short? MaxArmorPr { get; set; }

        [JsonProperty("isMagic")]
        public bool? IsMagic { get; set; }

        [JsonProperty("baseEv")]
        public short? BaseEv { get; set; }

        [JsonProperty("factorEv")]
        public double? FactorEv { get; set; }

        [JsonProperty("bonusEv")]
        public short? BonusEv { get; set; }

        [JsonProperty("baseEa")]
        public short? BaseEa { get; set; }

        [JsonProperty("diceEaLevelUp")]
        public short? DiceEaLevelUp { get; set; }

        [JsonProperty("baseAT")]
        public short? BaseAt { get; set; }

        [JsonProperty("basePRD")]
        public short? BasePrd { get; set; }

        [JsonProperty("parentJobId")]
        public long? ParentJobId { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }

        [JsonProperty("skills")]
        [NotMapped]
        public IEnumerable<dynamic> Skills => JobSkill.Where(s => s.Default).Select(os =>
        {
            dynamic d = new ExpandoObject();
            d.id = os.SkillId;
            return d;
        });

        [JsonProperty("availableSkills")]
        [NotMapped]
        public IEnumerable<dynamic> AvailableSkills => JobSkill.Where(s => !s.Default).Select(os =>
        {
            dynamic d = new ExpandoObject();
            d.id = os.SkillId;
            return d;
        });

        [JsonProperty("originsWhitelist")]
        [NotMapped]
        public IEnumerable<dynamic> OriginWhitelist => JobOriginWhitelist.Select(os =>
        {
            dynamic d = new ExpandoObject();
            d.id = os.Origin.Id;
            d.name = os.Origin.Name;
            return d;
        });

        [JsonProperty("originsBlacklist")]
        [NotMapped]
        public IEnumerable<dynamic> OriginBlacklist => JobOriginBlacklist.Select(os =>
        {
            dynamic d = new ExpandoObject();
            d.id = os.Origin.Id;
            d.name = os.Origin.Name;
            return d;
        });

        [JsonProperty("bonuses")]
        public ICollection<JobBonus> JobBonus { get; set; }

        public ICollection<JobOriginBlacklist> JobOriginBlacklist { get; set; }

        public ICollection<JobOriginWhitelist> JobOriginWhitelist { get; set; }

        [JsonProperty("requirements")]
        public ICollection<JobRequirement> JobRequirement { get; set; }

        [JsonProperty("restricts")]
        public ICollection<JobRestrict> JobRestrict { get; set; }

        public ICollection<JobSkill> JobSkill { get; set; }

        [JsonProperty("specialities")]
        public ICollection<Speciality> Speciality { get; set; }

        public Job Parentjob { get; set; }

        public ICollection<CharacterJob> CharacterJobs { get; set; }
    }
}