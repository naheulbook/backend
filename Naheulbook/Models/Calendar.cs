﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Calendar
    {
        public Calendar()
        {
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public int StartDay { get; set; }

        [JsonProperty]
        public int EndDay { get; set; }

        [JsonProperty]
        public string Note { get; set; }
    }
}