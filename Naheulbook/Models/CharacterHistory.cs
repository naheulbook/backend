﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class CharacterHistory : IHistory
    {
        public CharacterHistory()
        {
        }

        public CharacterHistory(Character character, User user, string action, object data = null)
        {
            CharacterId = character.Id;
            UserId = user.Id;
            Action = action;
            Date = DateTime.Now;
            if (data != null)
            {
                Data = JsonConvert.SerializeObject(data);
            }
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public DateTime Date { get; set; }

        public long CharacterId { get; set; }

        [JsonProperty]
        public string Action { get; set; }

        [JsonProperty]
        public string Info { get; set; }

        public long? EffectId { get; set; }

        public long? ModifierId { get; set; }

        public long? ItemId { get; set; }

        [JsonProperty]
        public bool Gm { get; set; }

        [NotMapped]
        [JsonProperty("isGroup")]
        public bool IsGroup => false;

        public long? UserId { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Data { get; set; }

        [JsonProperty("effect")]
        public virtual Effect Effect { get; set; }

        [JsonProperty("item")]
        public virtual Item Item { get; set; }

        [JsonProperty("modifier")]
        public virtual CharacterModifier Modifier { get; set; }

        public virtual User User { get; set; }

        public virtual Character Character { get; set; }
    }
}