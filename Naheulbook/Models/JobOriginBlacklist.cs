﻿namespace Naheulbook.Models
{
    public partial class JobOriginBlacklist
    {
        public long Id { get; set; }

        public long JobId { get; set; }

        public long OriginId { get; set; }

        public virtual Job Job { get; set; }

        public virtual Origin Origin { get; set; }
    }
}