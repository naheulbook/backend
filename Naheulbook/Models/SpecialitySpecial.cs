﻿using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SpecialitySpecial
    {
        [JsonProperty]
        public long Id { get; set; }

        public long SpecialityId { get; set; }

        [JsonProperty("isBonus")]
        public bool Isbonus { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        public virtual Speciality Speciality { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Flags { get; set; }
    }
}