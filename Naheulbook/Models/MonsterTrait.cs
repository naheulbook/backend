﻿using System.ComponentModel.DataAnnotations.Schema;
using Naheulbook.Tool;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class MonsterTrait
    {
        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        [JsonProperty("levels")]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string LevelsJson { get; set; }

        [NotMapped]
        public JArray Levels {
            get { return JArray.Parse(LevelsJson); }
            set { LevelsJson = value?.ToString(Formatting.None); }
        }
    }
}