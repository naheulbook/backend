﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Character
    {
        public Character()
        {
            CharacterModifiers = new HashSet<CharacterModifier>();
            CharacterSkills = new HashSet<CharacterSkills>();
            CharacterSpecialities = new HashSet<CharacterSpeciality>();
            CharacterJobs = new HashSet<CharacterJob>();
            GroupInvitations = new HashSet<GroupInvitations>();
            Items = new HashSet<Item>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public long? UserId { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty("originId")]
        public long OriginId { get; set; }

        [JsonProperty]
        public short Level { get; set; }

        [JsonProperty("jobIds")]
        [NotMapped]
        public List<long> JobIds => CharacterJobs.OrderBy(j => j.Order).Select(j => j.JobId).ToList();

        [JsonObject(MemberSerialization.OptIn)]
        public class CharacterStats
        {
            [JsonProperty("COU")]
            public int Cou { get; set; }

            [JsonProperty("AD")]
            public int Ad { get; set; }

            [JsonProperty("FO")]
            public int Fo { get; set; }

            [JsonProperty("INT")]
            public int Int { get; set; }

            [JsonProperty("CHA")]
            public int Cha { get; set; }
        }

        [NotMapped]
        [JsonProperty("stats")]
        public CharacterStats Stats => new CharacterStats() {Cou = Cou, Ad = Ad, Fo = Fo, Int = Int, Cha = Cha};

        public short Cou { get; set; }

        public short Int { get; set; }

        public short Cha { get; set; }

        public short Ad { get; set; }

        public short Fo { get; set; }

        [JsonProperty]
        public long Experience { get; set; }

        [JsonProperty("sex")]
        public string Sexe { get; set; }

        [JsonProperty("statBonusAD")]
        public string StatBonusAD { get; set; }

        [JsonProperty("fatePoint")]
        public short FatePoint { get; set; }

        [JsonProperty]
        public short? Ev { get; set; }

        [JsonProperty]
        public short? Ea { get; set; }

        public long? GroupId { get; set; }

        [JsonProperty]
        public bool Active { get; set; }

        [JsonProperty]
        public string Color { get; set; }

        public long? TargetCharacterId { get; set; }

        public long? TargetMonsterId { get; set; }

        [JsonProperty("isNpc")]
        public bool IsNpc { get; set; }

        [JsonConverter(typeof(PlainJsonStringConverter))]
        [JsonProperty(PropertyName = "gmData", NullValueHandling = NullValueHandling.Ignore)]
        public string GmDataJson
        {
            get => GmData.GetJson();
            set => GmData.SetJson(value);
        }

        [NotMapped]
        public JsonDataField GmData { get; } = new JsonDataField();

        [JsonProperty("modifiers")]
        public ICollection<CharacterModifier> CharacterModifiers { get; set; }

        [JsonProperty("skills")]
        public ICollection<CharacterSkills> CharacterSkills { get; set; }

        public ICollection<CharacterSpeciality> CharacterSpecialities { get; set; }

        [NotMapped]
        [JsonProperty("specialities")]
        public IEnumerable<object> Specialities => CharacterSpecialities.Select(g => g.Speciality);


        public ICollection<GroupInvitations> GroupInvitations { get; set; }

        [NotMapped]
        [JsonProperty("invites")]
        public IEnumerable<object> Invites
            => GroupInvitations.Select(g => new {name = g.Group.Name, id = g.GroupId});

        [JsonProperty("items")]
        public ICollection<Item> Items { get; set; }

        [JsonProperty("group")]
        public Group Group { get; set; }

        public Origin Origin { get; set; }

        public Character TargetCharacter { get; set; }

        public ICollection<Character> InverseTargetCharacter { get; set; }

        public ICollection<CharacterJob> CharacterJobs { get; set; }

        public ICollection<Monster> InverseTargetMonsters { get; set; }

        public ICollection<CharacterHistory> History { get; set; }

        [JsonProperty("target")]
        public object Target
        {
            get
            {
                if (TargetMonsterId != null)
                {
                    return new
                    {
                        isMonster = true,
                        id = TargetMonsterId
                    };
                }
                if (TargetCharacterId != null)
                {
                    return new
                    {
                        isMonster = false,
                        id = TargetCharacterId
                    };
                }
                return null;
            }
        }


        public Monster TargetMonster { get; set; }

        public User User { get; set; }

        public bool IsEditableBy(User currentUser)
        {
            if (UserId == currentUser.Id)
            {
                return true;
            }
            if (Group == null)
            {
                throw new NullReferenceException(
                    "GroupId must be include in EntityFramework request before trying to use this method.");
            }
            if (Group.MasterId == currentUser.Id)
            {
                return true;
            }
            return false;
        }

        public bool IsGm(User currentUser)
        {
            return Group != null && Group.MasterId == currentUser.Id;
        }
    }
}