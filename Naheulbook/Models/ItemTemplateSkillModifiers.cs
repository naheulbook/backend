﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ItemTemplateSkillModifiers
    {
        public long Id { get; set; }

        public long ItemTemplateId { get; set; }

        [JsonProperty("skill")]
        public long SkillId { get; set; }

        [JsonProperty]
        public short Value { get; set; }

        public virtual ItemTemplate ItemTemplate { get; set; }

        public virtual Skill Skill { get; set; }
    }
}