﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class SpecialityModifier
    {
        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public long SpecialityId { get; set; }

        [JsonProperty]
        public string Stat { get; set; }

        [JsonProperty]
        public long Value { get; set; }

        public virtual Speciality Speciality { get; set; }
    }
}