﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Naheulbook.Tool;
using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class Location
    {
        public Location()
        {
            LocationMap = new HashSet<LocationMap>();
            Sons = new HashSet<Location>();
        }

        [JsonProperty]
        public long Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty("parent")]
        public long? ParentId { get; set; }

        [JsonProperty]
        [JsonConverter(typeof(PlainJsonStringConverter))]
        public string Data { get; set; }

        [JsonProperty("sons")]
        [NotMapped]
        public List<object> ShortSons => new List<object> (Sons.Select(s => new {id = s.Id,name = s.Name}));

        [JsonProperty("maps")]
        public virtual ICollection<LocationMap> LocationMap { get; set; }

        public virtual Location Parent { get; set; }

        public virtual ICollection<Location> Sons { get; set; }

        public ICollection<MonsterLocation> Monsters { get; set; }
    }
}