﻿using Newtonsoft.Json;

namespace Naheulbook.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class CharacterSpeciality
    {
        public long CharacterId { get; set; }
        public long SpecialityId { get; set; }

        public virtual Character Character { get; set; }

        public virtual Speciality Speciality { get; set; }
    }
}
